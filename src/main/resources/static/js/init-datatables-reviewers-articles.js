$(document).ready(function () {
    $('#reviewers-articles-table').DataTable({
        processing: true,
        serverSide: true,
        filter: false,
        ajax: "/articles/reviewers",
        "language": {
            "url": chooseLanguage()
        },
        columns: [
            {data: "title"},
            {
                data: "creationDate",
                type: "date"
            },
            {data: "articleStatus"},
            {data: "decision"},
            {data: "idArticle"}
        ],
        columnDefs: [
            {
                "render": function (data, type, row) {
                    return formatDate(row.creationDate);
                },
                "targets": 1
            },
            {
                "targets": 4,
                "visible": false,
                "searchable": false
            },
            {
                "render": function (data, type, row){
                    return $("#enum"+row.articleStatus).val()
                },
                "targets" : 2
            },
            {
                "render": function (data, type, row){
                    if(row.decision!=null) {
                        return $("#enum"+row.decision).val()
                    }else{
                        return ''
                    }
                },
                "targets" : 3
            },
            {
                "render": function (data, type, row) {
                    return '<a onclick="handleArticleDetailsButton(`' + row.idArticle+'`,false,true)" class="btn btn-info btn-lg" data-toggle="modal" ><span class="glyphicon glyphicon-list" aria-hidden="true"></span></a>'
                },
                "targets": 5
            },
            {
                "render": function (data, type, row) {
                    if (isArticleReviewed(row.idArticle)) {
                        return ''
                    }
                    return  '<a onclick="handleReviewArticleButton(`' + row.idArticle + '`)" class="btn btn-info btn-lg" data-toggle="modal" ><span class="glyphicon glyphicon-list" aria-hidden="true"></span></a>'
                },
                "targets": 6
            }
        ]
    })
});

function handleReviewArticleButton(id) {
    window.location.href = "/reviewer/review/" + id
}

function isArticleReviewed(id) {
    var result = true;
    $.ajax({
        url: "/review/isReviewed/" + id,
        async: false,
        success: function (data) {
            result =  Boolean(data.status)
        }
    });
    return result;
}