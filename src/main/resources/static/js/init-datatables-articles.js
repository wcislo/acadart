$(document).ready( function () {
    $('#articles-table').DataTable({
        "language": {
            "url": chooseLanguage()
        },
        processing: true,
        serverSide: true,
        ordering: false,
        filter: false,
        ajax: "/author/myArticles/populate",
        columns: [
            {data: "title"},
            {data: "creationDate",
             type: "date"},
            {data: "articleStatus"},
            {data: "decision"},
            {data : "idArticle"}
        ],
        columnDefs: [

            {
                "render": function(data, type, row) {
                        return formatDate(row.creationDate);
                },
                "targets": 1
            },
            {
                "targets":  4 ,
                "visible": false,
                "searchable": false
            },
            {
                "render": function (data, type, row){
                    return $("#enum"+row.articleStatus).val()
                },
                "targets" : 2
            },
            {
                "render": function (data, type, row){
                    if(row.decision!=null) {
                        return $("#enum"+row.decision).val()
                    }else{
                        return ''
                    }
                },
                "targets" : 3
            },
            {
                "render": function (data, type, row) {
                    return '<a onclick="handleArticleDetailsButton(`'+row.idArticle+'`,true,false)" class="btn btn-info btn-lg" data-toggle="modal" ><span class="glyphicon glyphicon-list" aria-hidden="true"></span></a>'
                },
                "targets": 5
            }
        ]
    })
});



