$(document).ready(function () {
    switchRoleSelector();
    $("#login-edition").hide()
    $("#login").on("input", function(){
        if($("#current-login").val()!=$("#login").val()){
            $("#login-edition").show()
        }else{
            $("#login-edition").hide()
        }
    })
});

$(document).on('change', "#role_select", function () {
    switchView($(this), $("#role_select").val());
});

function switchView(obj, role) {
    if(role==="redactor"){
    window.location.replace("/redactor/articles")
    }
    if(role==="reviewer"){
        window.location.replace("/reviewer/myReviews")
    }
    if(role==="admin"){
        window.location.replace("/admin/accountManagement")
    }
    if(role==="author"){
        window.location.replace("/author/myArticles")
    }
    switchRoleContext(obj)
}

function switchRoleSelector() {
    $.ajax({
        url: "/switchSelector",
        success: function (data) {
            $("#roleSelector").html(data);
            switchRoleContext($("#role_select"))
        }
    })
}

function switchRoleContext(obj) {
    if (obj.val() != '') {
        $.ajax({
            url: "/switchContext",
            data: {
                selectedRole: obj.val()
            },
            success: function (data) {
                $('#sidebar').html(data)
            }
        })
    }
}

function showRedactorDetailsPopover(login) {
    $.ajax({
        url: "/account/popover/"+login,
        dataType: 'html',
        success: function(html) {
            $('#redactorPopover' + login).popover({
                content: html,
                placement: 'left',
                html: true,
            }).popover('toggle');
        }
    });
    $('#redactorPopover' + login).popover('destroy');
}

function showReviewerDetailsPopover(login) {
    $.ajax({
        url: "/account/popover/"+login,
        dataType: 'html',
        success: function(html) {
            $('#reviewerPopover' + login).popover({
                content: html,
                placement: 'right',
                html: true,
            }).popover('toggle');
        }
    });
    $('#reviewerPopover' + login).popover('destroy');
}


function showArticleDetailsPopover(identifier) {
    $.ajax({
        url: "/article/articlePopover/"+identifier,
        dataType: 'html',
        success: function(html) {
            $('#articlePopover' + identifier).popover({
                content: html,
                placement: 'bottom',
                html: true,
            }).popover('toggle');
        }
    });
    $('#articlePopover' + identifier).popover('destroy');
}

$('html').click(function(e) {
    $('#articlePopover').popover('hide');
});

$('#articlePopover').popover({
    html: true,
    trigger: 'manual'
}).click(function(e) {
    $(this).popover('toggle');
    e.stopPropagation();
});

function showReviewDetailsPopover(identifier){
    $.ajax({
        url: "/review/popover/"+identifier,
        dataType: 'html',
        success: function(html) {
            $('#reviewPopover' + identifier).popover({
                content: html,
                placement: 'left',
                html: true,
            }).popover('toggle');
        }
    });
    $('#reviewPopover' + identifier).popover('destroy');
}

$('html').click(function(e) {
    $('#reviewPopover').popover('destroy');
});

function getAllRedactors(){
    $.ajax({
        url: "/admin/getAllRedactors",
        dataType: "json",
        success: function(data){
            return data;
        }
    })
}

function handleShowDetailsButton(login) {
    jQuery('#details-modal').load("/account/" + login);
    $('#details-modal').modal('show');
}

function handleShowReviewDetailsButton(id) {
    jQuery('#review-details-modal').load("/review/details/" + id);

    $('#review-details-modal').on('show', function() {
        $('#article-details-modal').css('opacity', .5);
        $('#article-details-modal').unbind();
    });

    $('#review-details-modal').on('hidden', function() {
        $('#article-details-modal').css('opacity', 1);
        $('#article-details-modal').removeData("modal").modal({});
    });
    $('#review-details-modal').modal('show');
}


function padZeros(num){
    return ((num < 10) ? ("0" + num) : num)
}

function formatDate(miliseconds){
    var t = new Date(miliseconds);
    var month = t.getMonth()+1
    var date =  padZeros(t.getDate())+"/"+padZeros(month)+"/"+ t.getFullYear()+" "+ padZeros(t.getHours())+":"+padZeros(t.getMinutes())
    return date;
}

function handleArticleDetailsButton(idArticle, author, reviewer){
    jQuery('#article-details-modal').load("/article/details/"+idArticle+"/?author="+author+"&reviewer="+reviewer);
    $('#article-details-modal').modal('show');
}

function chooseLanguage() {
    var userLang = navigator.language || navigator.userLanguage;
    if (userLang === "pl") {
        return "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Polish.json"
    } else {
        return "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/English.json"
    }
}

function handleEditPasswordButton(){
    $("#edit-password-form").show()
    $("#edit-password-button").hide()
    $("#abort-edit-password-button").show()
}

function abortEditPasswordButton(){
    $("#edit-password-button").show()
    $("#abort-edit-password-button").hide()
    $("#edit-password-form").hide()
    $("#new-password").val("")
    $("#confirm-new-password").val("")

}

