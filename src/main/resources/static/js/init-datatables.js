var accountsTableUrl = "/admin/accountManagement/account"

$(document).ready(function () {
    accountsTable = $('#accounts-table').DataTable({
        "language": {
            "url": chooseLanguage()
        },
        processing: true,
        serverSide: true,
        ordering: false,
        filter: false,
        ajax: {
            url: accountsTableUrl,
            data: function(d){
                d.login= $("#input-1").val(),
                d.email = $("#input-2").val(),
                d.name = $("#input-3").val(),
                d.surname=  $("#input-4").val()
        }
        },
        columns: [
            {data: "login",
             width: "20%"},
            {data: "email",
                width: "15%"},
            {data: "name",
                width: "20%"},
            {data: "surname",
                width: "20%"},
            {data: "authorities",
                width: "20%"},
            {width: "2%"},
            {width: "3%"}
        ],
        columnDefs: [
            {"className": "dt-center", "targets": "_all"},
            {
                "render": function (data, type, row){
                    var htmlToReturn = '<span>';
                    if(row.authorities.indexOf("ADMINISTRATOR") > -1){
                        htmlToReturn+='<span><img src="../../img/admin.png" title="'+$("#adminLocale").val()+'" class="img-rounded"></span>'
                    }
                    if(row.authorities.indexOf("REDACTOR") > -1){
                        htmlToReturn+='<span><img src="../../img/redactor.png" title="'+$("#redactorLocale").val()+'" class="img-rounded"></span>'
                    }
                    if(row.authorities.indexOf("AUTHOR") > -1){
                        htmlToReturn+='<span><img src="../../img/author.png" title="'+$("#authorLocale").val()+'" class="img-rounded"></span>'
                    }
                    if(row.authorities.indexOf("REVIEWER") > -1){
                        htmlToReturn+='<span><img src="../../img/reviewer.png" title="'+$("#reviewerLocale").val()+'" class="img-rounded"></span>'
                    }
                    htmlToReturn+='</span>';
                    return htmlToReturn;
                },
                "targets": 4
            },
            {
                "render": function (data, type, row) {
                    return '<a onclick="handleShowDetailsButton(`' + row.login + '`)" class="btn btn-info btn-lg" data-toggle="modal" ><span class="glyphicon glyphicon-list" aria-hidden="true"></span></a>'
                },
                "targets": 5
            },
            {
                "render": function (data, type, row) {
                    return '<a href="#" onclick="handleEditionButton(`' + row.login + '`)"  class="btn-icon show-details" role="button"><span class="glyphicon glyphicon-list" aria-hidden="true"></span></a>'
                },
                "targets": 6
            }]
    });

    var count = 1;

    $('.filtered').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control input-sm" id="input-' + count + '" placeholder="' + title + '" />');
        count++
    });


    accountsTable.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change', function () {
            accountsTable.ajax.reload()
        });
    });
});


function handleEditionButton(login) {
    window.location.href = "/admin/editUser/" + login
}



