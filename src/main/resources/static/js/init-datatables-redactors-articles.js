$(document).ready( function () {
    $('#redactors-articles-table').DataTable({
        "language": {
            "url": chooseLanguage()
        },
        processing: true,
        serverSide: true,
        ordering: false,
        filter: false,
        ajax: "/redactor/articles/populate",
        columns: [
            {data: "title"},
            {data: "creationDate",
                type: "date"},
            {data: "articleStatus"},
            {data: "decision"},
            {data : "idArticle"}
        ],
        columnDefs: [
            {
                "render": function(data, type, row) {
                    return formatDate(row.creationDate);
                },
                "targets": 1
            },
            {
                "render": function (data, type, row){
                    return $("#enum"+row.articleStatus).val()
                },
                "targets" : 2
            },
            {
                "render": function (data, type, row){
                    if(row.decision!=null) {
                        return $("#enum"+row.decision).val()
                    }else{
                        return ''
                    }
                },
                "targets" : 3
            },
            {
                "targets":  4 ,
                "visible": false,
                "searchable": false
            },
            {
                "render": function (data, type, row) {
                    return '<a onclick="handleArticleDetailsButton(`'+row.idArticle+'`,false,false)" class="btn btn-info btn-lg" data-toggle="modal" ><span class="glyphicon glyphicon-list" aria-hidden="true"></span></a>'
                },
                "targets": 5
            },
            {
                "render": function (data, type, row) {
                    if(row.articleStatus=='PENDING') {
                        return '<a onclick="handleAssignButton(`' + row.idArticle + '`)" class="btn btn-info btn-lg" data-toggle="modal" ><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>'
                    }else{
                        return ''
                    }
                },
                "targets": 6
            },
            {
                "render": function (data, type, row) {
                    if(row.articleStatus!='PROCESSED') {
                        return '<a onclick="handleAssembleButton(`' + row.idArticle + '`)" class="btn btn-info btn-lg" data-toggle="modal" ><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></a>'
                    }else{
                        return ''
                    }
                },
                "targets": 7
            }
        ]
    })
});

function handleAssignButton(id) {
    window.location.href = "/redactor/articles/assign/" + id
}

function handleAssembleButton(id) {
    window.location.href = "/redactor/articles/assemble/" + id
}