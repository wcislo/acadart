INSERT INTO `acadart2`.`address` (`id_address`, `city`, `country`, `postal_code`, `street_number_flat`) VALUES ('1', 'Łódź', 'Polska', '94-214', 'Piotrkowska 93 m 9');
INSERT INTO `acadart2`.`address` (`id_address`, `city`, `country`, `postal_code`, `street_number_flat`) VALUES ('2', 'Warszawa', 'Polska', '93-111', 'Kwiatowa 10 m 12');

INSERT INTO `acadart2`.`account` (`id_account`, `affiliation`, `login`, `password`, `email`, `name`, `surname`, `id_address`, `is_enabled`, `is_account_non_locked`, `title`) VALUES('1', 'UW', 'admin', '$2a$10$7rVepGdt62Oy3Z.qcZ1Kdekp92DBtwh3xF8AbFnhFzkU5qS1J3bKa', 'admin@acadart.com', 'Adam', 'Wolański','1', TRUE, true, 'MR');
INSERT INTO `acadart2`.`account` (`id_account`, `affiliation`, `login`, `password`, `email`, `name`, `surname`, `id_address`, `is_enabled`, `is_account_non_locked`, `title`) VALUES('2', 'UW',  'redactor', '$2a$10$7rVepGdt62Oy3Z.qcZ1Kdekp92DBtwh3xF8AbFnhFzkU5qS1J3bKa', 'redactor@acadart.com', 'Paweł','Polka', '2', TRUE,true, 'PROF');
INSERT INTO `acadart2`.`account` (`id_account`, `affiliation`, `login`, `password`, `email`, `name`, `surname`, `id_address`, `is_enabled`, `is_account_non_locked`, `title`) VALUES('3', 'UW',  'reviewer', '$2a$10$7rVepGdt62Oy3Z.qcZ1Kdekp92DBtwh3xF8AbFnhFzkU5qS1J3bKa', 'reviewer@acadart.com', 'Joanna','Głuchowska', '2', TRUE,true, 'MRS');
INSERT INTO `acadart2`.`account` (`id_account`, `affiliation`, `login`, `password`, `email`, `name`, `surname`, `id_address`, `is_enabled`, `is_account_non_locked`, `title`) VALUES ('4', 'UW',  'author', '$2a$10$7rVepGdt62Oy3Z.qcZ1Kdekp92DBtwh3xF8AbFnhFzkU5qS1J3bKa', 'author@acadart.com', 'Zygmunt','Polański', '2', TRUE,true, 'PROF');
INSERT INTO `acadart2`.`account` (`id_account`, `affiliation`, `login`, `password`, `email`, `name`, `surname`, `id_address`, `is_enabled`, `is_account_non_locked`, `title`) VALUES ('5', 'UW',  'jankowalski', '$2a$10$7rVepGdt62Oy3Z.qcZ1Kdekp92DBtwh3xF8AbFnhFzkU5qS1J3bKa', 'jankowalski@acadart.com','Jan', 'Kowalski', '2', TRUE,true, 'PROF');
INSERT INTO `acadart2`.`account` (`id_account`, `affiliation`, `login`, `password`, `email`, `name`, `surname`, `id_address`, `is_enabled`, `is_account_non_locked`, `title`) VALUES ('6', 'UW',  'krzysztofn', '$2a$10$7rVepGdt62Oy3Z.qcZ1Kdekp92DBtwh3xF8AbFnhFzkU5qS1J3bKa', 'krzysztofn@acadart.com','Krzysztof', 'Nowak', '2', TRUE,true, 'MR');
INSERT INTO `acadart2`.`account` (`id_account`, `affiliation`, `login`, `password`, `email`, `name`, `surname`, `id_address`, `is_enabled`, `is_account_non_locked`, `title`) VALUES ('7', 'UW',  'katarzynaw', '$2a$10$7rVepGdt62Oy3Z.qcZ1Kdekp92DBtwh3xF8AbFnhFzkU5qS1J3bKa', 'kataw@acadart.com','Katarzyna', 'Wiśniewska', '2', TRUE,true, 'PROF');
INSERT INTO `acadart2`.`account` (`id_account`, `affiliation`, `login`, `password`, `email`, `name`, `surname`, `id_address`, `is_enabled`, `is_account_non_locked`, `title`) VALUES ('8', 'UW',  'jacekw', '$2a$10$7rVepGdt62Oy3Z.qcZ1Kdekp92DBtwh3xF8AbFnhFzkU5qS1J3bKa', 'wcislja@acadart.com', 'Jacek','Wcisło', '2', TRUE,true, 'PROF');
INSERT INTO `acadart2`.`account` (`id_account`, `affiliation`, `login`, `password`, `email`, `name`,  `surname`, `id_address`, `is_enabled`, `is_account_non_locked`, `title`) VALUES ('9', 'UW',  'kamilaw', '$2a$10$7rVepGdt62Oy3Z.qcZ1Kdekp92DBtwh3xF8AbFnhFzkU5qS1J3bKa', 'kawi@acadart.com', 'Kamila', 'Więckowska', '2', true,true, 'MRS');
INSERT INTO `acadart2`.`account` (`id_account`, `affiliation`, `login`, `password`, `email`, `name`,  `surname`, `id_address`, `is_enabled`, `is_account_non_locked`, `title`) VALUES ('10', 'UW',  'mieczyslawp', '$2a$10$7rVepGdt62Oy3Z.qcZ1Kdekp92DBtwh3xF8AbFnhFzkU5qS1J3bKa', 'mieczyp@acadart.com', 'Mieczysław', 'Piotrowski', '2', true,true, 'MR');
INSERT INTO `acadart2`.`account` (`id_account`, `affiliation`, `login`, `password`, `email`, `name`,  `surname`, `id_address`, `is_enabled`, `is_account_non_locked`, `title`) VALUES ('11', 'UW',  'kamilb', '$2a$10$7rVepGdt62Oy3Z.qcZ1Kdekp92DBtwh3xF8AbFnhFzkU5qS1J3bKa', 'kamilbacadart2.com', 'Kamil', 'Bednarek', '2', true,true, 'MR');

INSERT INTO `acadart2`.`account_authority` (`id_account`, `authority`) VALUES ('1', 'AUTHOR');
INSERT INTO `acadart2`.`account_authority` (`id_account`, `authority`) VALUES ('1', 'ADMINISTRATOR');
INSERT INTO `acadart2`.`account_authority` (`id_account`, `authority`) VALUES ('1', 'REDACTOR');
INSERT INTO `acadart2`.`account_authority` (`id_account`, `authority`) VALUES ('1', 'REVIEWER');

INSERT INTO `acadart2`.`account_authority` (`id_account`, `authority`) VALUES ('2', 'REDACTOR');

INSERT INTO `acadart2`.`account_authority` (`id_account`, `authority`) VALUES ('3', 'REVIEWER');

INSERT INTO `acadart2`.`account_authority` (`id_account`, `authority`) VALUES ('4', 'AUTHOR');

INSERT INTO `acadart2`.`account_authority` (`id_account`, `authority`) VALUES ('5', 'REDACTOR');
INSERT INTO `acadart2`.`account_authority` (`id_account`, `authority`) VALUES ('5', 'REVIEWER');

INSERT INTO `acadart2`.`account_authority` (`id_account`, `authority`) VALUES ('6', 'AUTHOR');
INSERT INTO `acadart2`.`account_authority` (`id_account`, `authority`) VALUES ('6', 'REVIEWER');

INSERT INTO `acadart2`.`account_authority` (`id_account`, `authority`) VALUES ('7', 'ADMINISTRATOR');
INSERT INTO `acadart2`.`account_authority` (`id_account`, `authority`) VALUES ('7', 'REDACTOR');
INSERT INTO `acadart2`.`account_authority` (`id_account`, `authority`) VALUES ('7', 'REVIEWER');

INSERT INTO `acadart2`.`account_authority` (`id_account`, `authority`) VALUES ('8', 'REVIEWER');


INSERT INTO `acadart2`.`account_authority` (`id_account`, `authority`) VALUES ('9', 'AUTHOR');
INSERT INTO `acadart2`.`account_authority` (`id_account`, `authority`) VALUES ('9', 'REVIEWER');

INSERT INTO `acadart2`.`account_authority` (`id_account`, `authority`) VALUES ('10', 'AUTHOR');
INSERT INTO `acadart2`.`account_authority` (`id_account`, `authority`) VALUES ('10', 'REVIEWER');

INSERT INTO `acadart2`.`account_authority` (`id_account`, `authority`) VALUES ('11', 'AUTHOR');

INSERT INTO `acadart2`.`reviewer_redactor` (`reviewer`, `redactor`) VALUES ('1', '2');

INSERT INTO `acadart2`.`uploaded_file` (`id_file`, `file`, `file_name`) VALUES('1', 'file', 'filename');

INSERT INTO `acadart2`.`article` (`id_article`, `article_abstract`, `article_status`, `creation_date`,  `author_id_account`, `title`, `uploaded_file_id_file`) VALUES ('1', 'abstrakt', 'ASSIGNED', now(), '1',  'Tytuł artykułu', '1');

INSERT INTO `acadart2`.`review` (`id_review`, `reviewer_id_account`, `decision`, `id_article`, `uploaded_file_id_file`) VALUES ('1', '3', 'ACCEPTED', '1', '1');
