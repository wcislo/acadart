package pl.lodz.p.acadart.services;

import pl.lodz.p.acadart.dtos.ArticleDTO;
import pl.lodz.p.acadart.dtos.GenericDatatablesDTO;
import pl.lodz.p.acadart.models.Article;
import pl.lodz.p.acadart.models.ArticleStatus;

import java.util.Map;
import java.util.Set;

/**
 * Created by jacek on 10.04.2016.
 */
public interface ArticleService {

    /**
     * Add article.
     *
     * @param article the article
     */
    void addArticle(ArticleDTO article);

    /**
     * Find by id article dto.
     *
     * @param id the id
     * @return the article dto
     */
    ArticleDTO findById(Long id);

    /**
     * Find articles by params generic datatables dto.
     *
     * @param params the params
     * @return the generic datatables dto
     */
    GenericDatatablesDTO<ArticleDTO> findArticlesByParams(Map<String, String> params);

    /**
     * Update article article dto.
     *
     * @param articleDTO the article dto
     * @return the article dto
     */
    ArticleDTO updateArticle(ArticleDTO articleDTO);

    /**
     * Find reviewers available articles by login generic datatables dto.
     *
     * @param login the login
     * @param draw  the draw
     * @param start the start
     * @param count the count
     * @return the generic datatables dto
     */
    GenericDatatablesDTO<ArticleDTO> findReviewersAvailableArticlesByLogin(String login, Integer draw, Integer start, Integer count);

    /**
     * Is article reviewed by current user boolean.
     *
     * @param userLogin the user login
     * @param articleId the article id
     * @return the boolean
     */
    Boolean isArticleReviewedByCurrentUser(String userLogin, Long articleId);
}
