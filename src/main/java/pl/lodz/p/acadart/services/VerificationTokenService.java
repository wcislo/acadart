package pl.lodz.p.acadart.services;

import pl.lodz.p.acadart.dtos.AccountDTO;
import pl.lodz.p.acadart.models.VerificationToken;

/**
 * Created by jacek on 09.04.2016.
 */
public interface VerificationTokenService {

    /**
     * Create verification token.
     *
     * @param accountDto the account dto
     * @param token      the token
     */
    void createVerificationToken(AccountDTO accountDto, String token);

    /**
     * Gets verification token.
     *
     * @param verificationToken the verification token
     * @return the verification token
     */
    VerificationToken getVerificationToken(String verificationToken);
}
