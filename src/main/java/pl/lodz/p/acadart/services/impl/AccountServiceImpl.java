package pl.lodz.p.acadart.services.impl;

import org.dozer.Mapper;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.lodz.p.acadart.dtos.AccountDTO;
import pl.lodz.p.acadart.dtos.GenericDatatablesDTO;
import pl.lodz.p.acadart.forms.RegistrationForm;
import pl.lodz.p.acadart.models.Account;
import pl.lodz.p.acadart.models.Authority;
import pl.lodz.p.acadart.repositories.AccountRepository;
import pl.lodz.p.acadart.repositories.CustomAccountRepository;
import pl.lodz.p.acadart.repositories.VerificationTokenRepository;
import pl.lodz.p.acadart.services.AccountService;
import pl.lodz.p.acadart.utils.EncodingUtils;

import javax.annotation.Resource;
import java.util.*;

/**
 * The type Account service.
 */
@Service(value = "accountService")
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class AccountServiceImpl implements AccountService {

    private final org.slf4j.Logger log = LoggerFactory.getLogger(this.getClass());

    @Resource
    private AccountRepository accountRepository;

    @Resource
    private CustomAccountRepository customAccountRepository;

    @Resource
    private Mapper mapper;

    @Resource
    private VerificationTokenRepository verificationTokenRepository;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        UserDetails userDetails = null;
        Account account = accountRepository.findByLogin(login);
        if (null == account) {
            log.warn("User " + login + " not found");
            throw new UsernameNotFoundException("User " + login + " not found.");
        }
        List<GrantedAuthority> authorities = buildUserAuthority(account.getAuthorities());
        userDetails = buildUserForAuthentication(account, authorities);
        return userDetails;
    }

    private User buildUserForAuthentication(Account account, List<GrantedAuthority> authorities) {
        return new User(account.getUsername(), account.getPassword(), account.isEnabled(), account.isAccountNonExpired(), account.isCredentialsNonExpired(), account.isAccountNonLocked(), authorities);
    }

    private List<GrantedAuthority> buildUserAuthority(Set<Authority> authorities) {

        Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();
        for (Authority authority : authorities) {
            setAuths.add(new SimpleGrantedAuthority(authority.name()));
        }
        return new ArrayList<>(setAuths);
    }

    @Override
    public AccountDTO registerUser(RegistrationForm registrationForm) {
        if (null == registrationForm) {
            log.error("registrationForm was invalid, registration failed");
        } else {
            registrationForm.setPassword(EncodingUtils.encrypt(registrationForm.getPassword()));
            Account account = getMapper().map(registrationForm, Account.class);
            account.setAuthorities(new HashSet<>(Arrays.asList(Authority.AUTHOR)));
            return getMapper().map(accountRepository.save(account), AccountDTO.class);
        }
        return null;
    }

    @Override
    public AccountDTO getAccountByLogin(String login) {
        Account account = accountRepository.findByLogin(login);
        if (account != null) {
            return getMapper().map(accountRepository.findByLogin(login), AccountDTO.class);
        }
        return null;
    }

    @Override
    public GenericDatatablesDTO<AccountDTO> getPaginatedAndFilteredAccounts(Integer draw, Integer start, Integer length, String name, String surname, String email, String login) {
        Set<AccountDTO> accountDTOs = new LinkedHashSet<AccountDTO>();
        for (Account acc : getCustomAccountRepository().getPaginatedFilteredAndOrderedByLoginAccounts(start, length, name, surname, email, login)) {
            accountDTOs.add(getMapper().map(acc, AccountDTO.class));
        }
        return new GenericDatatablesDTO<>(accountDTOs, draw, getAccountRepository().getAccountCount(), getCustomAccountRepository().getFilteredAccountsCount(name, surname, email, login).intValue());
    }

    @Override
    public void updateUser(AccountDTO accountDTO) {
        Account account = getAccountRepository().findOne(accountDTO.getIdAccount());
        getMapper().map(accountDTO, account);
        getAccountRepository().saveAndFlush(account);
    }

    @Override
    public AccountDTO saveRegisteredUser(Account account) {
        getAccountRepository().saveAndFlush(account);
        return getMapper().map(account, AccountDTO.class);
    }

    @Override
    public Set<AccountDTO> getAccountsByAuthority(Authority authority) {
        Set<Account> accountSet = getCustomAccountRepository().getAccountsByAuthority(authority);
        Set<AccountDTO> accountDTOSet = new HashSet<AccountDTO>();
        for (Account account : accountSet) {
            accountDTOSet.add(getMapper().map(account, AccountDTO.class));
        }
        return accountDTOSet;
    }

    @Override
    public AccountDTO getAccountById(Long id) {
        return getMapper().map(getAccountRepository().findOne(id), AccountDTO.class);
    }


    @Override
    public AccountDTO getAccountByEmail(String email) {
        Account account = accountRepository.findByEmail(email);
        if (account != null) {
            return getMapper().map(accountRepository.findByEmail(email), AccountDTO.class);
        }
        return null;
    }


    /**
     * Gets account repository.
     *
     * @return the account repository
     */
    public AccountRepository getAccountRepository() {
        return accountRepository;
    }

    /**
     * Sets account repository.
     *
     * @param accountRepository the account repository
     */
    public void setAccountRepository(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    /**
     * Gets custom account repository.
     *
     * @return the custom account repository
     */
    public CustomAccountRepository getCustomAccountRepository() {
        return customAccountRepository;
    }

    /**
     * Gets verification token repository.
     *
     * @return the verification token repository
     */
    public VerificationTokenRepository getVerificationTokenRepository() {
        return verificationTokenRepository;
    }

    /**
     * Gets mapper.
     *
     * @return the mapper
     */
    public Mapper getMapper() {
        return mapper;
    }

    /**
     * Sets custom account repository.
     *
     * @param customAccountRepository the custom account repository
     */
    public void setCustomAccountRepository(CustomAccountRepository customAccountRepository) {
        this.customAccountRepository = customAccountRepository;
    }

    /**
     * Sets mapper.
     *
     * @param mapper the mapper
     */
    public void setMapper(Mapper mapper) {
        this.mapper = mapper;
    }

    /**
     * Sets verification token repository.
     *
     * @param verificationTokenRepository the verification token repository
     */
    public void setVerificationTokenRepository(VerificationTokenRepository verificationTokenRepository) {
        this.verificationTokenRepository = verificationTokenRepository;
    }
}
