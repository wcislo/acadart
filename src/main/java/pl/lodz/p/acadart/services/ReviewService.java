package pl.lodz.p.acadart.services;

import pl.lodz.p.acadart.dtos.ReviewDTO;

/**
 * Created by jacek on 08.05.2016.
 */
public interface ReviewService {

    /**
     * Gets review by id.
     *
     * @param id the id
     * @return the review by id
     */
    ReviewDTO getReviewById(Long id);
}
