package pl.lodz.p.acadart.services.impl;

import org.dozer.Mapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.lodz.p.acadart.dtos.ArticleDTO;
import pl.lodz.p.acadart.dtos.BaseAccountInformationDTO;
import pl.lodz.p.acadart.dtos.GenericDatatablesDTO;
import pl.lodz.p.acadart.dtos.ReviewDTO;
import pl.lodz.p.acadart.models.Account;
import pl.lodz.p.acadart.models.Article;
import pl.lodz.p.acadart.models.ArticleStatus;
import pl.lodz.p.acadart.repositories.AccountRepository;
import pl.lodz.p.acadart.repositories.ArticleRepository;
import pl.lodz.p.acadart.repositories.CustomArticleRepository;
import pl.lodz.p.acadart.services.AccountService;
import pl.lodz.p.acadart.services.ArticleService;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by jacek on 10.04.2016.
 */
@Service(value = "articleService")
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class ArticleServiceImpl implements ArticleService {

    @Resource
    private CustomArticleRepository customArticleRepository;

    @Resource
    private Mapper mapper;

    @Resource
    private ArticleRepository articleRepository;

    @Resource
    private AccountRepository accountRepository;

    @Override
    public GenericDatatablesDTO<ArticleDTO> findArticlesByParams(Map<String, String> params){
        Set<Article> articleSet = getCustomArticleRepository().findArticlesByParams(params);
        Set<ArticleDTO> articleDTOSet = new LinkedHashSet<>();
        for(Article article : articleSet){
            articleDTOSet.add(getMapper().map(article, ArticleDTO.class));
        }
        articleDTOSet = articleDTOSet.stream().sorted(Comparator.comparing(ArticleDTO::getArticleStatus, Comparator.nullsLast(Comparator.naturalOrder()))).collect(Collectors.toCollection(LinkedHashSet::new));
        Integer articlesByCriteriaCount = getCustomArticleRepository().countCriteriaByParams(params);
        return new GenericDatatablesDTO<ArticleDTO>(articleDTOSet, Integer.valueOf(params.get("draw")), articlesByCriteriaCount, articlesByCriteriaCount);
    }

    @Override
    public ArticleDTO updateArticle(ArticleDTO articleDTO) {
        Article article = getArticleRepository().saveAndFlush(getMapper().map(articleDTO, Article.class));
        return getMapper().map(article, ArticleDTO.class);
    }

    @Override
    public GenericDatatablesDTO<ArticleDTO> findReviewersAvailableArticlesByLogin(String login, Integer draw, Integer start, Integer count) {
        Account reviewer = getAccountRepository().findByLogin(login);
        Set<Article> reviewersArticles = reviewer.getReviewersArticles();
        reviewersArticles = reviewersArticles.stream().filter(article -> !article.getArticleStatus().equals(ArticleStatus.PENDING)).collect(Collectors.toSet());
        Set<ArticleDTO> articleDTOSet = new HashSet<>();
        for(Article article : reviewersArticles){
            articleDTOSet.add(getMapper().map(article, ArticleDTO.class));
        }
        Integer totalSize = articleDTOSet.size();
        Set<ArticleDTO> paginatedAndSorted = new LinkedHashSet<>(articleDTOSet);
        paginatedAndSorted = paginatedAndSorted.stream().skip(start).limit(count).sorted((o1, o2) -> o1.getArticleStatus().compareTo(o2.getArticleStatus())).collect(Collectors.toCollection(LinkedHashSet::new));
        return new GenericDatatablesDTO<ArticleDTO>(paginatedAndSorted, draw, totalSize, totalSize);

    }

    @Override
    public Boolean isArticleReviewedByCurrentUser(String userLogin, Long articleId) {
        ArticleDTO articleDTO = this.findById(articleId);
        Set<String> reviewersLogins = articleDTO.getAssignedReviewers().stream().map(BaseAccountInformationDTO::getLogin).collect(Collectors.toSet());
        Set<String> reviewedReviewersLogins = articleDTO.getReviews().stream().map(ReviewDTO::getReviewer).map(BaseAccountInformationDTO::getLogin).collect(Collectors.toSet());
        return reviewersLogins.contains(userLogin) && reviewedReviewersLogins.contains(userLogin);
    }


    @Override
    public void addArticle(ArticleDTO articleDto) {
        Article article = new Article();
        getMapper().map(articleDto, article);
        getArticleRepository().saveAndFlush(article);
    }

    @Override
    public ArticleDTO findById(Long id) {
        return getMapper().map(getArticleRepository().findOne(id), ArticleDTO.class);
    }

    /**
     * Gets custom article repository.
     *
     * @return the custom article repository
     */
    public CustomArticleRepository getCustomArticleRepository() {
        return customArticleRepository;
    }

    /**
     * Gets article repository.
     *
     * @return the article repository
     */
    public ArticleRepository getArticleRepository() {
        return articleRepository;
    }

    /**
     * Gets account repository.
     *
     * @return the account repository
     */
    public AccountRepository getAccountRepository() {
        return accountRepository;
    }

    /**
     * Gets mapper.
     *
     * @return the mapper
     */
    public Mapper getMapper() {
        return mapper;
    }
}
