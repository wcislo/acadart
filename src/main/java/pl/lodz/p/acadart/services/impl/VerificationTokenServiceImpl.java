package pl.lodz.p.acadart.services.impl;

import org.dozer.Mapper;
import org.springframework.stereotype.Service;
import pl.lodz.p.acadart.dtos.AccountDTO;
import pl.lodz.p.acadart.dtos.VerificationTokenDTO;
import pl.lodz.p.acadart.models.Account;
import pl.lodz.p.acadart.models.VerificationToken;
import pl.lodz.p.acadart.repositories.AccountRepository;
import pl.lodz.p.acadart.repositories.VerificationTokenRepository;
import pl.lodz.p.acadart.services.VerificationTokenService;

import javax.annotation.Resource;

/**
 * Created by jacek on 09.04.2016.
 */
@Service(value = "verificationTokenService")
public class VerificationTokenServiceImpl implements VerificationTokenService {

    @Resource
    private VerificationTokenRepository verificationTokenRepository;

    @Resource
    private AccountRepository accountRepository;

    @Resource
    private Mapper mapper;

    @Override
    public void createVerificationToken(AccountDTO accountDto, String token) {
        VerificationTokenDTO newToken = new VerificationTokenDTO(token, accountDto);
        Account account = getAccountRepository().findByLogin(accountDto.getLogin());
        VerificationToken verificationToken = getMapper().map(newToken, VerificationToken.class);
        verificationToken.setAccount(account);
        getVerificationTokenRepository().save(verificationToken);
    }

    @Override
    public VerificationToken getVerificationToken(String verificationToken) {
        return getVerificationTokenRepository().findByToken(verificationToken);
    }

    /**
     * Gets verification token repository.
     *
     * @return the verification token repository
     */
    public VerificationTokenRepository getVerificationTokenRepository() {
        return verificationTokenRepository;
    }

    /**
     * Gets account repository.
     *
     * @return the account repository
     */
    public AccountRepository getAccountRepository() {
        return accountRepository;
    }

    /**
     * Gets mapper.
     *
     * @return the mapper
     */
    public Mapper getMapper() {
        return mapper;
    }
}
