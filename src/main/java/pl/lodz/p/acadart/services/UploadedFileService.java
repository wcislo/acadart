package pl.lodz.p.acadart.services;

import pl.lodz.p.acadart.dtos.UploadedFileDTO;

/**
 * Created by jacek on 12.04.2016.
 */
public interface UploadedFileService {

    /**
     * Find by id uploaded file dto.
     *
     * @param id the id
     * @return the uploaded file dto
     */
    UploadedFileDTO findById(Long id);
}
