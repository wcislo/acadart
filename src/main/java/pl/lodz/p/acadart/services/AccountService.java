package pl.lodz.p.acadart.services;

import org.springframework.security.core.userdetails.UserDetailsService;
import pl.lodz.p.acadart.dtos.AccountDTO;
import pl.lodz.p.acadart.dtos.GenericDatatablesDTO;
import pl.lodz.p.acadart.forms.RegistrationForm;
import pl.lodz.p.acadart.models.Account;
import pl.lodz.p.acadart.models.Authority;
import pl.lodz.p.acadart.models.VerificationToken;

import java.util.Set;

/**
 * The interface Account service.
 */
public interface AccountService extends UserDetailsService {

    /**
     * Register user account dto.
     *
     * @param registrationForm the registration form
     * @return the account dto
     */
    AccountDTO registerUser(RegistrationForm registrationForm);

    /**
     * Gets paginated and filtered accounts.
     *
     * @param draw    the draw
     * @param start   the start
     * @param length  the length
     * @param name    the name
     * @param surname the surname
     * @param email   the email
     * @param login   the login
     * @return the paginated and filtered accounts
     */
    GenericDatatablesDTO<AccountDTO> getPaginatedAndFilteredAccounts(Integer draw, Integer start, Integer length, String name, String surname, String email, String login);

    /**
     * Gets account by email.
     *
     * @param email the email
     * @return the account by email
     */
    AccountDTO getAccountByEmail(String email);

    /**
     * Gets account by login.
     *
     * @param login the login
     * @return the account by login
     */
    AccountDTO getAccountByLogin(String login);

    /**
     * Update user.
     *
     * @param accountDTO the account dto
     */
    void updateUser(AccountDTO accountDTO);

    /**
     * Save registered user account dto.
     *
     * @param account the account
     * @return the account dto
     */
    AccountDTO saveRegisteredUser(Account account);

    /**
     * Gets accounts by authority.
     *
     * @param authority the authority
     * @return the accounts by authority
     */
    Set<AccountDTO> getAccountsByAuthority(Authority authority);

    /**
     * Gets account by id.
     *
     * @param id the id
     * @return the account by id
     */
    AccountDTO getAccountById(Long id);

}
