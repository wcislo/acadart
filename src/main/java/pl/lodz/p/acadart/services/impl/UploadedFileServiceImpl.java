package pl.lodz.p.acadart.services.impl;

import org.dozer.Mapper;
import org.springframework.stereotype.Service;
import pl.lodz.p.acadart.dtos.UploadedFileDTO;
import pl.lodz.p.acadart.repositories.UploadedFileRepository;
import pl.lodz.p.acadart.services.UploadedFileService;

import javax.annotation.Resource;

/**
 * Created by jacek on 12.04.2016.
 */
@Service(value="uploadedFileService")
public class UploadedFileServiceImpl implements UploadedFileService {

    @Resource
    private UploadedFileRepository uploadedFileRepository;

    @Resource
    private Mapper mapper;

    @Override
    public UploadedFileDTO findById(Long id) {
        return getMapper().map(getUploadedFileRepository().findOne(id), UploadedFileDTO.class);
    }

    /**
     * Gets uploaded file repository.
     *
     * @return the uploaded file repository
     */
    public UploadedFileRepository getUploadedFileRepository() {
        return uploadedFileRepository;
    }

    /**
     * Gets mapper.
     *
     * @return the mapper
     */
    public Mapper getMapper() {
        return mapper;
    }
}
