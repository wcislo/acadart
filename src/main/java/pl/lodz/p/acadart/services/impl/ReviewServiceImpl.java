package pl.lodz.p.acadart.services.impl;

import org.dozer.Mapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.lodz.p.acadart.dtos.ReviewDTO;
import pl.lodz.p.acadart.models.Review;
import pl.lodz.p.acadart.repositories.ReviewRepository;
import pl.lodz.p.acadart.services.ReviewService;

import javax.annotation.Resource;

/**
 * Created by jacek on 08.05.2016.
 */
@Service
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class ReviewServiceImpl implements ReviewService {

    @Resource
    private ReviewRepository reviewRepository;

    @Resource
    private Mapper mapper;

    @Override
    public ReviewDTO getReviewById(Long id) {
        Review review = getReviewRepository().findByIdReview(id);
        return getMapper().map(review, ReviewDTO.class);
    }

    /**
     * Gets review repository.
     *
     * @return the review repository
     */
    public ReviewRepository getReviewRepository() {
        return reviewRepository;
    }

    /**
     * Gets mapper.
     *
     * @return the mapper
     */
    public Mapper getMapper() {
        return mapper;
    }
}
