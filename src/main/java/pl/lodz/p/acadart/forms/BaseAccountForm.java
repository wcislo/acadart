package pl.lodz.p.acadart.forms;

import org.hibernate.validator.constraints.NotEmpty;
import pl.lodz.p.acadart.models.Title;

import javax.validation.constraints.Size;

/**
 * Created by jacek on 06.04.2016.
 */
public class BaseAccountForm {

    @NotEmpty(message = "{validation.login.not.empty}")
    @Size(min=6, max = 64, message = "{validation.login.length}")
    private String login;
    @NotEmpty(message = "{validation.name.not.empty}")
    @Size(max = 64, message = "{validation.name.length}")
    private String name;
    @NotEmpty(message = "{validation.surname.not.empty}")
    @Size(max = 64, message = "{validation.surname.length}")
    private String surname;
    @NotEmpty(message = "{validation.email.not.empty}")
    private String email;

    private Title title;

    @NotEmpty(message = "{validation.affiliation.not.empty}")
    @Size(max = 64,message = "{validation.affiliation.length}")
    private String affiliation;

    @NotEmpty(message = "{validation.country.not.empty}")
    @Size(max = 64, message = "{validation.country.length}")
    private String country;

    @NotEmpty(message = "{validation.city.not.empty}")
    @Size(max = 64, message = "{validation.city.length}")
    private String city;

    @NotEmpty(message = "{validation.street.number.flat.not.empty}")
    @Size(max = 64, message = "{validation.street.number.flat.length}")
    private String streetNumberFlat;

    @NotEmpty(message = "{validation.postal.code.not.empty}")
    @Size(max = 10, message ="{validation.postal.code.length}")
    private String postalCode;

    private Long idAccount;

    private String currentEmail = this.email;

    private String currentLogin = this.login;

    private Long idAddress;


    /**
     * Instantiates a new Base account form.
     */
    public BaseAccountForm() {
    }

    /**
     * Gets login.
     *
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * Sets login.
     *
     * @param login the login
     */
    public void setLogin(String login) {
        this.login = login;
    }


    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets surname.
     *
     * @return the surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Sets surname.
     *
     * @param surname the surname
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * Gets email.
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets email.
     *
     * @param email the email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets title.
     *
     * @return the title
     */
    public Title getTitle() {
        return title;
    }

    /**
     * Sets title.
     *
     * @param title the title
     */
    public void setTitle(Title title) {
        this.title = title;
    }

    /**
     * Gets affiliation.
     *
     * @return the affiliation
     */
    public String getAffiliation() {
        return affiliation;
    }

    /**
     * Sets affiliation.
     *
     * @param affiliation the affiliation
     */
    public void setAffiliation(String affiliation) {
        this.affiliation = affiliation;
    }

    /**
     * Gets country.
     *
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets country.
     *
     * @param country the country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * Gets city.
     *
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets city.
     *
     * @param city the city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * Gets street number flat.
     *
     * @return the street number flat
     */
    public String getStreetNumberFlat() {
        return streetNumberFlat;
    }

    /**
     * Sets street number flat.
     *
     * @param streetNumberFlat the street number flat
     */
    public void setStreetNumberFlat(String streetNumberFlat) {
        this.streetNumberFlat = streetNumberFlat;
    }

    /**
     * Gets postal code.
     *
     * @return the postal code
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * Sets postal code.
     *
     * @param postalCode the postal code
     */
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    /**
     * Getter for property 'currentEmail'.
     *
     * @return Value for property 'currentEmail'.
     */
    public String getCurrentEmail() {
        return currentEmail;
    }

    /**
     * Setter for property 'currentEmail'.
     *
     * @param currentEmail Value to set for property 'currentEmail'.
     */
    public void setCurrentEmail(String currentEmail) {
        this.currentEmail = currentEmail;
    }

    /**
     * Getter for property 'currentLogin'.
     *
     * @return Value for property 'currentLogin'.
     */
    public String getCurrentLogin() {
        return currentLogin;
    }

    /**
     * Setter for property 'currentLogin'.
     *
     * @param currentLogin Value to set for property 'currentLogin'.
     */
    public void setCurrentLogin(String currentLogin) {
        this.currentLogin = currentLogin;
    }


    /**
     * Gets id account.
     *
     * @return the id account
     */
    public Long getIdAccount() {
        return idAccount;
    }

    /**
     * Sets id account.
     *
     * @param idAccount the id account
     */
    public void setIdAccount(Long idAccount) {
        this.idAccount = idAccount;
    }

    /**
     * Gets id address.
     *
     * @return the id address
     */
    public Long getIdAddress() {
        return idAddress;
    }

    /**
     * Sets id address.
     *
     * @param idAddress the id address
     */
    public void setIdAddress(Long idAddress) {
        this.idAddress = idAddress;
    }
}
