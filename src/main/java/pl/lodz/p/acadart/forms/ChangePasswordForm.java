package pl.lodz.p.acadart.forms;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * Created by jacek on 29.05.2016.
 */
public class ChangePasswordForm {

    private String retypeCurrentPassword;

    @NotEmpty(message = "{validation.password.not.empty}")
    @Size(min = 6, max = 64, message = "{validation.password.length}")
    private String newPassword;

    private String confirmNewPassword;

    /**
     * Gets retype current password.
     *
     * @return the retype current password
     */
    public String getRetypeCurrentPassword() {
        return retypeCurrentPassword;
    }

    /**
     * Sets retype current password.
     *
     * @param retypeCurrentPassword the retype current password
     */
    public void setRetypeCurrentPassword(String retypeCurrentPassword) {
        this.retypeCurrentPassword = retypeCurrentPassword;
    }

    /**
     * Gets new password.
     *
     * @return the new password
     */
    public String getNewPassword() {
        return newPassword;
    }

    /**
     * Sets new password.
     *
     * @param newPassword the new password
     */
    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    /**
     * Gets confirm new password.
     *
     * @return the confirm new password
     */
    public String getConfirmNewPassword() {
        return confirmNewPassword;
    }

    /**
     * Sets confirm new password.
     *
     * @param confirmNewPassword the confirm new password
     */
    public void setConfirmNewPassword(String confirmNewPassword) {
        this.confirmNewPassword = confirmNewPassword;
    }
}
