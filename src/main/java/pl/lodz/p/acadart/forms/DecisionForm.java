package pl.lodz.p.acadart.forms;

import org.hibernate.validator.constraints.NotEmpty;
import pl.lodz.p.acadart.models.Decision;

import javax.validation.constraints.Size;

/**
 * Created by jacek on 10.05.2016.
 */
public class DecisionForm {

    private Decision decision;

    private Long idArticle;

    /**
     * Gets decision.
     *
     * @return the decision
     */
    public Decision getDecision() {
        return decision;
    }

    /**
     * Sets decision.
     *
     * @param decision the decision
     */
    public void setDecision(Decision decision) {
        this.decision = decision;
    }

    /**
     * Gets id article.
     *
     * @return the id article
     */
    public Long getIdArticle() {
        return idArticle;
    }

    /**
     * Sets id article.
     *
     * @param idArticle the id article
     */
    public void setIdArticle(Long idArticle) {
        this.idArticle = idArticle;
    }
}
