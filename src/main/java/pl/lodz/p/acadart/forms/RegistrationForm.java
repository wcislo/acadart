package pl.lodz.p.acadart.forms;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * The type Registration form.
 */
public class RegistrationForm extends BaseAccountForm{

	@NotEmpty(message = "{validation.password.not.empty}")
	@Size(min=6, max = 64, message = "{validation.password.length}")
	private String password;

	private String secondPassword;


    /**
     * Instantiates a new Registration form.
     */
    public RegistrationForm() {
	}

    /**
     * Gets password.
     *
     * @return the password
     */
    public String getPassword() {
		return password;
	}

    /**
     * Sets password.
     *
     * @param password the password
     */
    public void setPassword(String password) {
		this.password = password;
	}

    /**
     * Gets second password.
     *
     * @return the second password
     */
    public String getSecondPassword() {
		return secondPassword;
	}

    /**
     * Sets second password.
     *
     * @param secondPassword the second password
     */
    public void setSecondPassword(String secondPassword) {
		this.secondPassword = secondPassword;
	}


}
