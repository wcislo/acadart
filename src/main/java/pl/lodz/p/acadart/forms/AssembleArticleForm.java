package pl.lodz.p.acadart.forms;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * Created by jacek on 15.06.2016.
 */
public class AssembleArticleForm extends DecisionForm{


    @NotEmpty(message= "{decision.comment.not.empty}")
    @Size(max = 255, message="{decision.comment.too.long}")
    private String redactorsComment;

    /**
     * Gets redactors comment.
     *
     * @return the redactors comment
     */
    public String getRedactorsComment() {
        return redactorsComment;
    }

    /**
     * Sets redactors comment.
     *
     * @param redactorsComment the redactors comment
     */
    public void setRedactorsComment(String redactorsComment) {
        this.redactorsComment = redactorsComment;
    }
}
