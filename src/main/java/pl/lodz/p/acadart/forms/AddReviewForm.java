package pl.lodz.p.acadart.forms;

import org.springframework.web.multipart.MultipartFile;
import pl.lodz.p.acadart.models.Decision;

import javax.validation.constraints.NotNull;

/**
 * Created by jacek on 03.05.2016.
 */
public class AddReviewForm extends DecisionForm{

    @NotNull
    private MultipartFile multipartFile;

    private String comment;

    /**
     * Gets multipart file.
     *
     * @return the multipart file
     */
    public MultipartFile getMultipartFile() {
        return multipartFile;
    }

    /**
     * Sets multipart file.
     *
     * @param multipartFile the multipart file
     */
    public void setMultipartFile(MultipartFile multipartFile) {
        this.multipartFile = multipartFile;
    }

    /**
     * Gets comment.
     *
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * Sets comment.
     *
     * @param comment the comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }
}
