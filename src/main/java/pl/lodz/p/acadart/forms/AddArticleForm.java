package pl.lodz.p.acadart.forms;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by jacek on 11.04.2016.
 */
public class AddArticleForm {

    @NotEmpty(message = "{add.article.error.title.not.empty}")
    @Size(max=255, message = "{add.article.error.title.too.long}")
    private String title;

    @NotEmpty(message = "{add.article.error.abstract.not.empty}")
    @Size(max=255, message = "{add.article.error.abstract.too.long}")
    private String articleAbstract;

    @Size(max=255, message = "{add.article.error.comment.too.long}")
    private String authorsComment;

    @NotNull
    private MultipartFile multipartFile;

    /**
     * Gets title.
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets title.
     *
     * @param title the title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Gets article abstract.
     *
     * @return the article abstract
     */
    public String getArticleAbstract() {
        return articleAbstract;
    }

    /**
     * Sets article abstract.
     *
     * @param articleAbstract the article abstract
     */
    public void setArticleAbstract(String articleAbstract) {
        this.articleAbstract = articleAbstract;
    }

    /**
     * Gets authors comment.
     *
     * @return the authors comment
     */
    public String getAuthorsComment() {
        return authorsComment;
    }

    /**
     * Sets authors comment.
     *
     * @param authorsComment the authors comment
     */
    public void setAuthorsComment(String authorsComment) {
        this.authorsComment = authorsComment;
    }

    /**
     * Gets multipart file.
     *
     * @return the multipart file
     */
    public MultipartFile getMultipartFile() {
        return multipartFile;
    }

    /**
     * Sets multipart file.
     *
     * @param multipartFile the multipart file
     */
    public void setMultipartFile(MultipartFile multipartFile) {
        this.multipartFile = multipartFile;
    }
}
