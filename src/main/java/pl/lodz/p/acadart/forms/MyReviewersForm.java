package pl.lodz.p.acadart.forms;

import pl.lodz.p.acadart.dtos.BaseAccountInformationDTO;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by jacek on 02.05.2016.
 */
public class MyReviewersForm {

    private Set<String> redactorsReviewersLogins = new HashSet<>();

    private Set<BaseAccountInformationDTO> redactorsReviewers = new HashSet<>();

    /**
     * Gets redactors reviewers logins.
     *
     * @return the redactors reviewers logins
     */
    public Set<String> getRedactorsReviewersLogins() {
        return redactorsReviewersLogins;
    }

    /**
     * Sets redactors reviewers logins.
     *
     * @param redactorsReviewersLogins the redactors reviewers logins
     */
    public void setRedactorsReviewersLogins(Set<String> redactorsReviewersLogins) {
        this.redactorsReviewersLogins = redactorsReviewersLogins;
    }

    /**
     * Gets redactors reviewers.
     *
     * @return the redactors reviewers
     */
    public Set<BaseAccountInformationDTO> getRedactorsReviewers() {
        return redactorsReviewers;
    }

    /**
     * Sets redactors reviewers.
     *
     * @param redactorsReviewers the redactors reviewers
     */
    public void setRedactorsReviewers(Set<BaseAccountInformationDTO> redactorsReviewers) {
        this.redactorsReviewers = redactorsReviewers;
    }
}
