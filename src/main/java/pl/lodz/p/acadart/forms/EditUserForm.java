package pl.lodz.p.acadart.forms;

import pl.lodz.p.acadart.dtos.AccountDTO;
import pl.lodz.p.acadart.dtos.ArticleDTO;
import pl.lodz.p.acadart.dtos.BaseAccountInformationDTO;
import pl.lodz.p.acadart.dtos.ReviewDTO;
import pl.lodz.p.acadart.models.Authority;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by jacek on 06.04.2016.
 */
public class EditUserForm extends BaseAccountForm{

    private Set<BaseAccountInformationDTO> redactors = new HashSet<>();

    private Set<BaseAccountInformationDTO> reviewers = new HashSet<>();

    private Set<String> redactorsLogins = new HashSet<>();

    private Set<String> reviewersLogins = new HashSet<>();

    private Set<Authority> authorities = new HashSet<>();

    private Boolean enabled;

    private Boolean accountNonLocked;

    private Set<ArticleDTO> articles = new HashSet<>();

    private Set<ReviewDTO> reviews = new HashSet<>();

    private String newPassword = "";

    private String confirmNewPassword = "";

    private String password;

    private String currentPassword = this.password;

    /**
     * Gets redactors.
     *
     * @return the redactors
     */
    public Set<BaseAccountInformationDTO> getRedactors() {
        return redactors;
    }

    /**
     * Sets redactors.
     *
     * @param redactors the redactors
     */
    public void setRedactors(Set<BaseAccountInformationDTO> redactors) {
        this.redactors = redactors;
    }

    /**
     * Gets reviewers.
     *
     * @return the reviewers
     */
    public Set<BaseAccountInformationDTO> getReviewers() {
        return reviewers;
    }

    /**
     * Sets reviewers.
     *
     * @param reviewers the reviewers
     */
    public void setReviewers(Set<BaseAccountInformationDTO> reviewers) {
        this.reviewers = reviewers;
    }

    /**
     * Gets redactors logins.
     *
     * @return the redactors logins
     */
    public Set<String> getRedactorsLogins() {
        return redactorsLogins;
    }

    /**
     * Sets redactors logins.
     *
     * @param redactorsLogins the redactors logins
     */
    public void setRedactorsLogins(Set<String> redactorsLogins) {
        this.redactorsLogins = redactorsLogins;
    }

    /**
     * Gets reviewers logins.
     *
     * @return the reviewers logins
     */
    public Set<String> getReviewersLogins() {
        return reviewersLogins;
    }

    /**
     * Sets reviewers logins.
     *
     * @param reviewersLogins the reviewers logins
     */
    public void setReviewersLogins(Set<String> reviewersLogins) {
        this.reviewersLogins = reviewersLogins;
    }

    /**
     * Gets authorities.
     *
     * @return the authorities
     */
    public Set<Authority> getAuthorities() {
        return authorities;
    }

    /**
     * Sets authorities.
     *
     * @param authorities the authorities
     */
    public void setAuthorities(Set<Authority> authorities) {
        this.authorities = authorities;
    }

    /**
     * Gets enabled.
     *
     * @return the enabled
     */
    public Boolean getEnabled() {
        return enabled;
    }

    /**
     * Sets enabled.
     *
     * @param enabled the enabled
     */
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * Gets account non locked.
     *
     * @return the account non locked
     */
    public Boolean getAccountNonLocked() {
        return accountNonLocked;
    }

    /**
     * Sets account non locked.
     *
     * @param accountNonLocked the account non locked
     */
    public void setAccountNonLocked(Boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    /**
     * Gets articles.
     *
     * @return the articles
     */
    public Set<ArticleDTO> getArticles() {
        return articles;
    }

    /**
     * Sets articles.
     *
     * @param articles the articles
     */
    public void setArticles(Set<ArticleDTO> articles) {
        this.articles = articles;
    }

    /**
     * Gets reviews.
     *
     * @return the reviews
     */
    public Set<ReviewDTO> getReviews() {
        return reviews;
    }

    /**
     * Sets reviews.
     *
     * @param reviews the reviews
     */
    public void setReviews(Set<ReviewDTO> reviews) {
        this.reviews = reviews;
    }


    /**
     * Gets new password.
     *
     * @return the new password
     */
    public String getNewPassword() {
        return newPassword;
    }

    /**
     * Sets new password.
     *
     * @param newPassword the new password
     */
    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    /**
     * Gets confirm new password.
     *
     * @return the confirm new password
     */
    public String getConfirmNewPassword() {
        return confirmNewPassword;
    }

    /**
     * Sets confirm new password.
     *
     * @param confirmNewPassword the confirm new password
     */
    public void setConfirmNewPassword(String confirmNewPassword) {
        this.confirmNewPassword = confirmNewPassword;
    }

    /**
     * Gets password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Gets current password.
     *
     * @return the current password
     */
    public String getCurrentPassword() {
        return currentPassword;
    }

    /**
     * Sets current password.
     *
     * @param currentPassword the current password
     */
    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }
}
