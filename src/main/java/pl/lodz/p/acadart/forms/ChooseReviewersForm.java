package pl.lodz.p.acadart.forms;

import org.hibernate.validator.constraints.NotEmpty;
import pl.lodz.p.acadart.dtos.AccountDTO;
import pl.lodz.p.acadart.dtos.BaseAccountInformationDTO;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by jacek on 02.05.2016.
 */
public class ChooseReviewersForm {

    private Long idArticle;

    private String redactorsLogin;

    private Set<BaseAccountInformationDTO> assignedReviewers = new HashSet<BaseAccountInformationDTO>();

    @NotEmpty(message = "{assign.reviewers.not.empty}")
    private Set<String> reviewersLogins = new HashSet<>();

    /**
     * Gets id article.
     *
     * @return the id article
     */
    public Long getIdArticle() {
        return idArticle;
    }

    /**
     * Sets id article.
     *
     * @param idArticle the id article
     */
    public void setIdArticle(Long idArticle) {
        this.idArticle = idArticle;
    }

    /**
     * Gets redactors login.
     *
     * @return the redactors login
     */
    public String getRedactorsLogin() {
        return redactorsLogin;
    }

    /**
     * Sets redactors login.
     *
     * @param redactorsLogin the redactors login
     */
    public void setRedactorsLogin(String redactorsLogin) {
        this.redactorsLogin = redactorsLogin;
    }

    /**
     * Gets assigned reviewers.
     *
     * @return the assigned reviewers
     */
    public Set<BaseAccountInformationDTO> getAssignedReviewers() {
        return assignedReviewers;
    }

    /**
     * Sets assigned reviewers.
     *
     * @param assignedReviewers the assigned reviewers
     */
    public void setAssignedReviewers(Set<BaseAccountInformationDTO> assignedReviewers) {
        this.assignedReviewers = assignedReviewers;
    }

    /**
     * Gets reviewers logins.
     *
     * @return the reviewers logins
     */
    public Set<String> getReviewersLogins() {
        return reviewersLogins;
    }

    /**
     * Sets reviewers logins.
     *
     * @param reviewersLogins the reviewers logins
     */
    public void setReviewersLogins(Set<String> reviewersLogins) {
        this.reviewersLogins = reviewersLogins;
    }

}
