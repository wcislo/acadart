package pl.lodz.p.acadart.utils;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * The type Encoding utils.
 */
@Component
public class EncodingUtils {

	private static PasswordEncoder encoder = new BCryptPasswordEncoder();

    /**
     * Encrypt string.
     *
     * @param str the str
     * @return the string
     */
    public static String encrypt(String str) {
		return encoder.encode(str);
	}

    /**
     * Password encoder password encoder.
     *
     * @return the password encoder
     */
    public static PasswordEncoder passwordEncoder(){
		return encoder;
	}

    /**
     * Password match boolean.
     *
     * @param rawPassword     the raw password
     * @param encodedPassword the encoded password
     * @return the boolean
     */
    public static Boolean passwordMatch(String rawPassword, String encodedPassword) {
		return encoder.matches(rawPassword, encodedPassword);
	}

}