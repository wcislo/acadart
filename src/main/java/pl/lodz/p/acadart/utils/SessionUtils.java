package pl.lodz.p.acadart.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.orm.hibernate5.SpringSessionContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by jacek on 02.05.2016.
 */
public class SessionUtils {

    private static final Logger log = LoggerFactory.getLogger(SessionUtils.class);

    /**
     * Gets current users login.
     *
     * @return the current users login
     */
    public static String getCurrentUsersLogin() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

    /**
     * Invalidate session.
     *
     * @param request the request
     */
    public static void invalidateSession(HttpServletRequest request) {
        try {
            request.logout();
        } catch (ServletException e) {
            log.warn("Failed to log out user", e);
        }
    }

}

