package pl.lodz.p.acadart.models;

import javax.persistence.*;

/**
 * The type Address.
 */
@Entity
@Table(name = "address")
public class Address {

	/**
	 * The Id address.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_address")
	private Long idAddress;

	/**
	 * The Country.
	 */
	@Column(nullable = false, length = 64)
	private String country;

	/**
	 * The City.
	 */
	@Column(nullable = false, length = 64)
	private String city;

	/**
	 * The Street number flat.
	 */
	@Column(nullable = false, length = 255)
	private String streetNumberFlat;

	/**
	 * The Postal code.
	 */
	@Column(nullable = false, length = 10)
	private String postalCode;

    /**
     * Gets id address.
     *
     * @return the id address
     */
    public Long getIdAddress() {
		return idAddress;
	}

    /**
     * Sets id address.
     *
     * @param idAddress the id address
     */
    public void setIdAddress(Long idAddress) {
		this.idAddress = idAddress;
	}

    /**
     * Gets country.
     *
     * @return the country
     */
    public String getCountry() {
		return country;
	}

    /**
     * Sets country.
     *
     * @param country the country
     */
    public void setCountry(String country) {
		this.country = country;
	}

    /**
     * Gets city.
     *
     * @return the city
     */
    public String getCity() {
		return city;
	}

    /**
     * Sets city.
     *
     * @param city the city
     */
    public void setCity(String city) {
		this.city = city;
	}

    /**
     * Gets street number flat.
     *
     * @return the street number flat
     */
    public String getStreetNumberFlat() {
		return streetNumberFlat;
	}

    /**
     * Sets street number flat.
     *
     * @param streetNumberFlat the street number flat
     */
    public void setStreetNumberFlat(String streetNumberFlat) {
		this.streetNumberFlat = streetNumberFlat;
	}

    /**
     * Gets postal code.
     *
     * @return the postal code
     */
    public String getPostalCode() {
		return postalCode;
	}

    /**
     * Sets postal code.
     *
     * @param postalCode the postal code
     */
    public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	
	
}
