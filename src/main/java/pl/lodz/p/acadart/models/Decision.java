package pl.lodz.p.acadart.models;

/**
 * Created by jacek on 07.05.2016.
 */
public enum Decision {
    /**
     * Accepted decision.
     */
    ACCEPTED("enums.accepted"),
    /**
     * Minor revision decision.
     */
    MINOR_REVISION("enums.minor.revision"),
    /**
     * Major revision decision.
     */
    MAJOR_REVISION("enums.major.revision"),
    /**
     * Rejected decision.
     */
    REJECTED("enums.rejected"),
    /**
     * Refuse reviewing decision.
     */
    REFUSE_REVIEWING("enums.refuse.reviewing");

    /**
     * The Display name.
     */
    private final String displayName;

    /**
     * Instantiates a new Decision.
     *
     * @param displayName the display name
     */
    Decision(String displayName){
        this.displayName=displayName;
    }

    /**
     * Gets display name.
     *
     * @return the display name
     */
    public String getDisplayName() {
        return displayName;
    }


}
