package pl.lodz.p.acadart.models;


import javax.persistence.*;

/**
 * The type Review.
 */
@Entity
@Table(name = "review")
public class Review {

	/**
	 * The Id review.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_review")
	private Long idReview;

	/**
	 * The Article.
	 */
	@JoinColumn(name = "id_article", referencedColumnName = "id_article")
	@ManyToOne(optional = false)
	private Article article;

	/**
	 * The Decision.
	 */
	@Enumerated(EnumType.STRING)
	@Column(nullable=false, length=32)
	private Decision decision;

	/**
	 * The Reviewer.
	 */
	@ManyToOne(optional = false)
	private Account reviewer;

	/**
	 * The Uploaded file.
	 */
	@OneToOne(cascade={CascadeType.MERGE, CascadeType.PERSIST}, optional = false)
	@JoinColumn(nullable = false, name = "id_file", unique = true)
	private UploadedFile uploadedFile;

	@Column(columnDefinition = "TEXT")
	private String comment;

    /**
     * Gets id review.
     *
     * @return the id review
     */
    public Long getIdReview() {
		return idReview;
	}

    /**
     * Sets id review.
     *
     * @param idReview the id review
     */
    public void setIdReview(Long idReview) {
		this.idReview = idReview;
	}

    /**
     * Gets article.
     *
     * @return the article
     */
    public Article getArticle() {
		return article;
	}

    /**
     * Sets article.
     *
     * @param article the article
     */
    public void setArticle(Article article) {
		this.article = article;
	}

    /**
     * Gets decision.
     *
     * @return the decision
     */
    public Decision getDecision() {
		return decision;
	}

    /**
     * Sets decision.
     *
     * @param decision the decision
     */
    public void setDecision(Decision decision) {
		this.decision = decision;
	}

    /**
     * Gets reviewer.
     *
     * @return the reviewer
     */
    public Account getReviewer() {
		return reviewer;
	}

    /**
     * Sets reviewer.
     *
     * @param reviewer the reviewer
     */
    public void setReviewer(Account reviewer) {
		this.reviewer = reviewer;
	}

    /**
     * Gets uploaded file.
     *
     * @return the uploaded file
     */
    public UploadedFile getUploadedFile() {
		return uploadedFile;
	}

    /**
     * Sets uploaded file.
     *
     * @param uploadedFile the uploaded file
     */
    public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

    /**
     * Gets comment.
     *
     * @return the comment
     */
    public String getComment() {
		return comment;
	}

    /**
     * Sets comment.
     *
     * @param comment the comment
     */
    public void setComment(String comment) {
		this.comment = comment;
	}
}
