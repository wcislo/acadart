package pl.lodz.p.acadart.models;

import org.springframework.security.core.GrantedAuthority;

/**
 * Created by jacek on 21.03.2016.
 */
public enum Authority implements GrantedAuthority{
    /**
     * Reviewer authority.
     */
    REVIEWER("enums.reviewer"), /**
     * Redactor authority.
     */
    REDACTOR("enums.redactor"), /**
     * Author authority.
     */
    AUTHOR("enums.author"), /**
     * Administrator authority.
     */
    ADMINISTRATOR("enums.admin");

        @Override
        public String getAuthority() {
            return "ROLE_" + name();
        }

    /**
     * The Display name.
     */
    private final String displayName;

    /**
     * Instantiates a new Title.
     *
     * @param displayName the display name
     */
    Authority(String displayName){
        this.displayName=displayName;
    }

    /**
     * Gets display name.
     *
     * @return the display name
     */
    public String getDisplayName() {
        return displayName;
    }

}
