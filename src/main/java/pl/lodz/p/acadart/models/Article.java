package pl.lodz.p.acadart.models;


import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * The type Article.
 */
@Entity
@Table(name = "article")
public class Article {

	/**
	 * The Id article.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_article")
	private Long idArticle;

	/**
	 * The Reviews.
	 */
	@OneToMany(mappedBy = "article", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
	private Set<Review> reviews = new HashSet<>();


	/**
	 * The Creation date.
	 */
	@Column(name="creation_date", nullable=false)
	private Date creationDate = new Date();

	/**
	 * The Title.
	 */
	@Column(nullable = false)
	private String title;

	/**
	 * The Author.
	 */
	@ManyToOne(cascade = {CascadeType.MERGE}, optional=false)
	private Account author;

	/**
	 * The Authors comment.
	 */
	@Column(name="authors_comment", columnDefinition = "TEXT")
	private String authorsComment;


	/**
	 * The Redactors comment.
	 */
	@Column(name="redactors_comment", columnDefinition = "TEXT")
	private String redactorsComment;

	/**
	 * The Uploaded file.
	 */
	@OneToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, optional = false)
	@JoinColumn(nullable = false, name = "id_file", unique = true)
	private UploadedFile uploadedFile;


	@Enumerated(EnumType.STRING)
	@Column(name="article_status", nullable=false, length=16)
	private ArticleStatus articleStatus = ArticleStatus.PENDING;

	@Enumerated(EnumType.STRING)
	@Column(length=32)
	private Decision decision;

	@ManyToOne
	private Account redactor;

	@ManyToMany
	private Set<Account> assignedReviewers = new HashSet<>();

	@Column(nullable=false, name="article_abstract", columnDefinition = "TEXT")
	private String articleAbstract;

	/**
	 * Gets id article.
	 *
	 * @return the id article
	 */
	public Long getIdArticle() {
		return idArticle;
	}

	/**
	 * Gets reviews.
	 *
	 * @return the reviews
	 */
	public Set<Review> getReviews() {
		return reviews;
	}

	/**
	 * Sets reviews.
	 *
	 * @param reviews the reviews
	 */
	public void setReviews(Set<Review> reviews) {
		this.reviews = reviews;
	}

	/**
	 * Gets creation date.
	 *
	 * @return the creation date
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * Sets creation date.
	 *
	 * @param creationDate the creation date
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Gets author.
	 *
	 * @return the author
	 */
	public Account getAuthor() {
		return author;
	}

	/**
	 * Sets author.
	 *
	 * @param author the author
	 */
	public void setAuthor(Account author) {
		this.author = author;
	}

	/**
	 * Gets title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Gets authors comment.
	 *
	 * @return the authors comment
	 */
	public String getAuthorsComment() {
		return authorsComment;
	}

	/**
	 * Sets authors comment.
	 *
	 * @param authorsComment the authors comment
	 */
	public void setAuthorsComment(String authorsComment) {
		this.authorsComment = authorsComment;
	}

	/**
	 * Gets redactors comment.
	 *
	 * @return the redactors comment
	 */
	public String getRedactorsComment() {
		return redactorsComment;
	}

	/**
	 * Sets redactors comment.
	 *
	 * @param redactorsComment the redactors comment
	 */
	public void setRedactorsComment(String redactorsComment) {
		this.redactorsComment = redactorsComment;
	}

	/**
	 * Sets title.
	 *
	 * @param title the title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets uploaded file.
	 *
	 * @return the uploaded file
	 */
	public UploadedFile getUploadedFile() {
		return uploadedFile;
	}

	/**
	 * Sets uploaded file.
	 *
	 * @param uploadedFile the uploaded file
	 */
	public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}


	/**
	 * Gets article status.
	 *
	 * @return the article status
	 */
	public ArticleStatus getArticleStatus() {
		return articleStatus;
	}

	/**
	 * Sets article status.
	 *
	 * @param articleStatus the article status
	 */
	public void setArticleStatus(ArticleStatus articleStatus) {
		this.articleStatus = articleStatus;
	}

	/**
	 * Gets redactor.
	 *
	 * @return the redactor
	 */
	public Account getRedactor() {
		return redactor;
	}

	/**
	 * Sets redactor.
	 *
	 * @param redactor the redactor
	 */
	public void setRedactor(Account redactor) {
		this.redactor = redactor;
	}

	/**
	 * Gets assigned reviewers.
	 *
	 * @return the assigned reviewers
	 */
	public Set<Account> getAssignedReviewers() {
		return assignedReviewers;
	}

	/**
	 * Sets assigned reviewers.
	 *
	 * @param assignedReviewers the assigned reviewers
	 */
	public void setAssignedReviewers(Set<Account> assignedReviewers) {
		this.assignedReviewers = assignedReviewers;
	}

	/**
	 * Gets article abstract.
	 *
	 * @return the article abstract
	 */
	public String getArticleAbstract() {
		return articleAbstract;
	}

	/**
	 * Sets article abstract.
	 *
	 * @param articleAbstract the article abstract
	 */
	public void setArticleAbstract(String articleAbstract) {
		this.articleAbstract = articleAbstract;
	}

	/**
	 * Gets decision.
	 *
	 * @return the decision
	 */
	public Decision getDecision() {
		return decision;
	}

	/**
	 * Sets decision.
	 *
	 * @param decision the decision
	 */
	public void setDecision(Decision decision) {
		this.decision = decision;
	}
}
