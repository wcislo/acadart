package pl.lodz.p.acadart.models;

/**
 * Created by jacek on 02.05.2016.
 */
public enum ArticleStatus {

    /**
     * Pending article status.
     */
    PENDING("enums.pending"),

    /**
     * Assigned article status.
     */
    ASSIGNED("enums.assigned"),

    /**
     * Reviewed article status.
     */
    REVIEWED("enums.reviewed"),

    /**
     * Processed article status.
     */
    PROCESSED("enums.processed");

    /**
     * The Display name.
     */
    private final String displayName;

    /**
     * Instantiates a new Title.
     *
     * @param displayName the display name
     */
    ArticleStatus(String displayName){
        this.displayName=displayName;
    }

    /**
     * Gets display name.
     *
     * @return the display name
     */
    public String getDisplayName() {
        return displayName;
    }
}
