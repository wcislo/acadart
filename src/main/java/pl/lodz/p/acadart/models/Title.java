package pl.lodz.p.acadart.models;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by jacek on 20.02.2016.
 */
public  enum Title {
    /**
     * Mr title.
     */
    MR("enums.mr"),
    /**
     * Mrs title.
     */
    MRS("enums.mrs"),
    /**
     * Dr title.
     */
    DR("enums.dr"),
    /**
     * Prof title.
     */
    PROF("enums.prof"),
    /**
     * Mgr title.
     */
    MGR("enums.mgr");

    /**
     * The Display name.
     */
    private final String displayName;

    /**
     * Instantiates a new Title.
     *
     * @param displayName the display name
     */
    Title(String displayName){
        this.displayName=displayName;
    }

    /**
     * Gets display name.
     *
     * @return the display name
     */
    public String getDisplayName() {
        return displayName;
    }

}
