package pl.lodz.p.acadart.models;

import org.springframework.security.core.userdetails.User;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by jacek on 21.03.2016.
 */
@Entity
public class VerificationToken {

    /**
     * The Id verification token.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_verification_token")
    private Long idVerificationToken;

    /**
     * The Token.
     */
    @Column(nullable=false)
    private String token;

    /**
     * The Verified.
     */
    @Column(nullable=false)
    private Boolean verified;

    /**
     * The Account.
     */
    @OneToOne(targetEntity = Account.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "id_account")
    private Account account;

    /**
     * The Expiry date.
     */
    @Column(nullable=false)
    private Date expiryDate;


    /**
     * Gets id verification token.
     *
     * @return the id verification token
     */
    public Long getIdVerificationToken() {
        return idVerificationToken;
    }

    /**
     * Sets id verification token.
     *
     * @param idVerificationToken the id verification token
     */
    public void setIdVerificationToken(Long idVerificationToken) {
        this.idVerificationToken = idVerificationToken;
    }

    /**
     * Gets token.
     *
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * Sets token.
     *
     * @param token the token
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * Gets verified.
     *
     * @return the verified
     */
    public Boolean getVerified() {
        return verified;
    }

    /**
     * Sets verified.
     *
     * @param verified the verified
     */
    public void setVerified(Boolean verified) {
        this.verified = verified;
    }

    /**
     * Gets account.
     *
     * @return the account
     */
    public Account getAccount() {
        return account;
    }

    /**
     * Sets account.
     *
     * @param account the account
     */
    public void setAccount(Account account) {
        this.account = account;
    }

    /**
     * Gets expiry date.
     *
     * @return the expiry date
     */
    public Date getExpiryDate() {
        return expiryDate;
    }

    /**
     * Sets expiry date.
     *
     * @param expiryDate the expiry date
     */
    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }
}

