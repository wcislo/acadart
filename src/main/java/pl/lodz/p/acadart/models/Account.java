/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.acadart.models;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * The type Account.
 *
 * @author jacek.wcislo
 */
@Entity
@Table(name = "account")
public class Account implements  UserDetails {

    /**
     * The Id account.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_account")
    private Long idAccount;

    /**
     * The Login.
     */
    @Column(nullable = false, length = 64, unique=true)
    private String login;

    /**
     * The Password.
     */
    @Column(nullable = false, length = 64)
    private String password;

    /**
     * The Email.
     */
    @Column(nullable = false, length = 254, unique=true)
    private String email;

    /**
     * The Name.
     */
    @Column(nullable = false, length = 64)
    private String name;

    /**
     * The Surname.
     */
    @Column(nullable = false, length = 64)
    private String surname;

    /**
     * The Title.
     */
    @Enumerated(EnumType.STRING)
    @Column(length=4, nullable=false)
    private Title title;

    /**
     * The Affliction.
     */
    @Column(nullable = false, length = 128)
    private String affiliation;

    /**
     * The Address.
     */
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(nullable = false, name = "id_address", unique = true)
    private Address address;

    /**
     * The Authorities.
     */
    @ElementCollection(targetClass = Authority.class, fetch=FetchType.EAGER)
    @Enumerated(EnumType.STRING)
    @CollectionTable(name = "account_authority",
            joinColumns = @JoinColumn(name = "id_account"))
    @Column(name="authority", nullable=false, length=16)
    private Set<Authority> authorities = new HashSet<>();

    /**
     * The Articles.
     */
    @OneToMany(cascade = {CascadeType.MERGE}, mappedBy = "author")
    private Set<Article> articles = new HashSet<Article>();

    @OneToMany(cascade = {CascadeType.MERGE}, mappedBy ="redactor")
    private Set<Article> redactorsArticles = new HashSet<>();

    @ManyToMany(cascade = {CascadeType.MERGE}, mappedBy="assignedReviewers")
    private Set<Article> reviewersArticles = new HashSet<>();

    /**
     * The Reviews.
     */
    @OneToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, mappedBy = "reviewer")
    private Set<Review> reviews = new HashSet<Review>();

    /**
     * The Reviewers.
     */
    @ManyToMany(fetch = FetchType.LAZY, cascade={CascadeType.MERGE})
    @JoinTable(name = "reviewer_redactor", joinColumns = {
            @JoinColumn(name = "redactor", referencedColumnName = "id_account", nullable = false)}, inverseJoinColumns = {
            @JoinColumn(name = "reviewer", referencedColumnName = "id_account", nullable = false)})
    private Set<Account> reviewers = new HashSet<Account>();

    /**
     * The Redactors.
     */
    @ManyToMany(mappedBy = "reviewers", fetch = FetchType.LAZY, cascade={CascadeType.MERGE})
    private Set<Account> redactors = new HashSet<Account>();

    /**
     * The Is enabled.
     */
    @Column(nullable = false)
    private Boolean isEnabled = false;

    @Column(nullable = false)
    private Boolean isAccountNonLocked = true;

    /**
     * Instantiates a new Account.
     */
    public Account() {
        address = new Address();
    }

    /**
     * Instantiates a new Account.
     *
     * @param idAccount the id account
     */
    public Account(Long idAccount) {
        this.idAccount = idAccount;
    }

    /**
     * Instantiates a new Account.
     *
     * @param idAccount the id account
     * @param login     the login
     * @param password  the password
     * @param email     the email
     * @param name      the name
     * @param surname   the surname
     */
    public Account(Long idAccount, String login, String password, String email, String name, String surname) {
        this.idAccount = idAccount;
        this.login = login;
        this.password = password;
        this.email = email;
        this.name = name;
        this.surname = surname;
        this.isEnabled = false;
    }

    /**
     * Gets id account.
     *
     * @return the id account
     */
    public Long getIdAccount() {
        return idAccount;
    }

    /**
     * Gets login.
     *
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * Sets login.
     *
     * @param login the login
     */
    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    /**
     * Sets password.
     *
     * @param password the password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets email.
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets email.
     *
     * @param email the email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets surname.
     *
     * @return the surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Sets surname.
     *
     * @param surname the surname
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * Gets address.
     *
     * @return the address
     */
    public Address getAddress() {
        return address;
    }

    /**
     * Sets address.
     *
     * @param address the address
     */
    public void setAddress(Address address) {
        this.address = address;
    }

    /**
     * Gets affliction.
     *
     * @return the affliction
     */
    public String getAffiliation() {
        return affiliation;
    }

    /**
     * Sets affliction.
     *
     * @param affliction the affliction
     */
    public void setAffiliation(String affliction) {
        this.affiliation = affliction;
    }

    /**
     * Gets title.
     *
     * @return the title
     */
    public Title getTitle() {
        return title;
    }

    /**
     * Sets title.
     *
     * @param title the title
     */
    public void setTitle(Title title) {
        this.title = title;
    }


    /**
     * Gets reviewers.
     *
     * @return the reviewers
     */
    public Set<Account> getReviewers() {
        return reviewers;
    }

    /**
     * Sets reviewers.
     *
     * @param reviewers the reviewers
     */
    public void setReviewers(Set<Account> reviewers) {
        this.reviewers = reviewers;
    }

    /**
     * Gets redactors.
     *
     * @return the redactors
     */
    public Set<Account> getRedactors() {
        return redactors;
    }

    /**
     * Sets redactors.
     *
     * @param redactors the redactors
     */
    public void setRedactors(Set<Account> redactors) {
        this.redactors = redactors;
    }

    /**
     * Gets articles.
     *
     * @return the articles
     */
    public Set<Article> getArticles() {
        return articles;
    }

    /**
     * Sets articles.
     *
     * @param articles the articles
     */
    public void setArticles(Set<Article> articles) {
        this.articles = articles;
    }

    /**
     * Gets reviews.
     *
     * @return the reviews
     */
    public Set<Review> getReviews() {
        return reviews;
    }

    /**
     * Sets reviews.
     *
     * @param reviews the reviews
     */
    public void setReviews(Set<Review> reviews) {
        this.reviews = reviews;
    }

    /**
     * Sets enabled.
     *
     * @param enabled the enabled
     */
    public void setEnabled(Boolean enabled) {
        isEnabled = enabled;
    }

    /**
     * Sets authorities.
     *
     * @param authorities the authorities
     */
    public void setAuthorities(Set<Authority> authorities) {
        this.authorities = authorities;
    }

    /**
     * Gets redactors articles.
     *
     * @return the redactors articles
     */
    public Set<Article> getRedactorsArticles() {
        return redactorsArticles;
    }

    /**
     * Sets redactors articles.
     *
     * @param redactorsArticles the redactors articles
     */
    public void setRedactorsArticles(Set<Article> redactorsArticles) {
        this.redactorsArticles = redactorsArticles;
    }

    /**
     * Gets reviewers articles.
     *
     * @return the reviewers articles
     */
    public Set<Article> getReviewersArticles() {
        return reviewersArticles;
    }

    /**
     * Sets reviewers articles.
     *
     * @param reviewersArticles the reviewers articles
     */
    public void setReviewersArticles(Set<Article> reviewersArticles) {
        this.reviewersArticles = reviewersArticles;
    }

    /**
     * Gets account non locked.
     *
     * @return the account non locked
     */
    public Boolean getAccountNonLocked() {
        return isAccountNonLocked;
    }

    /**
     * Sets account non locked.
     *
     * @param accountNonLocked the account non locked
     */
    public void setAccountNonLocked(Boolean accountNonLocked) {
        isAccountNonLocked = accountNonLocked;
    }

    /**
     * Sets id account.
     *
     * @param idAccount the id account
     */
    public void setIdAccount(Long idAccount) {
        this.idAccount = idAccount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Account account = (Account) o;

        if (!login.equals(account.login)) return false;
        return email.equals(account.email);

    }

    @Override
    public int hashCode() {
        int result = login.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Account{" +
                "idAccount=" + idAccount +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'';
    }

    @Override
    public Set<Authority> getAuthorities() {
        return this.authorities;
    }

    @Override
    public String getUsername() {
        return this.login;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return isAccountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return isEnabled;
    }


}
