package pl.lodz.p.acadart.models;

import javax.persistence.*;

/**
 * Created by jacek on 11.04.2016.
 */
@Entity
public class UploadedFile {

    /**
     * The Id uploaded file.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_file")
    private Long idUploadedFile;

    /**
     * The File name.
     */
    @Column(nullable=false)
    private String fileName;

    /**
     * The File.
     */
    @Lob
    @Column(nullable=false)
    private byte[] file;


    /**
     * Gets id uploaded file.
     *
     * @return the id uploaded file
     */
    public Long getIdUploadedFile() {
        return idUploadedFile;
    }

    /**
     * Gets file name.
     *
     * @return the file name
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Sets file name.
     *
     * @param fileName the file name
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * Get file byte [ ].
     *
     * @return the byte [ ]
     */
    public byte[] getFile() {
        return file;
    }

    /**
     * Sets file.
     *
     * @param file the file
     */
    public void setFile(byte[] file) {
        this.file = file;
    }
}
