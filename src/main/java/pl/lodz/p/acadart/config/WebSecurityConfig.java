package pl.lodz.p.acadart.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import pl.lodz.p.acadart.security.CustomAuthenticationFailureHandler;
import pl.lodz.p.acadart.security.CustomAuthenticationSuccessHandler;
import pl.lodz.p.acadart.services.AccountService;
import pl.lodz.p.acadart.utils.EncodingUtils;

import javax.annotation.Resource;

/**
 * The type Web security config.
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private AccountService accountServiceImpl;

    @Resource
    private CustomAuthenticationSuccessHandler customAuthenticationSuccessHandler;

    @Resource
    private CustomAuthenticationFailureHandler customAuthenticationFailureHandler;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/css/**", "/fonts/**", "/js/**").permitAll()
                .antMatchers("/registration", "/login", "/registrationConfirm").anonymous()
                .antMatchers("/admin/**").hasAuthority("ADMINISTRATOR")
                .antMatchers("/reviewer/**").hasAuthority("REVIEWER")
                .antMatchers("/author/**").hasAuthority("AUTHOR")
                .antMatchers("/redactor/**").hasAuthority("REDACTOR")
                .anyRequest().authenticated()
                .and()
                .formLogin().loginPage("/login")
                .failureHandler(getCustomAuthenticationFailureHandler())
                .successHandler(getCustomAuthenticationSuccessHandler());
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(accountServiceImpl).passwordEncoder(EncodingUtils.passwordEncoder());
    }

    /**
     * Gets custom authentication success handler.
     *
     * @return the custom authentication success handler
     */
    public CustomAuthenticationSuccessHandler getCustomAuthenticationSuccessHandler() {
        return customAuthenticationSuccessHandler;
    }

    /**
     * Gets custom authentication failure handler.
     *
     * @return the custom authentication failure handler
     */
    public CustomAuthenticationFailureHandler getCustomAuthenticationFailureHandler() {
        return customAuthenticationFailureHandler;
    }
}