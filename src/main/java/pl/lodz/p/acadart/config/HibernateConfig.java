package pl.lodz.p.acadart.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.vendor.HibernateJpaSessionFactoryBean;

/**
 * Created by jacek on 25.04.2016.
 */
@Configuration
public class HibernateConfig {

    /**
     * Session factory hibernate jpa session factory bean.
     *
     * @return the hibernate jpa session factory bean
     */
    @Bean
    public HibernateJpaSessionFactoryBean sessionFactory() {
        return new HibernateJpaSessionFactoryBean();
    }

}
