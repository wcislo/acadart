package pl.lodz.p.acadart.controllers;

import org.dozer.Mapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import pl.lodz.p.acadart.dtos.AccountDTO;
import pl.lodz.p.acadart.dtos.ArticleDTO;
import pl.lodz.p.acadart.dtos.BaseAccountInformationDTO;
import pl.lodz.p.acadart.dtos.GenericDatatablesDTO;
import pl.lodz.p.acadart.forms.AssembleArticleForm;
import pl.lodz.p.acadart.forms.DecisionForm;
import pl.lodz.p.acadart.forms.ChooseReviewersForm;
import pl.lodz.p.acadart.models.ArticleStatus;
import pl.lodz.p.acadart.models.Authority;
import pl.lodz.p.acadart.services.AccountService;
import pl.lodz.p.acadart.services.ArticleService;
import pl.lodz.p.acadart.utils.SessionUtils;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by jacek on 02.05.2016.
 */
@Controller
public class RedactorController {

    @Resource
    private ArticleService articleService;

    @Resource
    private AccountService accountService;

    @Resource
    private Mapper mapper;

    /**
     * Gets articles.
     *
     * @return the articles
     */
    @RequestMapping(value = "/redactor/articles", method = RequestMethod.GET)
    public ModelAndView getArticles() {
        return new ModelAndView("redactor/articles");
    }

    /**
     * Gets redactors articles.
     *
     * @param params the params
     * @return the redactors articles
     */
    @ResponseBody
    @RequestMapping(value = "/redactor/articles/populate", method = RequestMethod.GET)
    public GenericDatatablesDTO<ArticleDTO> getRedactorsArticles(@RequestParam Map<String, String> params) {
        params.put("loginNotEquals", SessionUtils.getCurrentUsersLogin());
        params.put("redactorEquals", SessionUtils.getCurrentUsersLogin());
        params.put("statusNotEquals", ArticleStatus.PENDING.toString());
        GenericDatatablesDTO<ArticleDTO> articles = getArticleService().findArticlesByParams(params);
        Set<ArticleDTO> articleDTOSet = (LinkedHashSet<ArticleDTO>) articles.getData();
        articleDTOSet.stream().sorted((Comparator.comparing(ArticleDTO::getArticleStatus, Comparator.nullsLast(Comparator.naturalOrder())))).collect(Collectors.toCollection(LinkedHashSet::new));
        articles.setData(articleDTOSet);
        return articles;
    }

    /**
     * Gets my reviewers.
     *
     * @return the my reviewers
     */
    @RequestMapping(value = "/redactor/myReviewers", method = RequestMethod.GET)
    public ModelAndView getMyReviewers() {
        String redactorsLogin = SessionUtils.getCurrentUsersLogin();
        ModelAndView modelAndView = new ModelAndView("/redactor/myReviewers");
        modelAndView.addObject("chooseReviewersForm", getMapper().map(getAccountService().getAccountByLogin(redactorsLogin), ChooseReviewersForm.class));
        modelAndView.addObject("allReviewers", getAccountService().getAccountsByAuthority(Authority.REVIEWER).stream().filter(accDto -> !accDto.getLogin().equals(SessionUtils.getCurrentUsersLogin())).sorted((AccountDTO ac1, AccountDTO ac2) -> ac1.getSurname().compareTo(ac2.getSurname())).collect(Collectors.toCollection(LinkedHashSet::new)));
        return modelAndView;
    }

    /**
     * Post my reviewers model and view.
     *
     * @param chooseReviewersForm the choose reviewers form
     * @param result              the result
     * @param redirectAttributes  the redirect attributes
     * @return the model and view
     */
    @RequestMapping(value = "/redactor/myReviewers", method = RequestMethod.POST)
    public ModelAndView postMyReviewers(@Valid ChooseReviewersForm chooseReviewersForm, BindingResult result, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            ModelAndView modelAndView = new ModelAndView("/redactor/myReviewers");
            modelAndView.addObject("allReviewers", getAccountService().getAccountsByAuthority(Authority.REVIEWER).stream().filter(accDto -> !accDto.getLogin().equals(SessionUtils.getCurrentUsersLogin())).sorted((AccountDTO ac1, AccountDTO ac2) -> ac1.getSurname().compareTo(ac2.getSurname())).collect(Collectors.toCollection(LinkedHashSet::new)));
            return modelAndView;
        }
        getAccountService().updateUser(getMapper().map(chooseReviewersForm, AccountDTO.class));
        redirectAttributes.addFlashAttribute("redactorsChosenSuccess", true);
        return new ModelAndView(new RedirectView("/redactor/articles"));
    }

    /**
     * Gets assign pending articles.
     *
     * @param idArticle the id article
     * @param model     the model
     * @return the assign pending articles
     */
    @RequestMapping(value = "/redactor/articles/assign/{idArticle}", method = RequestMethod.GET)
    public ModelAndView getAssignPendingArticles(@PathVariable Long idArticle, Model model) {
        ModelAndView modelAndView = new ModelAndView("redactor/assignReviewers");
        AccountDTO accountDTO = getAccountService().getAccountByLogin(SessionUtils.getCurrentUsersLogin());
        modelAndView.addObject("redactorsReviewers", filterReviewerThatAddedArticle(accountDTO.getReviewers(), idArticle));
        if (!model.containsAttribute("chooseReviewersForm")) {
            modelAndView.addObject("chooseReviewersForm", getMapper().map(getArticleService().findById(idArticle), ChooseReviewersForm.class));
        }
        return modelAndView;
    }

    private Set<BaseAccountInformationDTO> filterReviewerThatAddedArticle(Set<BaseAccountInformationDTO> reviewersSet, Long idArticle) {
        ArticleDTO articleDTO = getArticleService().findById(idArticle);
        String authorsLogin = articleDTO.getAuthor().getLogin();
        return reviewersSet.stream().filter(b -> b.getLogin() != authorsLogin).collect(Collectors.toSet());
    }

    /**
     * Post assign pending articles model and view.
     *
     * @param chooseReviewersForm the choose reviewers form
     * @param result              the result
     * @param redirectAttributes  the redirect attributes
     * @return the model and view
     */
    @RequestMapping(value = "/redactor/articles/assign", method = RequestMethod.POST)
    public ModelAndView postAssignPendingArticles(@Valid ChooseReviewersForm chooseReviewersForm, BindingResult result, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.chooseReviewersForm", result);
            redirectAttributes.addFlashAttribute("chooseReviewersForm", chooseReviewersForm);
            return new ModelAndView(new RedirectView("/redactor/articles/assign/" + chooseReviewersForm.getIdArticle()));
        } else {
            ArticleDTO articleDTO = getMapper().map(chooseReviewersForm, ArticleDTO.class);
            AccountDTO accountDTO = getAccountService().getAccountByLogin(SessionUtils.getCurrentUsersLogin());
            articleDTO.setRedactor(accountDTO);
            articleDTO.setArticleStatus(ArticleStatus.ASSIGNED);
            getArticleService().updateArticle(articleDTO);
            redirectAttributes.addFlashAttribute("assignedSuccess", "assign.reviewer.success");
            return new ModelAndView(new RedirectView("/redactor/articles"));
        }
    }

    /**
     * Gets assemble article.
     *
     * @param idArticle          the id article
     * @param model              the model
     * @param redirectAttributes the redirect attributes
     * @return the assemble article
     */
    @RequestMapping(value = "/redactor/articles/assemble/{idArticle}", method = RequestMethod.GET)
    public ModelAndView getAssembleArticle(@PathVariable Long idArticle, Model model, RedirectAttributes redirectAttributes) {
        ArticleDTO articleDTO = getArticleService().findById(idArticle);
        if (articleDTO.getArticleStatus().equals(ArticleStatus.PROCESSED)) {
            redirectAttributes.addFlashAttribute("articleAlreadyAssembled");
            return new ModelAndView(new RedirectView("/redactor/articles"));
        }
        ModelAndView modelAndView = new ModelAndView("/redactor/assembleArticle");
        modelAndView.addObject("articleDTO", articleDTO);
        if (!model.containsAttribute("assembleArticleForm")) {
            AssembleArticleForm assembleArticleForm = new AssembleArticleForm();
            assembleArticleForm.setIdArticle(idArticle);
            modelAndView.addObject("assembleArticleForm", assembleArticleForm);
        }
        return modelAndView;
    }

    /**
     * Post assemble article model and view.
     *
     * @param assembleArticleForm the assemble article form
     * @param result              the result
     * @param redirectAttributes  the redirect attributes
     * @return the model and view
     */
    @RequestMapping(value = "/redactor/articles/assemble", method = RequestMethod.POST)
    public ModelAndView postAssembleArticle(@Valid AssembleArticleForm assembleArticleForm, BindingResult result, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.assembleArticleForm", result);
            redirectAttributes.addFlashAttribute("assembleArticleForm", assembleArticleForm);
            return new ModelAndView(new RedirectView("/redactor/articles/assemble/" + assembleArticleForm.getIdArticle()));
        }
        ArticleDTO articleDTO = getArticleService().findById(assembleArticleForm.getIdArticle());
        getMapper().map(assembleArticleForm, articleDTO);
        articleDTO.setArticleStatus(ArticleStatus.PROCESSED);
        getArticleService().updateArticle(articleDTO);
        redirectAttributes.addFlashAttribute("articleAssembled", true);
        return new ModelAndView(new RedirectView("/redactor/articles"));
    }


    /**
     * Gets article service.
     *
     * @return the article service
     */
    public ArticleService getArticleService() {
        return articleService;
    }

    /**
     * Gets account service.
     *
     * @return the account service
     */
    public AccountService getAccountService() {
        return accountService;
    }

    /**
     * Gets mapper.
     *
     * @return the mapper
     */
    public Mapper getMapper() {
        return mapper;
    }
}
