package pl.lodz.p.acadart.controllers;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.FlashMap;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by jacek.wcislo on 2016-06-15.
 */
@ControllerAdvice
public class ExceptionHandlingControllerAdvice {

    /**
     * Handle multipart exception redirect view.
     *
     * @param ex      the ex
     * @param request the request
     * @return the redirect view
     */
    @ExceptionHandler(MultipartException.class)
    public RedirectView handleMultipartException(MultipartException ex, HttpServletRequest request) {
        FlashMap outputFlashMap = RequestContextUtils.getOutputFlashMap(request);
        if (outputFlashMap != null) {
            outputFlashMap.put("fileTooBig", "upload.file.too.big");
        }
        return new RedirectView(request.getRequestURI());
    }
}
