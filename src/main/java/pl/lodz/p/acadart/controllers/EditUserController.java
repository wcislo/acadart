package pl.lodz.p.acadart.controllers;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import pl.lodz.p.acadart.dtos.AccountDTO;
import pl.lodz.p.acadart.forms.EditUserForm;
import pl.lodz.p.acadart.models.Authority;
import pl.lodz.p.acadart.services.AccountService;
import pl.lodz.p.acadart.utils.SessionUtils;
import pl.lodz.p.acadart.validators.EditUserFormValidator;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Created by jacek on 07.02.2016.
 */
@Controller
@RequestMapping(value = "/admin/")
public class EditUserController {

    @Resource
    private AccountService accountService;

    @Resource
    private EditUserFormValidator editUserFormValidator;

    @Resource
    private Mapper mapper;

    /**
     * Edit user model and view.
     *
     * @param login the login
     * @param model the model
     * @return the model and view
     */
    @RequestMapping(value = "/editUser/{login}", method = RequestMethod.GET)
    public ModelAndView editUser(@PathVariable String login, Model model) {
        ModelAndView modelAndView = new ModelAndView("admin/editUser");
        if (login.equals(SessionUtils.getCurrentUsersLogin())) {
            return new ModelAndView(new RedirectView("/account/myAccount"));
        }
        if (!model.containsAttribute("editUserForm")) {
            EditUserForm editUserForm = getMapper().map(getAccountService().getAccountByLogin(login), EditUserForm.class);
            modelAndView.addObject("editUserForm", editUserForm);
        }
        modelAndView.addObject("editMyAccount", false);
        modelAndView.addObject("isRedactor", getAccountService().getAccountByLogin(login).getAuthorities().contains(Authority.REDACTOR));
        modelAndView.addObject("isReviewer", getAccountService().getAccountByLogin(login).getAuthorities().contains(Authority.REVIEWER));
        modelAndView.addObject("allRedactors", getAccountService().getAccountsByAuthority(Authority.REDACTOR).stream().filter(accDto -> !accDto.getLogin().equals(login)).sorted((AccountDTO ac1, AccountDTO ac2) -> ac1.getSurname().compareTo(ac2.getSurname())).collect(Collectors.toCollection(LinkedHashSet::new)));
        modelAndView.addObject("allReviewers", getAccountService().getAccountsByAuthority(Authority.REVIEWER).stream().filter(accDto -> !accDto.getLogin().equals(login)).sorted((AccountDTO ac1, AccountDTO ac2) -> ac1.getSurname().compareTo(ac2.getSurname())).collect(Collectors.toCollection(LinkedHashSet::new)));
        return modelAndView;
    }

    /**
     * Edit user string.
     *
     * @param editUserForm       the edit user form
     * @param result             the result
     * @param redirectAttributes the redirect attributes
     * @return the string
     */
    @RequestMapping(value = "/editUser", method = RequestMethod.POST)
    public String editUser(@Valid EditUserForm editUserForm, BindingResult result, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.editUserForm", result);
            redirectAttributes.addFlashAttribute("editUserForm", editUserForm);
            return "redirect:/admin/editUser/" + editUserForm.getCurrentLogin();
        }
        getAccountService().updateUser(getMapper().map(editUserForm, AccountDTO.class));
        redirectAttributes.addFlashAttribute("successMessage", "account.edition.success");
        return "redirect:/admin/editUser/" + editUserForm.getLogin();
    }


    /**
     * Init binder.
     *
     * @param binder the binder
     */
    @InitBinder("editUserForm")
    public void initBinder(WebDataBinder binder) {
        binder.addValidators(getEditUserFormValidator());
    }

    /**
     * Gets edit user form validator.
     *
     * @return the edit user form validator
     */
    public EditUserFormValidator getEditUserFormValidator() {
        return editUserFormValidator;
    }

    /**
     * Gets account service.
     *
     * @return the account service
     */
    public AccountService getAccountService() {
        return accountService;
    }

    /**
     * Gets mapper.
     *
     * @return the mapper
     */
    public Mapper getMapper() {
        return mapper;
    }
}
