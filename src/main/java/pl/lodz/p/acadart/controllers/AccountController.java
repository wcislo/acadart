package pl.lodz.p.acadart.controllers;

import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import pl.lodz.p.acadart.dtos.AccountDTO;
import pl.lodz.p.acadart.forms.BaseAccountForm;
import pl.lodz.p.acadart.forms.ChangePasswordForm;
import pl.lodz.p.acadart.forms.EditUserForm;
import pl.lodz.p.acadart.models.Account;
import pl.lodz.p.acadart.services.AccountService;
import pl.lodz.p.acadart.utils.EncodingUtils;
import pl.lodz.p.acadart.utils.SessionUtils;
import pl.lodz.p.acadart.validators.BaseAccountFormValidator;
import pl.lodz.p.acadart.validators.ChangePasswordFormValidator;
import pl.lodz.p.acadart.validators.EditUserFormValidator;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;

/**
 * Created by jacek on 08.05.2016.
 */
@Controller
@RequestMapping(value = "/account")
public class AccountController {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Resource
    private AccountService accountService;

    @Resource
    private Mapper mapper;

    @Resource
    private BaseAccountFormValidator baseAccountFormValidator;

    @Resource
    private ChangePasswordFormValidator changePasswordFormValidator;

    /**
     * Account details model and view.
     *
     * @param login the login
     * @return the model and view
     */
    @RequestMapping(value = "/{login}", method = RequestMethod.GET)
    public ModelAndView accountDetails(@PathVariable String login) {
        AccountDTO accountDto = getAccountService().getAccountByLogin(login);
        ModelAndView modelAndView = new ModelAndView("fragments/detailsModal");
        modelAndView.addObject("accountDto", accountDto);
        return modelAndView;
    }

    /**
     * Account details popover model and view.
     *
     * @param login the login
     * @return the model and view
     */
    @RequestMapping(value = "/popover/{login}", method = RequestMethod.GET)
    public ModelAndView accountDetailsPopover(@PathVariable String login) {
        AccountDTO accountDto = getAccountService().getAccountByLogin(login);
        ModelAndView modelAndView = new ModelAndView("fragments/detailsPopover");
        modelAndView.addObject("accountDto", accountDto);
        return modelAndView;
    }

    /**
     * Gets my account.
     *
     * @return the my account
     */
    @RequestMapping(value = "/myAccount", method = RequestMethod.GET)
    public ModelAndView getMyAccount() {
        ModelAndView modelAndView = new ModelAndView("editMyAccount");
        modelAndView.addObject("editMyAccount", true);
        modelAndView.addObject("baseAccountForm", getMapper().map(getAccountService().getAccountByLogin(SessionUtils.getCurrentUsersLogin()), EditUserForm.class));
        return modelAndView;
    }

    /**
     * Post my account model and view.
     *
     * @param baseAccountForm    the base account form
     * @param bindingResult      the binding result
     * @param redirectAttributes the redirect attributes
     * @param request            the request
     * @param response           the response
     * @return the model and view
     */
    @RequestMapping(value = "/myAccount", method = RequestMethod.POST)
    public ModelAndView postMyAccount(@Valid BaseAccountForm baseAccountForm, BindingResult bindingResult, RedirectAttributes redirectAttributes, HttpServletRequest request, HttpServletResponse response) {
        ModelAndView modelAndView = new ModelAndView("editMyAccount");
        if (bindingResult.hasErrors()) {
            return modelAndView;
        }
        AccountDTO accountDTO = getAccountService().getAccountById(baseAccountForm.getIdAccount());
        getMapper().map(baseAccountForm, accountDTO);
        getAccountService().updateUser(accountDTO);
        if (getAccountService().getAccountByLogin(SessionUtils.getCurrentUsersLogin()) == null) {
            SessionUtils.invalidateSession(request);
            try {
                response.sendRedirect("/login");
            } catch (IOException e) {
                log.warn("Failed to redirect user to login page.", e);
            }
        }
        modelAndView.addObject("successMessage", "account.edition.success");
        return modelAndView;
    }

    /**
     * Gets change password.
     *
     * @return the change password
     */
    @RequestMapping(value = "/changePassword", method = RequestMethod.GET)
    public ModelAndView getChangePassword() {
        AccountDTO accountDto = getAccountService().getAccountByLogin(SessionUtils.getCurrentUsersLogin());
        ModelAndView modelAndView = new ModelAndView("changePassword");
        modelAndView.addObject("changePasswordForm", new ChangePasswordForm());
        return modelAndView;
    }

    /**
     * Post change password model and view.
     *
     * @param changePasswordForm the change password form
     * @param result             the result
     * @return the model and view
     */
    @RequestMapping(value = "/changePassword", method = RequestMethod.POST)
    public ModelAndView postChangePassword(@Valid ChangePasswordForm changePasswordForm, BindingResult result) {
        ModelAndView modelAndView = new ModelAndView("changePassword");
        if (result.hasErrors()) {
            return modelAndView;
        }
        AccountDTO accountDTO = getAccountService().getAccountByLogin(SessionUtils.getCurrentUsersLogin());
        accountDTO.setPassword(EncodingUtils.encrypt(changePasswordForm.getNewPassword()));
        getAccountService().updateUser(accountDTO);
        modelAndView.addObject("successMessage", "password.edition.success");
        return modelAndView;
    }

    /**
     * Init binder.
     *
     * @param binder the binder
     */
    @InitBinder("baseAccountForm")
    public void initBinder(WebDataBinder binder) {
        binder.addValidators(getBaseAccountFormValidator());
    }

    /**
     * Init binder change password.
     *
     * @param binder the binder
     */
    @InitBinder("changePasswordForm")
    public void initBinderChangePassword(WebDataBinder binder) {
        binder.addValidators(getChangePasswordFormValidator());
    }

    /**
     * Getter for property 'editUserFormValidator'.
     *
     * @return Value for property 'editUserFormValidator'.
     */
    public BaseAccountFormValidator getBaseAccountFormValidator() {
        return baseAccountFormValidator;
    }

    /**
     * Getter for property 'changePasswordFormValidator'.
     *
     * @return Value for property 'changePasswordFormValidator'.
     */
    public ChangePasswordFormValidator getChangePasswordFormValidator() {
        return changePasswordFormValidator;
    }

    /**
     * Gets account service.
     *
     * @return the account service
     */
    public AccountService getAccountService() {
        return accountService;
    }

    /**
     * Gets mapper.
     *
     * @return the mapper
     */
    public Mapper getMapper() {
        return mapper;
    }
}
