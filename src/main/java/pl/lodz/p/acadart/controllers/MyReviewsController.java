package pl.lodz.p.acadart.controllers;

import org.apache.tomcat.util.http.fileupload.FileUploadBase;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import pl.lodz.p.acadart.dtos.ArticleDTO;
import pl.lodz.p.acadart.dtos.GenericDatatablesDTO;
import pl.lodz.p.acadart.dtos.ReviewDTO;
import pl.lodz.p.acadart.events.ArticleReviewedEvent;
import pl.lodz.p.acadart.forms.AddReviewForm;
import pl.lodz.p.acadart.models.ArticleStatus;
import pl.lodz.p.acadart.models.Review;
import pl.lodz.p.acadart.services.AccountService;
import pl.lodz.p.acadart.services.ArticleService;
import pl.lodz.p.acadart.utils.SessionUtils;
import pl.lodz.p.acadart.validators.AddReviewFormValidator;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by jacek on 03.05.2016.
 */
@Controller
public class MyReviewsController {

    @Resource
    private ArticleService articleService;

    @Resource
    private AccountService accountService;

    @Resource
    private AddReviewFormValidator addReviewFormValidator;

    @Resource
    private Mapper mapper;

    @Autowired
    private ApplicationEventPublisher eventPublisher;


    /**
     * Gets my reviews.
     *
     * @return the my reviews
     */
    @RequestMapping(value = "/reviewer/myReviews", method = RequestMethod.GET)
    public ModelAndView getMyReviews() {
        return new ModelAndView("/reviewer/myReviews");
    }

    /**
     * Gets reviewers articles.
     *
     * @param draw   the draw
     * @param start  the start
     * @param length the length
     * @return the reviewers articles
     */
    @ResponseBody
    @RequestMapping(value = "/articles/reviewers", method = RequestMethod.GET)
    public GenericDatatablesDTO<ArticleDTO> getReviewersArticles(@RequestParam Integer draw, @RequestParam Integer start, @RequestParam Integer length) {
        return getArticleService().findReviewersAvailableArticlesByLogin(SessionUtils.getCurrentUsersLogin(), draw, start, length);
    }

    /**
     * Gets review.
     *
     * @param idArticle          the id article
     * @param redirectAttributes the redirect attributes
     * @param model              the model
     * @return the review
     */
    @RequestMapping(value = "/reviewer/review/{idArticle}", method = RequestMethod.GET)
    public ModelAndView getReview(@PathVariable Long idArticle, RedirectAttributes redirectAttributes, Model model) {
        if (getArticleService().isArticleReviewedByCurrentUser(SessionUtils.getCurrentUsersLogin(), idArticle)) {
            redirectAttributes.addFlashAttribute("articleAlreadyReviewed", "review.already.reviewed");
            return new ModelAndView(new RedirectView("/reviewer/myReviews"));
        }
        ModelAndView modelAndView = new ModelAndView("/reviewer/addReview");
        AddReviewForm addReviewForm = new AddReviewForm();
        addReviewForm.setIdArticle(idArticle);
        if (!model.containsAttribute("addReviewForm")) {
            modelAndView.addObject("addReviewForm", addReviewForm);
        }
        return modelAndView;
    }

    /**
     * Post review model and view.
     *
     * @param addReviewForm      the add review form
     * @param result             the result
     * @param redirectAttributes the redirect attributes
     * @return the model and view
     * @throws FileSizeLimitExceededException the file size limit exceeded exception
     */
    @RequestMapping(value = "/reviewer/review", method = RequestMethod.POST)
    public ModelAndView postReview(@Valid AddReviewForm addReviewForm, BindingResult result, RedirectAttributes redirectAttributes) throws FileUploadBase.FileSizeLimitExceededException {
        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.addReviewForm", result);
            redirectAttributes.addFlashAttribute("addReviewForm", addReviewForm);
            return new ModelAndView(new RedirectView("/reviewer/review/" + addReviewForm.getIdArticle()));
        }
        ReviewDTO reviewDTO = getMapper().map(addReviewForm, ReviewDTO.class);
        reviewDTO.setReviewer(getAccountService().getAccountByLogin(SessionUtils.getCurrentUsersLogin()));
        ArticleDTO articleDTO = getArticleService().findById(addReviewForm.getIdArticle());
        articleDTO.getReviews().add(reviewDTO);
        articleDTO = getArticleService().updateArticle(articleDTO);
        getEventPublisher().publishEvent(new ArticleReviewedEvent(articleDTO));
        redirectAttributes.addFlashAttribute("reviewSuccess", "review.added.success");
        return new ModelAndView(new RedirectView("/reviewer/myReviews"));
    }


    /**
     * Init binder.
     *
     * @param binder the binder
     */
    @InitBinder("addReviewForm")
    public void initBinder(WebDataBinder binder) {
        binder.addValidators(getAddReviewFormValidator());
    }


    /**
     * Gets article service.
     *
     * @return the article service
     */
    public ArticleService getArticleService() {
        return articleService;
    }

    /**
     * Gets account service.
     *
     * @return the account service
     */
    public AccountService getAccountService() {
        return accountService;
    }

    /**
     * Gets mapper.
     *
     * @return the mapper
     */
    public Mapper getMapper() {
        return mapper;
    }

    /**
     * Gets add review form validator.
     *
     * @return the add review form validator
     */
    public AddReviewFormValidator getAddReviewFormValidator() {
        return addReviewFormValidator;
    }

    /**
     * Gets event publisher.
     *
     * @return the event publisher
     */
    public ApplicationEventPublisher getEventPublisher() {
        return eventPublisher;
    }
}
