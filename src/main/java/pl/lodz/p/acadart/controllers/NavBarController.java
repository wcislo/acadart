package pl.lodz.p.acadart.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;


/**
 * The type Nav bar controller.
 */
@Controller
public class NavBarController {

    /**
     * Switch context model and view.
     *
     * @param selectedRole the selected role
     * @param session      the session
     * @return the model and view
     */
    @RequestMapping(value = "/switchContext", method = RequestMethod.GET)
    public ModelAndView switchContext(@RequestParam String selectedRole, HttpSession session) {
        ModelAndView modelAndView = new ModelAndView("fragments/sidebar");
        session.setAttribute("selectedRole", selectedRole);
        return modelAndView;
    }

    /**
     * Switch selector model and view.
     *
     * @return the model and view
     */
    @RequestMapping(value = "/switchSelector", method = RequestMethod.GET)
    public ModelAndView switchSelector() {
        ModelAndView modelAndView = new ModelAndView("fragments/roleSelector");
        return modelAndView;
    }

}
