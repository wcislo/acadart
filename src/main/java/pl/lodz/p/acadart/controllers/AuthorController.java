package pl.lodz.p.acadart.controllers;

import org.apache.tomcat.util.codec.binary.Base64;
import org.dozer.Mapper;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import pl.lodz.p.acadart.dtos.ArticleDTO;
import pl.lodz.p.acadart.dtos.GenericDatatablesDTO;
import pl.lodz.p.acadart.dtos.UploadedFileDTO;
import pl.lodz.p.acadart.forms.AddArticleForm;
import pl.lodz.p.acadart.forms.RegistrationForm;
import pl.lodz.p.acadart.models.ArticleStatus;
import pl.lodz.p.acadart.services.ArticleService;
import pl.lodz.p.acadart.services.UploadedFileService;
import pl.lodz.p.acadart.utils.SessionUtils;
import pl.lodz.p.acadart.validators.AddArticleFormValidator;
import pl.lodz.p.acadart.validators.RegistrationFormValidator;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.*;
import java.util.Map;

/**
 * Created by jacek on 10.04.2016.
 */
@Controller
@RequestMapping(value = "/author/myArticles")
public class AuthorController {

    @Resource
    private ArticleService articleService;

    @Resource
    private Mapper mapper;

    @Resource
    private AddArticleFormValidator addArticleFormValidator;

    /**
     * My articles model and view.
     *
     * @return the model and view
     */
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView myArticles() {
        ModelAndView modelAndView = new ModelAndView("/author/myArticles");
        modelAndView.addObject("authorContext", true);
        modelAndView.addObject("reviewerRedactorContext", false);
        return new ModelAndView();
    }

    /**
     * Populate datatable generic datatables dto.
     *
     * @param params the params
     * @return the generic datatables dto
     */
    @ResponseBody
    @RequestMapping(value = "/populate", method = RequestMethod.GET)
    public GenericDatatablesDTO<ArticleDTO> populateDatatable(@RequestParam Map<String, String> params) {
        params.put("login", SessionUtils.getCurrentUsersLogin());
        return getArticleService().findArticlesByParams(params);
    }

    /**
     * Gets add article.
     *
     * @return the add article
     */
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public ModelAndView getAddArticle() {
        ModelAndView modelAndView = new ModelAndView("/author/addArticle");
        modelAndView.addObject("addArticleForm", new AddArticleForm());
        return modelAndView;
    }

    /**
     * Add article model and view.
     *
     * @param addArticleForm the add article form
     * @param result         the result
     * @return the model and view
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ModelAndView addArticle(@Valid AddArticleForm addArticleForm, BindingResult result) {
        if (result.hasErrors()) {
            return new ModelAndView("/author/addArticle");
        }
        ArticleDTO articleDTO = new ArticleDTO(SessionUtils.getCurrentUsersLogin());
        getMapper().map(addArticleForm, articleDTO);
        articleDTO.setArticleStatus(ArticleStatus.PENDING);
        getArticleService().addArticle(articleDTO);
        ModelAndView modelAndView = new ModelAndView("/author/myArticles");
        modelAndView.addObject("messageArticleAdded", "add.article.added");
        return modelAndView;
    }


    /**
     * Init binder.
     *
     * @param binder the binder
     */
    @InitBinder("addArticleForm")
    public void initBinder(WebDataBinder binder) {
        binder.addValidators(getAddArticleFormValidator());
    }

    /**
     * Gets add article form validator.
     *
     * @return the add article form validator
     */
    public AddArticleFormValidator getAddArticleFormValidator() {
        return addArticleFormValidator;
    }

    /**
     * Gets article service.
     *
     * @return the article service
     */
    public ArticleService getArticleService() {
        return articleService;
    }


    /**
     * Gets mapper.
     *
     * @return the mapper
     */
    public Mapper getMapper() {
        return mapper;
    }


}
