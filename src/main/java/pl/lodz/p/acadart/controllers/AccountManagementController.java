package pl.lodz.p.acadart.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import pl.lodz.p.acadart.dtos.AccountDTO;
import pl.lodz.p.acadart.dtos.GenericDatatablesDTO;
import pl.lodz.p.acadart.services.AccountService;
import pl.lodz.p.acadart.services.ArticleService;

import javax.annotation.Resource;

/**
 * The type Account management controller.
 */
@Controller
@RequestMapping(value = "/admin/accountManagement")
public class AccountManagementController {

    @Resource
    private AccountService accountService;

    @Resource
    private ArticleService articleService;

    /**
     * User management model and view.
     *
     * @return the model and view
     */
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView userManagement() {
        ModelAndView modelAndView = new ModelAndView("admin/manageUsers");
        return modelAndView;
    }

    /**
     * Gets paginated accounts.
     *
     * @param draw    the draw
     * @param start   the start
     * @param length  the length
     * @param login   the login
     * @param name    the name
     * @param surname the surname
     * @param email   the email
     * @return the paginated accounts
     */
    @ResponseBody
    @RequestMapping(value = "/account", method = RequestMethod.GET)
    public GenericDatatablesDTO<AccountDTO> getPaginatedAccounts(@RequestParam Integer draw, @RequestParam Integer start, @RequestParam Integer length, @RequestParam(required = false) String login, @RequestParam(required = false) String name, @RequestParam(required = false) String surname, @RequestParam(required = false) String email) {
        return getAccountService().getPaginatedAndFilteredAccounts(draw, start, length, name, surname, email, login);
    }


    /**
     * Gets account service.
     *
     * @return the account service
     */
    public AccountService getAccountService() {
        return accountService;
    }

    /**
     * Gets article service.
     *
     * @return the article service
     */
    public ArticleService getArticleService() {
        return articleService;
    }
}
