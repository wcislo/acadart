package pl.lodz.p.acadart.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.mail.MailException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import pl.lodz.p.acadart.dtos.AccountDTO;
import pl.lodz.p.acadart.events.OnRegistrationCompleteEvent;
import pl.lodz.p.acadart.forms.RegistrationForm;
import pl.lodz.p.acadart.models.Account;
import pl.lodz.p.acadart.models.VerificationToken;
import pl.lodz.p.acadart.services.AccountService;
import pl.lodz.p.acadart.services.VerificationTokenService;
import pl.lodz.p.acadart.validators.RegistrationFormValidator;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Calendar;
import java.util.Locale;

/**
 * The type Registration controller.
 */
@Controller
public class RegistrationController {

    @Resource
    private AccountService accountService;

    @Resource
    private RegistrationFormValidator registrationFormValidator;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private MessageSource messages;

    @Resource
    private VerificationTokenService verificationTokenService;

    /**
     * Registration form string.
     *
     * @param model the model
     * @return the string
     */
    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registrationForm(Model model) {
        model.addAttribute("registrationForm", new RegistrationForm());
        return "registration";
    }

    /**
     * Registration submit model and view.
     *
     * @param registrationForm   the registration form
     * @param result             the result
     * @param request            the request
     * @param redirectAttributes the redirect attributes
     * @return the model and view
     */
    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public ModelAndView registrationSubmit(@Valid RegistrationForm registrationForm,
                                           BindingResult result, HttpServletRequest request, RedirectAttributes redirectAttributes) {
        Locale locale = request.getLocale();
        if (result.hasErrors()) {
            return new ModelAndView("/registration");
        }
        AccountDTO registeredAccount = getAccountService().registerUser(registrationForm);
        try {
            String appUrl = request.getContextPath();
            eventPublisher.publishEvent(new OnRegistrationCompleteEvent
                    (registeredAccount, request.getLocale(), getApplicationAddress(request)));
        } catch (MailException me) {
            redirectAttributes.addFlashAttribute("messageError", messages.getMessage("registration.mail.error", null, locale));
            return new ModelAndView(new RedirectView("/login"));
        }
        redirectAttributes.addFlashAttribute("message", messages.getMessage("registration.mail.success", null, locale));
        return new ModelAndView(new RedirectView("/login"));
    }

    /**
     * Confirm registration model and view.
     *
     * @param request            the request
     * @param token              the token
     * @param redirectAttributes the redirect attributes
     * @return the model and view
     */
    @RequestMapping(value = "/registrationConfirm", method = RequestMethod.GET)
    public ModelAndView confirmRegistration
            (WebRequest request, @RequestParam("token") String token, RedirectAttributes redirectAttributes) {
        Locale locale = request.getLocale();
        VerificationToken verificationToken = getVerificationTokenService().getVerificationToken(token);
        if (verificationToken == null) {
            redirectAttributes.addFlashAttribute("messageError", messages.getMessage("registration.invalidToken", null, locale));
            return new ModelAndView(new RedirectView("/login"));
        }
        Account account = verificationToken.getAccount();
        Calendar cal = Calendar.getInstance();
        if ((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            redirectAttributes.addFlashAttribute("messageError", messages.getMessage("registration.expired", null, locale));
            return new ModelAndView(new RedirectView("/login"));
        }
        account.setEnabled(true);
        getAccountService().saveRegisteredUser(account);
        redirectAttributes.addFlashAttribute("message", messages.getMessage("registration.confirmed", null, locale));
        return new ModelAndView(new RedirectView("/login"));
    }

    private String getApplicationAddress(HttpServletRequest request) {
        StringBuilder addressBuilder = new StringBuilder();
        addressBuilder.append(request.getScheme()).append("://")
                .append(request.getServerName()).append(":")
                .append(request.getServerPort()).append(request.getContextPath());
        return addressBuilder.toString();
    }

    /**
     * Init binder.
     *
     * @param binder the binder
     */
    @InitBinder("registrationForm")
    public void initBinder(WebDataBinder binder) {
        binder.addValidators(getRegistrationFormValidator());
    }

    /**
     * Gets account service.
     *
     * @return the account service
     */
    public AccountService getAccountService() {
        return accountService;
    }

    /**
     * Gets event publisher.
     *
     * @return the event publisher
     */
    public ApplicationEventPublisher getEventPublisher() {
        return eventPublisher;
    }

    /**
     * Gets messages.
     *
     * @return the messages
     */
    public MessageSource getMessages() {
        return messages;
    }

    /**
     * Gets registration form validator.
     *
     * @return the registration form validator
     */
    public RegistrationFormValidator getRegistrationFormValidator() {
        return registrationFormValidator;
    }

    /**
     * Gets verification token service.
     *
     * @return the verification token service
     */
    public VerificationTokenService getVerificationTokenService() {
        return verificationTokenService;
    }
}
