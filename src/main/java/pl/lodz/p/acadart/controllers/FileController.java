package pl.lodz.p.acadart.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import pl.lodz.p.acadart.dtos.UploadedFileDTO;
import pl.lodz.p.acadart.services.UploadedFileService;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

/**
 * Created by jacek on 26.04.2016.
 */
@Controller
public class FileController {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Resource
    private UploadedFileService uploadedFileService;

    /**
     * Show file preview page model and view.
     *
     * @param id the id
     * @return the model and view
     */
    @RequestMapping(value = "/file/preview/{id}", method = RequestMethod.GET)
    public ModelAndView showFilePreviewPage(@PathVariable Long id) {
        ModelAndView modelAndView = new ModelAndView("/fragments/filePreviewModal");
        modelAndView.addObject("idFile", id);
        return modelAndView;
    }

    /**
     * Show file content.
     *
     * @param id       the id
     * @param response the response
     */
    @RequestMapping(value = "/file/{id}", method = RequestMethod.GET)
    public void showFileContent(@PathVariable Long id, HttpServletResponse response) {
        UploadedFileDTO uploadedFileDTO = getUploadedFileService().findById(id);
        response.setContentType("application/pdf");
        byte[] file = uploadedFileDTO.getFile();
        if (file.length > 0) {
            response.setContentLength(file.length);
        }
        try {
            FileCopyUtils.copy(new ByteArrayInputStream(file), response.getOutputStream());
        } catch (IOException e) {
            log.warn("Uploading file " + uploadedFileDTO.getFileName() + " failed", e);
        }
    }

    /**
     * Download file.
     *
     * @param id       the id
     * @param response the response
     */
    @RequestMapping(value = "/file/download/{id}", method = RequestMethod.GET)
    public void downloadFile(@PathVariable Long id, HttpServletResponse response) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        UploadedFileDTO uploadedFileDTO = getUploadedFileService().findById(id);
        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", String.format("attachment; filename=\"" + uploadedFileDTO.getFileName() + "\""));
        response.setContentLength((int) uploadedFileDTO.getFile().length);
        InputStream inputStream = new BufferedInputStream(new ByteArrayInputStream(uploadedFileDTO.getFile()));
        try {
            FileCopyUtils.copy(inputStream, response.getOutputStream());
        } catch (IOException e) {
            log.warn("Downloading file " + uploadedFileDTO.getFileName() + " failed", e);
        }
    }

    /**
     * Gets uploaded file service.
     *
     * @return the uploaded file service
     */
    public UploadedFileService getUploadedFileService() {
        return uploadedFileService;
    }
}
