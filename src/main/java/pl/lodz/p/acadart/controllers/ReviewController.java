package pl.lodz.p.acadart.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.lodz.p.acadart.dtos.AccountDTO;
import pl.lodz.p.acadart.dtos.ArticleDTO;
import pl.lodz.p.acadart.dtos.BaseAccountInformationDTO;
import pl.lodz.p.acadart.dtos.ReviewDTO;
import pl.lodz.p.acadart.services.AccountService;
import pl.lodz.p.acadart.services.ArticleService;
import pl.lodz.p.acadart.services.ReviewService;
import pl.lodz.p.acadart.utils.SessionUtils;

import javax.annotation.Resource;
import javax.websocket.server.PathParam;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by jacek on 08.05.2016.
 */
@RestController
public class ReviewController {

    @Resource
    private ReviewService reviewService;

    @Resource
    private AccountService accountService;

    @Resource
    private ArticleService articleService;


    /**
     * Gets details.
     *
     * @param id the id
     * @return the details
     */
    @RequestMapping(value = "/review/details/{id}", method = RequestMethod.GET)
    public ModelAndView getDetails(@PathVariable Long id) {
        ModelAndView modelAndView = new ModelAndView("/fragments/reviewDetailsModal");
        modelAndView.addObject("reviewDto", getReviewService().getReviewById(id));
        return modelAndView;
    }

    /**
     * Gets details popover.
     *
     * @param id the id
     * @return the details popover
     */
    @RequestMapping(value = "/review/popover/{id}", method = RequestMethod.GET)
    public ModelAndView getDetailsPopover(@PathVariable Long id) {
        ModelAndView modelAndView = new ModelAndView("/fragments/reviewDetailsPopover");
        modelAndView.addObject("reviewDto", getReviewService().getReviewById(id));
        return modelAndView;
    }

    /**
     * Is reviewed by current user map.
     *
     * @param id the id
     * @return the map
     */
    @RequestMapping(value = "/review/isReviewed/{id}", method = RequestMethod.GET, produces = "application/json")
    public Map<String, Boolean> isReviewedByCurrentUser(@PathVariable Long id) {
        Map<String, Boolean> returnMap = new HashMap<>();
        returnMap.put("status", getArticleService().isArticleReviewedByCurrentUser(SessionUtils.getCurrentUsersLogin(), id));
        return returnMap;

    }

    /**
     * Gets review service.
     *
     * @return the review service
     */
    public ReviewService getReviewService() {
        return reviewService;
    }

    /**
     * Gets account service.
     *
     * @return the account service
     */
    public AccountService getAccountService() {
        return accountService;
    }

    /**
     * Gets article service.
     *
     * @return the article service
     */
    public ArticleService getArticleService() {
        return articleService;
    }
}
