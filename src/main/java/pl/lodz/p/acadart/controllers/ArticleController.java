package pl.lodz.p.acadart.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import pl.lodz.p.acadart.dtos.ArticleDTO;
import pl.lodz.p.acadart.services.ArticleService;

import javax.annotation.Resource;

/**
 * Created by jacek on 08.05.2016.
 */
@Controller
@RequestMapping(value = "/article")
public class ArticleController {

    @Resource
    private ArticleService articleService;

    /**
     * Show article details model and view.
     *
     * @param id       the id
     * @param author   the author
     * @param reviewer the reviewer
     * @return the model and view
     */
    @RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
    public ModelAndView showArticleDetails(@PathVariable Long id, @RequestParam(value = "author") Boolean author, @RequestParam(value = "reviewer") Boolean reviewer) {
        ModelAndView modelAndView = new ModelAndView("/fragments/articleDetailsModal");
        modelAndView.addObject("authorContext", author);
        modelAndView.addObject("reviewerContext", reviewer);
        modelAndView.addObject("articleDto", getArticleService().findById(id));
        return modelAndView;
    }

    /**
     * Article details popover model and view.
     *
     * @param id the id
     * @return the model and view
     */
    @RequestMapping(value = "/articlePopover/{id}", method = RequestMethod.GET)
    public ModelAndView articleDetailsPopover(@PathVariable Long id) {
        ArticleDTO articleDto = getArticleService().findById(id);
        ModelAndView modelAndView = new ModelAndView("fragments/articleDetailsPopover");
        modelAndView.addObject("articleDto", articleDto);
        return modelAndView;
    }

    /**
     * Gets article service.
     *
     * @return the article service
     */
    public ArticleService getArticleService() {
        return articleService;
    }
}
