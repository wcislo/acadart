package pl.lodz.p.acadart.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import pl.lodz.p.acadart.forms.RegistrationForm;
import pl.lodz.p.acadart.services.AccountService;

import javax.annotation.Resource;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * The type Registration form validator.
 */
@Component
public class RegistrationFormValidator implements Validator {

	private Pattern pattern;
	private Matcher matcher;

	@Resource
	private AccountService accountService;

	@Override
	public boolean supports(Class<?> clazz) {
		return RegistrationForm.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		RegistrationForm form = (RegistrationForm) target;
		if (accountService.getAccountByLogin(form.getLogin()) != null) {
			errors.rejectValue("login", "validation.login.exists");
		}
		if (accountService.getAccountByEmail(form.getEmail()) != null) {
			errors.rejectValue("email", "validation.email.exists");
		}
		String password = form.getPassword();
		if (password!=null && !password.equals(form.getSecondPassword())) {
			errors.rejectValue("secondPassword", "validation.password.no_match");
		}
		pattern = Pattern.compile(ValidationConstants.EMAIL_PATTERN);
		matcher = pattern.matcher(form.getEmail());
		if (!matcher.matches()) {
			errors.rejectValue("email", "validation.email.no_match");
		}

	}

}
