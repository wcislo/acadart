package pl.lodz.p.acadart.validators;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import pl.lodz.p.acadart.dtos.AccountDTO;
import pl.lodz.p.acadart.forms.ChangePasswordForm;
import pl.lodz.p.acadart.services.AccountService;
import pl.lodz.p.acadart.utils.EncodingUtils;
import pl.lodz.p.acadart.utils.SessionUtils;

import javax.annotation.Resource;

/**
 * Created by jacek on 14.06.2016.
 */
@Component
public class ChangePasswordFormValidator implements Validator {


    @Resource
    private AccountService accountService;

    @Override
    public boolean supports(Class<?> clazz) {
        return ChangePasswordForm.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ChangePasswordForm changePasswordForm = (ChangePasswordForm) target;
        AccountDTO accountDTO = getAccountService().getAccountByLogin(SessionUtils.getCurrentUsersLogin());
        if(accountDTO!=null) {
            String currentEncodedPassword = accountDTO.getPassword();
            if (currentEncodedPassword!=null && !EncodingUtils.passwordMatch(changePasswordForm.getRetypeCurrentPassword(), currentEncodedPassword)) {
                errors.rejectValue("retypeCurrentPassword", "validation.password.wrong.password");
            }
            if (currentEncodedPassword!=null && EncodingUtils.passwordMatch(changePasswordForm.getNewPassword(), currentEncodedPassword)) {
                errors.rejectValue("newPassword", "validation.password.same");
            }
            if (currentEncodedPassword!=null && !changePasswordForm.getNewPassword().equals(changePasswordForm.getConfirmNewPassword())) {
                errors.rejectValue("newPassword", "validation.password.confirm.same");
            }
        }
    }

    /**
     * Getter for property 'accountService'.
     *
     * @return Value for property 'accountService'.
     */
    public AccountService getAccountService() {
        return accountService;
    }
}
