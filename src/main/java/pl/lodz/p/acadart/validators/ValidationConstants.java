package pl.lodz.p.acadart.validators;

/**
 * The type Validation constants.
 */
class ValidationConstants {

    final static String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    final static String SUPPORTED_FILE_FORMAT = "application/pdf";

    static final Integer BYTES_IN_MEGABYTE=1024 * 1024;
}
