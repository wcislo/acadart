package pl.lodz.p.acadart.validators;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import pl.lodz.p.acadart.forms.BaseAccountForm;
import pl.lodz.p.acadart.forms.EditUserForm;
import pl.lodz.p.acadart.forms.RegistrationForm;
import pl.lodz.p.acadart.services.AccountService;

import javax.annotation.Resource;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by jacek on 01.05.2016.
 */
@Component(value="editUserFormValidator")
public class EditUserFormValidator extends BaseAccountFormValidator implements Validator{




    @Override
    public boolean supports(Class<?> clazz) {
        return EditUserForm.class.isAssignableFrom(clazz);
    }

    @Resource
    private AccountService accountService;

    @Override
    public void validate(Object target, Errors errors) {
        EditUserForm editUserForm = (EditUserForm) target;
        super.validate(target, errors);
        if(CollectionUtils.isEmpty(editUserForm.getAuthorities())){
            errors.rejectValue("authorities", "validation.authorities.not.empty");
        }

    }

    public AccountService getAccountService() {
        return accountService;
    }
}
