package pl.lodz.p.acadart.validators;

import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.MultipartFile;
import pl.lodz.p.acadart.forms.AddReviewForm;
import pl.lodz.p.acadart.models.Decision;

import java.io.IOException;

/**
 * Created by jacek on 14.05.2016.
 */
@Component
public class AddReviewFormValidator implements Validator {

    private final org.slf4j.Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public boolean supports(Class<?> clazz) {
        return AddReviewForm.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        AddReviewForm addReviewForm = (AddReviewForm) target;
        if(addReviewForm!=null) {
            MultipartFile multipartFile = addReviewForm.getMultipartFile();
            if (multipartFile != null && multipartFile.isEmpty() && !addReviewForm.getDecision().equals(Decision.REFUSE_REVIEWING)) {
                errors.rejectValue("multipartFile", "add.review.file.empty");
            }
            if (multipartFile != null && !multipartFile.isEmpty() && addReviewForm.getDecision().equals(Decision.REFUSE_REVIEWING)) {
                errors.rejectValue("multipartFile", "add.review.refuse.reviewing.file.not.empty");
            }
            String comment = addReviewForm.getComment();
            if (comment!=null && comment.isEmpty() && addReviewForm.getDecision().equals(Decision.REFUSE_REVIEWING)) {
                errors.rejectValue("comment", "add.review.refuse.reviewing.comment.empty");
            }
            Decision decision = addReviewForm.getDecision();
            if (decision!=null && decision.equals(Decision.REFUSE_REVIEWING) && multipartFile!=null && !addReviewForm.getMultipartFile().isEmpty()) {
                String contentType = multipartFile.getContentType();
                if (contentType == null || !contentType.equals(ValidationConstants.SUPPORTED_FILE_FORMAT)) {
                    errors.rejectValue("multipartFile", "add.review.error.wrong.file.format");
                }
                try {
                    byte[] bytes = multipartFile.getBytes();
                    if (bytes.length / ValidationConstants.BYTES_IN_MEGABYTE > 2)
                        errors.rejectValue("multipartFile", "add.review.error.wrong.file.format");
                } catch (IOException e) {
                    log.warn("Failed to retrieve byte array from multipart file", e);
                }
            }
        }
    }
}
