package pl.lodz.p.acadart.validators;

import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import pl.lodz.p.acadart.forms.AddArticleForm;

import java.io.IOException;

/**
 * Created by jacek on 11.04.2016.
 */
@Component
public class AddArticleFormValidator implements Validator {

    private final org.slf4j.Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public boolean supports(Class<?> clazz) {
        return AddArticleForm.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        AddArticleForm addArticleForm = (AddArticleForm) target;
        MultipartFile multipartFile = addArticleForm.getMultipartFile();
        if (multipartFile == null) {
            errors.rejectValue("multipartFile", "article.add.error.file.is.empty");
        } else {
            String contentType = multipartFile.getContentType();
            if (contentType == null || !contentType.equals(ValidationConstants.SUPPORTED_FILE_FORMAT)) {
                errors.rejectValue("multipartFile", "article.add.error.wrong.file.format");
            }
            try {
                byte[] bytes = multipartFile.getBytes();
                if (bytes.length / ValidationConstants.BYTES_IN_MEGABYTE > 2)
                    errors.rejectValue("multipartFile", "article.add.error.wrong.file.format");
            } catch (IOException e) {
                log.warn("Failed to retrieve byte array from multipart file", e);
            }
        }
    }
}
