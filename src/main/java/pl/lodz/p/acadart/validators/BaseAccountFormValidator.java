package pl.lodz.p.acadart.validators;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import pl.lodz.p.acadart.forms.AddReviewForm;
import pl.lodz.p.acadart.forms.BaseAccountForm;
import pl.lodz.p.acadart.services.AccountService;

import javax.annotation.Resource;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by jacek.wcislo on 2016-06-14.
 */
@Component
public class BaseAccountFormValidator implements Validator {

    @Resource
    private AccountService accountService;

    private Pattern pattern;
    private Matcher matcher;

    @Override
    public boolean supports(Class<?> clazz) {
        return  BaseAccountForm.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        BaseAccountForm baseAccountForm = (BaseAccountForm) target;
        String login = baseAccountForm.getLogin();
        if (login!=null && !login.equals(baseAccountForm.getCurrentLogin()) && getAccountService().getAccountByLogin(login) != null) {
            errors.rejectValue("login", "validation.login.exists");
        }
        String email = baseAccountForm.getEmail();
        if (email!=null && !email.equals(baseAccountForm.getCurrentEmail()) && getAccountService().getAccountByEmail(email) != null) {
            errors.rejectValue("email", "validation.email.exists");
        }
        pattern = Pattern.compile(ValidationConstants.EMAIL_PATTERN);
        matcher = pattern.matcher(baseAccountForm.getEmail());
        if (!matcher.matches()) {
            errors.rejectValue("email", "validation.email.no_match");
        }
    }

    /**
     * Getter for property 'accountService'.
     *
     * @return Value for property 'accountService'.
     */
    public AccountService getAccountService() {
        return accountService;
    }
}
