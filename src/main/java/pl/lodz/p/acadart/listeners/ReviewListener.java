package pl.lodz.p.acadart.listeners;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import pl.lodz.p.acadart.dtos.ArticleDTO;
import pl.lodz.p.acadart.events.ArticleReviewedEvent;
import pl.lodz.p.acadart.events.OnRegistrationCompleteEvent;
import pl.lodz.p.acadart.models.ArticleStatus;
import pl.lodz.p.acadart.services.ArticleService;

import javax.annotation.Resource;

/**
 * Created by jacek on 16.05.2016.
 */
@Component
public class ReviewListener implements ApplicationListener<ArticleReviewedEvent> {

    @Resource
    private ArticleService articleService;

    @Override
    public void onApplicationEvent(ArticleReviewedEvent event) {
        ArticleDTO articleDTO = event.getArticleDTO();
        if(articleDTO.getAssignedReviewers()!=null) {
            Integer reviewersCount = articleDTO.getAssignedReviewers().size();
            if(articleDTO.getReviews()!=null){
                Integer reviewsCount = articleDTO.getReviews().size();
                if(reviewersCount.equals(reviewsCount)){
                    articleDTO.setArticleStatus(ArticleStatus.REVIEWED);
                    getArticleService().updateArticle(articleDTO);
                }
            }

        }
    }

    /**
     * Gets article service.
     *
     * @return the article service
     */
    public ArticleService getArticleService() {
        return articleService;
    }

    /**
     * Setter for property 'articleService'.
     *
     * @param articleService Value to set for property 'articleService'.
     */
    public void setArticleService(ArticleService articleService) {
        this.articleService = articleService;
    }
}
