package pl.lodz.p.acadart.listeners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.MessageSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import pl.lodz.p.acadart.dtos.AccountDTO;
import pl.lodz.p.acadart.events.OnRegistrationCompleteEvent;
import pl.lodz.p.acadart.models.Account;
import pl.lodz.p.acadart.services.AccountService;
import pl.lodz.p.acadart.services.VerificationTokenService;

import javax.annotation.Resource;
import java.util.UUID;

/**
 * Created by jacek on 21.03.2016.
 */
@Component
public class RegistrationListener implements ApplicationListener<OnRegistrationCompleteEvent> {

    private static final String REGISTRATION_CONFIRM_URL = "/registrationConfirm?token=";
    @Autowired
    private MessageSource messages;
    @Autowired
    private JavaMailSender mailSender;
    @Resource
    private VerificationTokenService verificationTokenService;

    @Override
    public void onApplicationEvent(OnRegistrationCompleteEvent event) {
        this.confirmRegistration(event);
    }

    private void confirmRegistration(OnRegistrationCompleteEvent event) {
        AccountDTO accountDto = event.getAccountDto();
        String token = UUID.randomUUID().toString();
        getVerificationTokenService().createVerificationToken(accountDto, token);
        String recipientAddress = accountDto.getEmail();
        String subject = "mail.subject";
        String confirmationUrl = event.getAppUrl() + REGISTRATION_CONFIRM_URL + token;
        String message = messages.getMessage("mail.confirm.registration", new String[]{accountDto.getSurname(), confirmationUrl}, event.getLocale());
        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(recipientAddress);
        email.setSubject(subject);
        email.setText(message);
        mailSender.send(email);
    }

    /**
     * Gets verification token service.
     *
     * @return the verification token service
     */
    public VerificationTokenService getVerificationTokenService() {
        return verificationTokenService;
    }
}
