package pl.lodz.p.acadart.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.lodz.p.acadart.models.Article;
import pl.lodz.p.acadart.models.ArticleStatus;

import java.util.Set;

/**
 * Created by jacek on 10.02.2016.
 */
@Repository(value="articleRepository")
@Transactional(propagation = Propagation.MANDATORY)
public interface ArticleRepository extends JpaRepository<Article, Long> {

}
