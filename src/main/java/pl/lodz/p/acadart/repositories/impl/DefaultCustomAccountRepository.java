package pl.lodz.p.acadart.repositories.impl;

import org.apache.commons.collections.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.lodz.p.acadart.models.Account;
import pl.lodz.p.acadart.models.Authority;
import pl.lodz.p.acadart.repositories.CustomAccountRepository;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;

/**
 * Created by jacek on 09.01.2016.
 */
@Repository(value = "customAccountRepository")
@Transactional(propagation = Propagation.MANDATORY)
public class DefaultCustomAccountRepository implements CustomAccountRepository{


   @Autowired
   private SessionFactory sessionFactory;

    @Override
    public Set<Account> getPaginatedFilteredAndOrderedByLoginAccounts(Integer start, Integer count, String name, String surname, String email, String login) {

        Criteria criteria = prepareCriteriaForFilteringAndPagination(name, surname, email, login);
        criteria.setFirstResult(start);
        criteria.setMaxResults(count);
        criteria.addOrder(Order.asc("login"));
       return new LinkedHashSet<Account>(criteria.list());
    }

    @Override
    public Long getFilteredAccountsCount(String name, String surname, String email, String login){
        Criteria criteria = prepareCriteriaForFilteringAndPagination(name, surname, email, login);
        return (Long) criteria.setProjection(Projections.rowCount()).uniqueResult();
    }

    private Criteria prepareCriteriaForFilteringAndPagination(String name, String surname, String email, String login){
        Criteria criteria = getSessionFactory().getCurrentSession().createCriteria(Account.class);

        if(StringUtils.isNotEmpty(name)){
            criteria.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE));
        }
        if(StringUtils.isNotEmpty(surname)){
            criteria.add(Restrictions.ilike("surname", surname, MatchMode.ANYWHERE));
        }
        if(StringUtils.isNotEmpty(email)){
            criteria.add(Restrictions.ilike("email", email, MatchMode.ANYWHERE));
        }
        if(StringUtils.isNotEmpty(login)){
            criteria.add(Restrictions.like("login", login, MatchMode.ANYWHERE));
        }
        return criteria;
    }

    @Override
    public Set<Account> getAccountsByAuthority(Authority authority){
        Query query = getSessionFactory().getCurrentSession().createQuery("select a from Account a where :authority member of a.authorities");
        query.setParameter("authority", authority);
        return new HashSet<>(query.list());
    }

    /**
     * Gets session factory.
     *
     * @return the session factory
     */
    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
