package pl.lodz.p.acadart.repositories;

import pl.lodz.p.acadart.models.Account;
import pl.lodz.p.acadart.models.Authority;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Set;

/**
 * Created by jacek on 09.01.2016.
 */
public interface CustomAccountRepository {

    /**
     * Gets paginated filtered and ordered by login accounts.
     *
     * @param start   the start
     * @param count   the count
     * @param name    the name
     * @param surname the surname
     * @param email   the email
     * @param login   the login
     * @return the paginated filtered and ordered by login accounts
     */
    Set<Account> getPaginatedFilteredAndOrderedByLoginAccounts(Integer start, Integer count, String name, String surname, String email, String login);

    /**
     * Gets filtered accounts count.
     *
     * @param name    the name
     * @param surname the surname
     * @param email   the email
     * @param login   the login
     * @return the filtered accounts count
     */
    Long getFilteredAccountsCount(String name, String surname, String email, String login);

    /**
     * Gets accounts by authority.
     *
     * @param authority the authority
     * @return the accounts by authority
     */
    Set<Account> getAccountsByAuthority(Authority authority);
}