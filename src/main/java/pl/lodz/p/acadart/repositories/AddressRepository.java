package pl.lodz.p.acadart.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.lodz.p.acadart.models.Address;
import pl.lodz.p.acadart.models.Article;

/**
 * Created by jacek on 10.02.2016.
 */
@Repository(value="addressRepository")
@Transactional(propagation = Propagation.MANDATORY)
public interface AddressRepository extends JpaRepository<Address, Integer> {

    /**
     * Find by id address address.
     *
     * @param id the id
     * @return the address
     */
    Address findByIdAddress(Long id);
}
