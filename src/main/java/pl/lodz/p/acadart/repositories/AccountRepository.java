package pl.lodz.p.acadart.repositories;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.lodz.p.acadart.models.Account;

/**
 * The interface Account repository.
 */
@Repository(value="accountRepository")
@Transactional(propagation = Propagation.MANDATORY)
public interface AccountRepository extends JpaRepository<Account, Long> {

    /**
     * Find by login account.
     *
     * @param login the login
     * @return the account
     */
    public Account findByLogin(String login);

    /**
     * Find by email account.
     *
     * @param email the email
     * @return the account
     */
    public Account findByEmail(String email);

    /**
     * Gets account count.
     *
     * @return the account count
     */
    @Query(value = "SELECT COUNT(a) FROM Account a")
	public Integer getAccountCount();

}