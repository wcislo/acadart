package pl.lodz.p.acadart.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.lodz.p.acadart.models.Review;

/**
 * Created by jacek on 10.02.2016.
 */
@Repository
@Transactional(propagation = Propagation.MANDATORY)
public interface ReviewRepository extends JpaRepository<Review, Long> {

    /**
     * Find by id review review.
     *
     * @param id the id
     * @return the review
     */
    Review findByIdReview(Long id);
}
