package pl.lodz.p.acadart.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.lodz.p.acadart.models.Account;
import pl.lodz.p.acadart.models.VerificationToken;

/**
 * Created by jacek on 21.03.2016.
 */
@Repository
@Transactional(propagation = Propagation.MANDATORY)
public interface VerificationTokenRepository extends JpaRepository<VerificationToken, Long> {

    /**
     * Find by token verification token.
     *
     * @param token the token
     * @return the verification token
     */
    VerificationToken findByToken(String token);

    /**
     * Find by account verification token.
     *
     * @param account the account
     * @return the verification token
     */
    VerificationToken findByAccount(Account account);
}
