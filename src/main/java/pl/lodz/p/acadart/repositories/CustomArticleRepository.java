package pl.lodz.p.acadart.repositories;

import pl.lodz.p.acadart.models.Article;

import java.util.Map;
import java.util.Set;

/**
 * Created by jacek on 10.04.2016.
 */
public interface CustomArticleRepository {

  //  public Set<Article> findUsersArticlesByLogin(String login, Integer start, Integer count);

    /**
     * Find articles by params set.
     *
     * @param params the params
     * @return the set
     */
    Set<Article> findArticlesByParams(Map<String, String> params);

    /**
     * Count criteria by params integer.
     *
     * @param params the params
     * @return the integer
     */
    public Integer countCriteriaByParams(Map<String, String> params);

}
