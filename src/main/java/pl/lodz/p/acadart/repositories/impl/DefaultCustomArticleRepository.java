package pl.lodz.p.acadart.repositories.impl;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.*;
import org.hibernate.sql.JoinType;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.lodz.p.acadart.models.Account;
import pl.lodz.p.acadart.models.Article;
import pl.lodz.p.acadart.models.ArticleStatus;
import pl.lodz.p.acadart.repositories.ArticleRepository;
import pl.lodz.p.acadart.repositories.CustomArticleRepository;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by jacek on 10.04.2016.
 */
@Repository(value="customArticleRepository")
@Transactional(propagation = Propagation.MANDATORY)
public class DefaultCustomArticleRepository implements CustomArticleRepository {

    @Resource
    private SessionFactory sessionFactory;

    @Override
    public Set<Article> findArticlesByParams(Map<String, String> params) {
        Criteria criteria = prepareCriteriaFromParams(params);
        setPaginationOnCriteria(params, criteria);
        return new HashSet<>(criteria.list());
    }

    private Criteria prepareCriteriaFromParams(Map<String, String> params){
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Article.class);
        if(params.get("login")!=null){
            criteria.createAlias("author", "a");
            criteria.add(Restrictions.ilike("a.login", params.get("login")));
        }
        if(params.get("articleStatus")!=null){
            criteria.add(Restrictions.eq("articleStatus", ArticleStatus.valueOf(params.get("articleStatus"))));
        }
        if(params.get("loginNotEquals")!=null){
            criteria.createAlias("author", "a");
            criteria.add(Restrictions.ne("a.login", params.get("loginNotEquals")));
        }
        if(params.get("pending")!=null) {
            if(Boolean.parseBoolean(params.get("pending"))) {
                criteria.add(Restrictions.eq("articleStatus", ArticleStatus.PENDING));
            }else{
                criteria.add(Restrictions.ne("articleStatus", ArticleStatus.PENDING));
            }
        }
        if(params.get("reviewersLogin")!=null){
            Account account = new Account();
            account.setLogin(params.get("reviewersLogin"));
            Article article = new Article();
            article.getAssignedReviewers().add(account);
            criteria.add(Example.create(article));
        }
        if(params.get("redactorEquals")!=null){
            criteria.createAlias("redactor", "r", JoinType.LEFT_OUTER_JOIN);
            criteria.add(Restrictions.or(Restrictions.ilike("r.login", params.get("redactorEquals")), Restrictions.isNull("r.idAccount")));
        }
        return criteria;
    }

    private void setPaginationOnCriteria(Map<String, String> params, Criteria criteria){
        if(params.get("count")!=null){
            criteria.setMaxResults(Integer.parseInt(params.get("count")));
        }
        if(params.get("start")!=null){
            criteria.setFirstResult(Integer.parseInt(params.get("start")));
        }
    }

    @Override
    public Integer countCriteriaByParams(Map<String, String> params){
        Criteria criteria = prepareCriteriaFromParams(params);
        return criteria.setProjection(Projections.rowCount()).uniqueResult().hashCode();
    }


}
