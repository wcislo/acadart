package pl.lodz.p.acadart.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.lodz.p.acadart.models.UploadedFile;

/**
 * Created by jacek on 12.04.2016.
 */
@Repository(value="uploadedFileRepository")
@Transactional(propagation = Propagation.MANDATORY)
public interface UploadedFileRepository extends JpaRepository<UploadedFile, Long> {
}
