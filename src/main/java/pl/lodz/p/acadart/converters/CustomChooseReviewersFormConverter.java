package pl.lodz.p.acadart.converters;

import org.dozer.DozerConverter;
import org.dozer.Mapper;
import org.springframework.stereotype.Component;
import pl.lodz.p.acadart.dtos.ArticleDTO;
import pl.lodz.p.acadart.dtos.BaseAccountInformationDTO;
import pl.lodz.p.acadart.forms.ChooseReviewersForm;
import pl.lodz.p.acadart.services.AccountService;
import pl.lodz.p.acadart.services.ArticleService;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by jacek on 02.05.2016.
 */
@Component
public class CustomChooseReviewersFormConverter extends DozerConverter<ChooseReviewersForm, ArticleDTO> {

    /**
     * Instantiates a new Custom choose reviewers form converter.
     */
    public CustomChooseReviewersFormConverter() {
        super(ChooseReviewersForm.class, ArticleDTO.class);
    }

    @Resource
    private Mapper mapper;

    @Resource
    private AccountService accountService;

    @Resource
    private ArticleService articleService;

    @Override
    public ArticleDTO convertTo(ChooseReviewersForm source, ArticleDTO destination) {
        if (destination == null) {
            destination = getArticleService().findById(source.getIdArticle());
        }
        if (destination == null) {
            destination = new ArticleDTO();
        }
        Set<BaseAccountInformationDTO> baseAccountInformationDTOSet = new HashSet<>();
        for (String login : source.getReviewersLogins()) {
            baseAccountInformationDTOSet.add(getMapper().map(getAccountService().getAccountByLogin(login), BaseAccountInformationDTO.class));
        }
        destination.setAssignedReviewers(baseAccountInformationDTOSet);
        return destination;
    }

    @Override
    public ChooseReviewersForm convertFrom(ArticleDTO source, ChooseReviewersForm destination) {
        if (destination == null) {
            destination = new ChooseReviewersForm();
        }
        destination.setAssignedReviewers(source.getAssignedReviewers());
        destination.setReviewersLogins(source.getAssignedReviewers().stream().map(BaseAccountInformationDTO::getLogin).collect(Collectors.toSet()));
        destination.setIdArticle(source.getIdArticle());
        return destination;
    }

    /**
     * Gets mapper.
     *
     * @return the mapper
     */
    public Mapper getMapper() {
        return mapper;
    }

    /**
     * Gets account service.
     *
     * @return the account service
     */
    public AccountService getAccountService() {
        return accountService;
    }

    /**
     * Gets article service.
     *
     * @return the article service
     */
    public ArticleService getArticleService() {
        return articleService;
    }
}
