package pl.lodz.p.acadart.converters;

import org.dozer.DozerConverter;
import org.dozer.Mapper;
import org.springframework.stereotype.Component;
import pl.lodz.p.acadart.dtos.*;
import pl.lodz.p.acadart.models.*;
import pl.lodz.p.acadart.repositories.AccountRepository;
import pl.lodz.p.acadart.repositories.ArticleRepository;
import pl.lodz.p.acadart.repositories.ReviewRepository;
import pl.lodz.p.acadart.services.AccountService;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by jacek on 11.04.2016.
 */
@Component(value = "customArticleConverter")
public class CustomArticleConverter extends DozerConverter<Article, ArticleDTO> {

    @Resource
    private Mapper mapper;

    @Resource
    private AccountRepository accountRepository;

    @Resource
    private ReviewRepository reviewRepository;

    @Resource
    private ArticleRepository articleRepository;

    @Resource
    private AccountService accountService;


    /**
     * Instantiates a new Custom article converter.
     */
    public CustomArticleConverter() {
        super(Article.class, ArticleDTO.class);
    }

    @Override
    public ArticleDTO convertTo(Article source, ArticleDTO destination) {
        if (source != null) {
            destination = new ArticleDTO();
            if (source.getIdArticle() != null) {
                destination.setIdArticle(source.getIdArticle());
            }
            if (source.getTitle() != null) {
                destination.setTitle(source.getTitle());
            }
            if (source.getAuthor() != null) {
                destination.setAuthor(getMapper().map(source.getAuthor(), BaseAccountInformationDTO.class));
            }
            destination.setRedactorsComment(source.getRedactorsComment());
            destination.setAuthorsComment(source.getAuthorsComment());
            Set<Review> reviewsSet = source.getReviews();
            Set<ReviewDTO> reviewDTOSet = new HashSet<>();
            for (Review review : reviewsSet) {
                reviewDTOSet.add(getMapper().map(review, ReviewDTO.class));
            }
            destination.setReviews(reviewDTOSet);
            destination.setCreationDate(source.getCreationDate());
            UploadedFileDTO uploadedFileDTO = new UploadedFileDTO();
            UploadedFile uploadedFile = source.getUploadedFile();
            if (uploadedFile != null) {
                uploadedFileDTO.setFileName(uploadedFile.getFileName());
                uploadedFileDTO.setIdUploadedFile(uploadedFile.getIdUploadedFile());
            }
            destination.setUploadedFile(uploadedFileDTO);
            destination.setArticleStatus(source.getArticleStatus());
            if (source.getRedactor() != null) {
                destination.setRedactor(getMapper().map(source.getRedactor(), BaseAccountInformationDTO.class));
            }
            Set<BaseAccountInformationDTO> assignedReviewersDTOs = new HashSet<>();
            for (Account reviewersAccount : source.getAssignedReviewers()) {
                assignedReviewersDTOs.add(getMapper().map(reviewersAccount, BaseAccountInformationDTO.class));
            }
            destination.setAssignedReviewers(assignedReviewersDTOs);
            destination.setArticleAbstract(source.getArticleAbstract());
            destination.setArticleStatus(source.getArticleStatus());
            destination.setDecision(source.getDecision());
            return destination;
        }
        return null;
    }

    @Override
    public Article convertFrom(ArticleDTO source, Article destination) {

        if (destination == null) {
            destination = getArticleRepository().findOne(source.getIdArticle());
        }
        destination.setTitle(source.getTitle());
        Account author = getAccountRepository().findByLogin(source.getAuthor().getLogin());
        destination.setAuthor(author);
        destination.setAuthorsComment(source.getAuthorsComment());
        destination.setRedactorsComment(source.getRedactorsComment());
        destination.setCreationDate(source.getCreationDate());

        Set<Review> reviewSet = destination.getReviews();
        for (ReviewDTO reviewDTO : source.getReviews()) {
            Review review = null;
            if (reviewDTO != null && reviewDTO.getIdReview() != null) {
                review = getReviewRepository().findOne(reviewDTO.getIdReview());

            }
            if (review == null) {
                review = new Review();
                getMapper().map(reviewDTO, review);
            }
            reviewSet.add(review);
        }
        destination.setReviews(reviewSet);
        UploadedFileDTO uploadedFileDTO = source.getUploadedFile();
        UploadedFile uploadedFile = destination.getUploadedFile();
        if (uploadedFile == null && uploadedFileDTO != null && uploadedFileDTO.getFile() != null) {
            uploadedFile = new UploadedFile();
            getMapper().map(source.getUploadedFile(), uploadedFile);
            destination.setUploadedFile(uploadedFile);
        }

        destination.setArticleStatus(source.getArticleStatus());
        if (source.getRedactor() != null) {
            Account assignedRedactor = getAccountRepository().findByLogin(source.getRedactor().getLogin());
            assignedRedactor.getRedactorsArticles().add(destination);
            destination.setRedactor(assignedRedactor);
        }
        Set<Account> assignedReviewersSet = new HashSet<>();
        for (BaseAccountInformationDTO assignedReviewerDTO : source.getAssignedReviewers()) {
            Account assignedReviewer = getAccountRepository().findByLogin(assignedReviewerDTO.getLogin());
            assignedReviewer.getReviewersArticles().add(destination);
            assignedReviewersSet.add(assignedReviewer);
        }
        destination.setAssignedReviewers(assignedReviewersSet);
        destination.setArticleAbstract(source.getArticleAbstract());
        destination.setArticleStatus(source.getArticleStatus());
        destination.setDecision(source.getDecision());
        return destination;
    }

    /**
     * Gets mapper.
     *
     * @return the mapper
     */
    public Mapper getMapper() {
        return mapper;
    }

    /**
     * Gets account repository.
     *
     * @return the account repository
     */
    public AccountRepository getAccountRepository() {
        return accountRepository;
    }

    /**
     * Gets review repository.
     *
     * @return the review repository
     */
    public ReviewRepository getReviewRepository() {
        return reviewRepository;
    }

    /**
     * Gets article repository.
     *
     * @return the article repository
     */
    public ArticleRepository getArticleRepository() {
        return articleRepository;
    }

    /**
     * Gets account service.
     *
     * @return the account service
     */
    public AccountService getAccountService() {
        return accountService;
    }
}
