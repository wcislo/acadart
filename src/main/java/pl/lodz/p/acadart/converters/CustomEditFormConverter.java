package pl.lodz.p.acadart.converters;

import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerConverter;
import org.dozer.Mapper;
import org.springframework.stereotype.Component;
import pl.lodz.p.acadart.dtos.AccountDTO;
import pl.lodz.p.acadart.dtos.AddressDTO;
import pl.lodz.p.acadart.dtos.BaseAccountInformationDTO;
import pl.lodz.p.acadart.forms.EditUserForm;
import pl.lodz.p.acadart.services.AccountService;
import pl.lodz.p.acadart.utils.EncodingUtils;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by jacek on 30.04.2016.
 */
@Component(value = "customEditFormConverter")
public class CustomEditFormConverter extends DozerConverter<EditUserForm, AccountDTO> {

    @Resource
    private AccountService accountService;

    @Resource
    private Mapper mapper;

    /**
     * Instantiates a new Custom edit form converter.
     */
    public CustomEditFormConverter() {
        super(EditUserForm.class, AccountDTO.class);
    }

    @Override
    public AccountDTO convertTo(EditUserForm source, AccountDTO destination) {
        destination = getAccountService().getAccountById(source.getIdAccount());
        if (destination == null) {
            destination = new AccountDTO();
        }
        destination.setReviews(source.getReviews());
        destination.setLogin(source.getLogin());
        destination.setAuthorities(source.getAuthorities());
        AddressDTO addressDTO = destination.getAddress();
        if (addressDTO == null) {
            addressDTO = new AddressDTO();
        }
        addressDTO.setCountry(source.getCountry());
        addressDTO.setCity(source.getCity());
        addressDTO.setPostalCode(source.getPostalCode());
        addressDTO.setStreetNumberFlat(source.getStreetNumberFlat());
        destination.setAddress(addressDTO);
        destination.setTitle(source.getTitle());
        destination.setArticles(source.getArticles());
        destination.setName(source.getName());
        destination.setSurname(source.getSurname());
        destination.setAffiliation(source.getAffiliation());
        if (source.getEnabled() != null) {
            destination.setEnabled(source.getEnabled());
        }
        destination.setEmail(source.getEmail());
        if (source.getRedactorsLogins() == null) {
            source.setRedactorsLogins(new HashSet<>());
        }
        Set<BaseAccountInformationDTO> redactorsSet = new HashSet<>();
        for (String login : source.getRedactorsLogins()) {
            redactorsSet.add(getMapper().map(getAccountService().getAccountByLogin(login), BaseAccountInformationDTO.class));
        }
        destination.setRedactors(redactorsSet);
        if (source.getReviewersLogins() == null) {
            source.setReviewersLogins(new HashSet<>());
        }
        Set<BaseAccountInformationDTO> reviewersSet = new HashSet<>();
        for (String login : source.getReviewersLogins()) {
            reviewersSet.add(getMapper().map(getAccountService().getAccountByLogin(login), BaseAccountInformationDTO.class));
        }
        destination.setReviewers(reviewersSet);
        if (source.getAccountNonLocked() != null) {
            destination.setAccountNonLocked(source.getAccountNonLocked());
        }
        if (StringUtils.isNotEmpty(source.getNewPassword())) {
            destination.setPassword(EncodingUtils.encrypt(source.getNewPassword()));
        }
        return destination;
    }

    @Override
    public EditUserForm convertFrom(AccountDTO source, EditUserForm destination) {

        if (destination == null) {
            destination = new EditUserForm();
        }
        destination.setIdAccount(source.getIdAccount());
        destination.setArticles(source.getArticles());
        destination.setLogin(source.getLogin());
        destination.setCurrentLogin(source.getLogin());
        destination.setEnabled(source.getEnabled());
        destination.setName(source.getName());
        destination.setSurname(source.getSurname());
        destination.setEmail(source.getEmail());
        destination.setCurrentEmail(source.getEmail());
        destination.setAffiliation(source.getAffiliation());
        destination.setAuthorities(source.getAuthorities());
        destination.setReviews(source.getReviews());
        AddressDTO addressDTO = source.getAddress();
        destination.setCountry(addressDTO.getCountry());
        destination.setCity(addressDTO.getCity());
        destination.setPostalCode(addressDTO.getPostalCode());
        destination.setStreetNumberFlat(addressDTO.getStreetNumberFlat());
        destination.setTitle(source.getTitle());
        destination.setRedactors(source.getRedactors());
        destination.setReviewers(source.getReviewers());
        destination.setRedactorsLogins(source.getRedactors().stream().map(BaseAccountInformationDTO::getLogin).collect(Collectors.toSet()));
        destination.setReviewersLogins(source.getReviewers().stream().map(BaseAccountInformationDTO::getLogin).collect(Collectors.toSet()));
        destination.setAccountNonLocked(source.getAccountNonLocked());
        destination.setCurrentPassword(source.getPassword());
        return destination;
    }

    /**
     * Gets account service.
     *
     * @return the account service
     */
    public AccountService getAccountService() {
        return accountService;
    }

    /**
     * Gets mapper.
     *
     * @return the mapper
     */
    public Mapper getMapper() {
        return mapper;
    }
}
