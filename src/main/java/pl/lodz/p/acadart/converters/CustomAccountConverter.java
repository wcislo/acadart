package pl.lodz.p.acadart.converters;

import org.apache.commons.collections.CollectionUtils;
import org.dozer.DozerConverter;
import org.dozer.Mapper;
import org.springframework.stereotype.Component;
import pl.lodz.p.acadart.dtos.*;
import pl.lodz.p.acadart.models.Account;
import pl.lodz.p.acadart.models.Address;
import pl.lodz.p.acadart.models.Article;
import pl.lodz.p.acadart.models.Review;
import pl.lodz.p.acadart.repositories.AccountRepository;
import pl.lodz.p.acadart.services.AccountService;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by jacek on 30.04.2016.
 */
@Component(value = "customAccountConverter")
public class CustomAccountConverter extends DozerConverter<Account, AccountDTO> {

    /**
     * Instantiates a new Custom account converter.
     */
    public CustomAccountConverter() {
        super(Account.class, AccountDTO.class);
    }

    @Resource
    private AccountService accountService;

    @Resource
    private AccountRepository accountRepository;

    @Resource
    private Mapper mapper;

    @Override
    public AccountDTO convertTo(Account source, AccountDTO destination) {
        if (destination == null) {
            destination = new AccountDTO();
        }
        Set<ArticleDTO> articleDTOSet = new HashSet<>();
        for (Article article : source.getArticles()) {
            articleDTOSet.add(getMapper().map(article, ArticleDTO.class));
        }
        destination.setIdAccount(source.getIdAccount());
        destination.setArticles(articleDTOSet);
        destination.setLogin(source.getLogin());
        destination.setAuthorities(source.getAuthorities());
        destination.setAffiliation(source.getAffiliation());
        Set<ReviewDTO> reviewDTOSet = new HashSet<ReviewDTO>();
        for (Review review : source.getReviews()) {
            reviewDTOSet.add(getMapper().map(review, ReviewDTO.class));
        }
        destination.setReviews(reviewDTOSet);
        destination.setSurname(source.getSurname());
        destination.setAddress(getMapper().map(source.getAddress(), AddressDTO.class));
        destination.setEnabled(source.isEnabled());
        destination.setPassword(source.getPassword());
        destination.setTitle(source.getTitle());
        destination.setEmail(source.getEmail());
        destination.setName(source.getName());
        Set<BaseAccountInformationDTO> redactors = new HashSet<BaseAccountInformationDTO>();
        for (Account redactor : source.getRedactors()) {
            redactors.add(getMapper().map(redactor, BaseAccountInformationDTO.class));
        }
        destination.setRedactors(redactors);
        Set<BaseAccountInformationDTO> reviewers = new HashSet<BaseAccountInformationDTO>();
        for (Account reviewer : source.getReviewers()) {
            reviewers.add(getMapper().map(reviewer, BaseAccountInformationDTO.class));
        }
        destination.setReviewers(reviewers);
        destination.setAccountNonLocked(source.getAccountNonLocked());
        return destination;
    }

    @Override
    public Account convertFrom(AccountDTO source, Account destination) {
        if (destination == null) {
            destination = getAccountRepository().findByLogin(source.getLogin());
        }
        if (destination == null) {
            destination = new Account();
        }
        destination.setAuthorities(source.getAuthorities());
        destination.setLogin(source.getLogin());
        if (source.getEnabled() != null) {
            destination.setEnabled(source.getEnabled());
        }
        Address address = destination.getAddress();
        getMapper().map(source.getAddress(), address);
        destination.setAddress(address);
        destination.setName(source.getName());
        destination.setTitle(source.getTitle());
        destination.setAffiliation(source.getAffiliation());
        Set<Article> articleSet = destination.getArticles();
        for (ArticleDTO articleDTO : source.getArticles()) {
            if (articleDTO.getIdArticle() == null) {
                articleSet.add(getMapper().map(articleDTO, Article.class));
            }
        }
        destination.setArticles(articleSet);
        destination.setEmail(source.getEmail());
        if (source.getPassword() != null) {
            destination.setPassword(source.getPassword());
        }
        destination.setSurname(source.getSurname());
        Set<Account> redactors = new HashSet<>();
        Account redactorsAccount;
        for (BaseAccountInformationDTO baseAccountInformationDTO : source.getRedactors()) {
            redactorsAccount = getAccountRepository().findByLogin(baseAccountInformationDTO.getLogin());
            redactorsAccount.getReviewers().add(destination);
            redactors.add(redactorsAccount);
        }
        Set<String> currentRedactorsLoginsSet = destination.getRedactors().stream().map(Account::getLogin).collect(Collectors.toSet());
        Set<String> destinationRedactorsLoginsSet = source.getRedactors().stream().map(BaseAccountInformationDTO::getLogin).collect(Collectors.toSet());
        Collection<String> redactorsToRemove = CollectionUtils.subtract(currentRedactorsLoginsSet, destinationRedactorsLoginsSet);
        Account redactorToRemove;
        for (String login : redactorsToRemove) {
            redactorToRemove = getAccountRepository().findByLogin(login);
            destination.getRedactors().remove(redactorToRemove);
            redactorToRemove.getReviewers().remove(destination);
        }
        destination.setRedactors(redactors);
        Set<Account> reviewers = new HashSet<>();
        Account reviewersAccount;
        for (BaseAccountInformationDTO baseAccountInformationDTO : source.getReviewers()) {
            reviewersAccount = getAccountRepository().findByLogin(baseAccountInformationDTO.getLogin());
            reviewersAccount.getRedactors().add(destination);
            reviewers.add(reviewersAccount);
        }
        destination.setReviewers(reviewers);
        if (source.getAccountNonLocked() != null) {
            destination.setAccountNonLocked(source.getAccountNonLocked());
        }
        return destination;
    }

    /**
     * Gets mapper.
     *
     * @return the mapper
     */
    public Mapper getMapper() {
        return mapper;
    }


    /**
     * Gets account service.
     *
     * @return the account service
     */
    public AccountService getAccountService() {
        return accountService;
    }

    /**
     * Gets account repository.
     *
     * @return the account repository
     */
    public AccountRepository getAccountRepository() {
        return accountRepository;
    }
}
