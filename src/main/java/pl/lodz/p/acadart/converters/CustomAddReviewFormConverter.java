package pl.lodz.p.acadart.converters;

import org.dozer.DozerConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import pl.lodz.p.acadart.dtos.ReviewDTO;
import pl.lodz.p.acadart.dtos.UploadedFileDTO;
import pl.lodz.p.acadart.forms.AddReviewForm;
import pl.lodz.p.acadart.services.AccountService;
import pl.lodz.p.acadart.services.ArticleService;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * Created by jacek on 03.05.2016.
 */
@Component
public class CustomAddReviewFormConverter extends DozerConverter<AddReviewForm, ReviewDTO> {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * Instantiates a new Custom add review form converter.
     */
    public CustomAddReviewFormConverter() {
        super(AddReviewForm.class, ReviewDTO.class);
    }

    @Resource
    private ArticleService articleService;

    @Resource
    private AccountService accountService;

    @Override
    public ReviewDTO convertTo(AddReviewForm source, ReviewDTO destination) {
        if (destination == null) {
            destination = new ReviewDTO();
        }
        destination.setArticle(getArticleService().findById(source.getIdArticle()));
        destination.setDecision(source.getDecision());
        UploadedFileDTO uploadedFileDTO = new UploadedFileDTO();
        MultipartFile multipartFile = source.getMultipartFile();
        if (multipartFile != null) {
            try {
                uploadedFileDTO.setFile(multipartFile.getBytes());
            } catch (IOException e) {
                log.warn("Failed to retrieve byte array from multipart file " + multipartFile.getName(), e);
            }
        }
        uploadedFileDTO.setFileName(source.getMultipartFile().getOriginalFilename());
        destination.setComment(source.getComment());
        destination.setUploadedFile(uploadedFileDTO);
        return destination;
    }

    @Override
    public AddReviewForm convertFrom(ReviewDTO source, AddReviewForm destination) {
        return null;
    }

    /**
     * Gets article service.
     *
     * @return the article service
     */
    public ArticleService getArticleService() {
        return articleService;
    }

    /**
     * Gets account service.
     *
     * @return the account service
     */
    public AccountService getAccountService() {
        return accountService;
    }
}
