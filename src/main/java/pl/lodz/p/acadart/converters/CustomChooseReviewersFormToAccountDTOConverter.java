package pl.lodz.p.acadart.converters;

import org.dozer.DozerConverter;
import org.springframework.stereotype.Component;
import pl.lodz.p.acadart.dtos.AccountDTO;
import pl.lodz.p.acadart.dtos.ArticleDTO;
import pl.lodz.p.acadart.dtos.BaseAccountInformationDTO;
import pl.lodz.p.acadart.forms.ChooseReviewersForm;
import pl.lodz.p.acadart.services.AccountService;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by jacek on 02.05.2016.
 */
@Component
public class CustomChooseReviewersFormToAccountDTOConverter extends DozerConverter<ChooseReviewersForm, AccountDTO> {

    @Resource
    private AccountService accountService;

    /**
     * Instantiates a new Custom choose reviewers form to account dto converter.
     */
    public CustomChooseReviewersFormToAccountDTOConverter() {
        super(ChooseReviewersForm.class, AccountDTO.class);
    }

    @Override
    public AccountDTO convertTo(ChooseReviewersForm source, AccountDTO destination) {
        if (destination == null) {
            destination = getAccountService().getAccountByLogin(source.getRedactorsLogin());
        }
        Set<BaseAccountInformationDTO> reviewersDTOSet = new HashSet<>();
        for (String reviewersLogin : source.getReviewersLogins()) {
            reviewersDTOSet.add(getAccountService().getAccountByLogin(reviewersLogin));
        }
        destination.setReviewers(reviewersDTOSet);
        return destination;
    }

    @Override
    public ChooseReviewersForm convertFrom(AccountDTO source, ChooseReviewersForm destination) {
        if (destination == null) {
            destination = new ChooseReviewersForm();
        }
        destination.setRedactorsLogin(source.getLogin());
        destination.setAssignedReviewers(source.getReviewers());
        destination.setReviewersLogins(source.getReviewers().stream().map(BaseAccountInformationDTO::getLogin).collect(Collectors.toSet()));
        return destination;
    }

    /**
     * Gets account service.
     *
     * @return the account service
     */
    public AccountService getAccountService() {
        return accountService;
    }
}
