package pl.lodz.p.acadart.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.LocaleResolver;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;

/**
 * Created by jacek on 19.04.2016.
 */
@Component(value = "customAuthenticationFailureHandler")
public class CustomAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler{

    @Resource
    private MessageSource messageSource;

    @Resource
    private LocaleResolver localeResolver;

    @Override
    public void onAuthenticationFailure(HttpServletRequest request,
                                        HttpServletResponse response, AuthenticationException exception)
            throws IOException, ServletException {
        setDefaultFailureUrl("/login?error=true");
        super.onAuthenticationFailure(request, response, exception);

        Locale locale = request.getLocale();

        String errorMessage = getMessageSource().getMessage("auth.bad.credentials", null, locale);

        if (exception.getMessage().equalsIgnoreCase("User account is locked")) {
            errorMessage = getMessageSource().getMessage("auth.account.locked", null, locale);
        }
        if (exception.getMessage().equalsIgnoreCase("User is disabled")) {
            errorMessage = getMessageSource().getMessage("auth.account.disabled", null, locale);
        }
        request.getSession().setAttribute(WebAttributes.AUTHENTICATION_EXCEPTION, errorMessage);
    }

    /**
     * Gets message source.
     *
     * @return the message source
     */
    public MessageSource getMessageSource() {
        return messageSource;
    }

    /**
     * Gets locale resolver.
     *
     * @return the locale resolver
     */
    public LocaleResolver getLocaleResolver() {
        return localeResolver;
    }
}
