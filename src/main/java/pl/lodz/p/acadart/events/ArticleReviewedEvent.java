package pl.lodz.p.acadart.events;

import org.springframework.context.ApplicationEvent;
import pl.lodz.p.acadart.dtos.ArticleDTO;

/**
 * Created by jacek on 16.05.2016.
 */
public class ArticleReviewedEvent extends ApplicationEvent {

    private final ArticleDTO articleDTO;

    /**
     * Instantiates a new Article reviewed event.
     *
     * @param articleDTO the article dto
     */
    public ArticleReviewedEvent(ArticleDTO articleDTO) {
        super(articleDTO);
        this.articleDTO = articleDTO;
    }

    /**
     * Gets article dto.
     *
     * @return the article dto
     */
    public ArticleDTO getArticleDTO() {
        return articleDTO;
    }
}
