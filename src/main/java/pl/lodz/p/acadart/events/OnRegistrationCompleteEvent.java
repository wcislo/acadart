package pl.lodz.p.acadart.events;

import org.springframework.context.ApplicationEvent;
import pl.lodz.p.acadart.dtos.AccountDTO;
import pl.lodz.p.acadart.models.Account;

import java.util.Locale;

/**
 * Created by jacek on 21.03.2016.
 */
public class OnRegistrationCompleteEvent extends ApplicationEvent {

    private final String appUrl;

    private final Locale locale;

    private final AccountDTO accountDto;

    /**
     * Instantiates a new On registration complete event.
     *
     * @param accountDto the account dto
     * @param locale     the locale
     * @param appUrl     the app url
     */
    public OnRegistrationCompleteEvent(AccountDTO accountDto, Locale locale, String appUrl) {
        super(accountDto);

        this.accountDto = accountDto;
        this.locale = locale;
        this.appUrl = appUrl;
    }

    /**
     * Gets app url.
     *
     * @return the app url
     */
    public String getAppUrl() {
        return appUrl;
    }

    /**
     * Gets locale.
     *
     * @return the locale
     */
    public Locale getLocale() {
        return locale;
    }

    /**
     * Gets account dto.
     *
     * @return the account dto
     */
    public AccountDTO getAccountDto() {
        return accountDto;
    }
}
