package pl.lodz.p.acadart.events;

import org.springframework.context.ApplicationEvent;
import pl.lodz.p.acadart.dtos.AccountDTO;

import java.util.Locale;

/**
 * Created by jacek on 06.04.2016.
 */
public class BasicEmailEvent extends ApplicationEvent {

    /**
     * Create a new BasicMailEvent.
     *
     * @param accountDTO user to which email will be sent
     * @param locale     locale to be used in email
     */
    public BasicEmailEvent(AccountDTO accountDTO, Locale locale) {
        super(accountDTO);
    }
}
