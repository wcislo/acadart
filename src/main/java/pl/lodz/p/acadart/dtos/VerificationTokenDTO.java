package pl.lodz.p.acadart.dtos;

import pl.lodz.p.acadart.models.Account;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by jacek on 09.04.2016.
 */
public class VerificationTokenDTO {

    private static final int EXPIRATION = 60 * 24;

    private Long idVerificationToken;

    private String token;

    private Boolean verified;

    private AccountDTO accountDto;

    private Date expiryDate;

    /**
     * Instantiates a new Verification token dto.
     */
    public VerificationTokenDTO() {
        super();
    }

    /**
     * Instantiates a new Verification token dto.
     *
     * @param token      the token
     * @param accountDto the account dto
     */
    public VerificationTokenDTO(String token, AccountDTO accountDto) {
        super();
        this.token = token;
        this.accountDto = accountDto;
        this.expiryDate = calculateExpiryDate(EXPIRATION);
        this.verified = false;
    }

    /**
     * Calculate expiry date date.
     *
     * @param expiryTimeInMinutes the expiry time in minutes
     * @return the date
     */
    private Date calculateExpiryDate(int expiryTimeInMinutes) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Timestamp(cal.getTime().getTime()));
        cal.add(Calendar.MINUTE, expiryTimeInMinutes);
        return new Date(cal.getTime().getTime());
    }

    /**
     * Gets id verification token.
     *
     * @return the id verification token
     */
    public Long getIdVerificationToken() {
        return idVerificationToken;
    }

    /**
     * Sets id verification token.
     *
     * @param id the id
     */
    public void setIdVerificationToken(Long id) {
        this.idVerificationToken = idVerificationToken;
    }

    /**
     * Gets token.
     *
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * Sets token.
     *
     * @param token the token
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * Gets verified.
     *
     * @return the verified
     */
    public Boolean getVerified() {
        return verified;
    }

    /**
     * Sets verified.
     *
     * @param verified the verified
     */
    public void setVerified(Boolean verified) {
        this.verified = verified;
    }

    /**
     * Gets account.
     *
     * @return the account
     */
    public AccountDTO getAccount() {
        return accountDto;
    }

    /**
     * Sets account dto.
     *
     * @param account the account
     */
    public void setAccountDto(AccountDTO account) {
        this.accountDto = account;
    }

    /**
     * Gets expiry date.
     *
     * @return the expiry date
     */
    public Date getExpiryDate() {
        return expiryDate;
    }

    /**
     * Sets expiry date.
     *
     * @param expiryDate the expiry date
     */
    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }
}
