package pl.lodz.p.acadart.dtos;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * The type Generic datatables dto.
 *
 * @param <T> the type parameter
 */
public class GenericDatatablesDTO<T> {

    private Collection<T> data;

    private Integer draw;

    private Integer recordsTotal;

    private Integer recordsFiltered;

    /**
     * Instantiates a new Generic datatables dto.
     *
     * @param data            the data
     * @param draw            the draw
     * @param recordsTotal    the records total
     * @param recordsFiltered the records filtered
     */
    public GenericDatatablesDTO(Collection<T> data, Integer draw, Integer recordsTotal, Integer recordsFiltered) {
        this.data = data;
        this.recordsTotal = recordsTotal;
        this.draw = draw;
        this.recordsFiltered = recordsFiltered;
    }

    /**
     * Gets data.
     *
     * @return the data
     */
    public Collection<T> getData() {
        return data;
    }

    /**
     * Sets data.
     *
     * @param data the data
     */
    public void setData(Collection<T> data) {
        this.data = data;
    }

    /**
     * Gets draw.
     *
     * @return the draw
     */
    public Integer getDraw() {
        return draw;
    }

    /**
     * Sets draw.
     *
     * @param draw the draw
     */
    public void setDraw(Integer draw) {
        this.draw = draw;
    }

    /**
     * Gets records total.
     *
     * @return the records total
     */
    public Integer getRecordsTotal() {
        return recordsTotal;
    }

    /**
     * Sets records total.
     *
     * @param recordsTotal the records total
     */
    public void setRecordsTotal(Integer recordsTotal) {
        this.recordsTotal = recordsTotal;
    }

    /**
     * Gets records filtered.
     *
     * @return the records filtered
     */
    public Integer getRecordsFiltered() {
        return recordsFiltered;
    }

    /**
     * Sets records filtered.
     *
     * @param recordsFiltered the records filtered
     */
    public void setRecordsFiltered(Integer recordsFiltered) {
        this.recordsFiltered = recordsFiltered;
    }


}
