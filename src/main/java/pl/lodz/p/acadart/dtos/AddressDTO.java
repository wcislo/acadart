package pl.lodz.p.acadart.dtos;

/**
 * The type Address dto.
 */
public class AddressDTO {

    private Long idAddress;

    private String country;

    private String city;

    private String streetNumberFlat;

    private String postalCode;

    /**
     * Gets id address.
     *
     * @return the id address
     */
    public Long getIdAddress() {
        return idAddress;
    }

    /**
     * Sets id address.
     *
     * @param idAddress the id address
     */
    public void setIdAddress(Long idAddress) {
        this.idAddress = idAddress;
    }

    /**
     * Gets country.
     *
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets country.
     *
     * @param country the country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * Gets city.
     *
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets city.
     *
     * @param city the city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * Gets street number flat.
     *
     * @return the street number flat
     */
    public String getStreetNumberFlat() {
        return streetNumberFlat;
    }

    /**
     * Sets street number flat.
     *
     * @param streetNumberFlat the street number flat
     */
    public void setStreetNumberFlat(String streetNumberFlat) {
        this.streetNumberFlat = streetNumberFlat;
    }

    /**
     * Gets postal code.
     *
     * @return the postal code
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * Sets postal code.
     *
     * @param postalCode the postal code
     */
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }
}
