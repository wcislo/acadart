package pl.lodz.p.acadart.dtos;

/**
 * Created by jacek on 12.04.2016.
 */
public class UploadedFileDTO {

    private Long idUploadedFile;

    private String fileName;

    private byte[] file;

    /**
     * Gets id uploaded file.
     *
     * @return the id uploaded file
     */
    public Long getIdUploadedFile() {
        return idUploadedFile;
    }

    /**
     * Sets id uploaded file.
     *
     * @param idUploadedFile the id uploaded file
     */
    public void setIdUploadedFile(Long idUploadedFile) {
        this.idUploadedFile = idUploadedFile;
    }

    /**
     * Gets file name.
     *
     * @return the file name
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Sets file name.
     *
     * @param fileName the file name
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * Get file byte [ ].
     *
     * @return the byte [ ]
     */
    public byte[] getFile() {
        return file;
    }

    /**
     * Sets file.
     *
     * @param file the file
     */
    public void setFile(byte[] file) {
        this.file = file;
    }
}
