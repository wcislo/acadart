package pl.lodz.p.acadart.dtos;

import pl.lodz.p.acadart.models.Account;
import pl.lodz.p.acadart.models.ArticleStatus;
import pl.lodz.p.acadart.models.Decision;

import javax.persistence.Column;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * The type Article dto.
 */
public class ArticleDTO extends BaseArticleInformationDTO {

    private Long idArticle;

    private Set<ReviewDTO> reviews = new HashSet<>();

    private Date creationDate = new Date();

    private String authorsComment = new String();

    private String redactorsComment = new String();

    private UploadedFileDTO uploadedFile = new UploadedFileDTO();

    private BaseAccountInformationDTO redactor;

    private Set<BaseAccountInformationDTO> assignedReviewers = new HashSet<>();

    private String articleAbstract;

    private Decision decision;

    /**
     * Instantiates a new Article dto.
     */
    public ArticleDTO() {
    }

    /**
     * Instantiates a new Article dto.
     *
     * @param authorLogin the author login
     */
    public ArticleDTO(String authorLogin) {
        super(authorLogin);
    }


    /**
     * Gets id article.
     *
     * @return the id article
     */
    public Long getIdArticle() {
        return idArticle;
    }

    /**
     * Sets id article.
     *
     * @param idArticle the id article
     */
    public void setIdArticle(Long idArticle) {
        this.idArticle = idArticle;
    }

    /**
     * Gets reviews.
     *
     * @return the reviews
     */
    public Set<ReviewDTO> getReviews() {
        return reviews;
    }

    /**
     * Sets reviews.
     *
     * @param reviews the reviews
     */
    public void setReviews(Set<ReviewDTO> reviews) {
        this.reviews = reviews;
    }

    /**
     * Gets creation date.
     *
     * @return the creation date
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * Sets creation date.
     *
     * @param creationDate the creation date
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * Gets authors comment.
     *
     * @return the authors comment
     */
    public String getAuthorsComment() {
        return authorsComment;
    }

    /**
     * Sets authors comment.
     *
     * @param authorsComment the authors comment
     */
    public void setAuthorsComment(String authorsComment) {
        this.authorsComment = authorsComment;
    }

    /**
     * Gets redactors comment.
     *
     * @return the redactors comment
     */
    public String getRedactorsComment() {
        return redactorsComment;
    }

    /**
     * Sets redactors comment.
     *
     * @param reviewerComment the reviewer comment
     */
    public void setRedactorsComment(String reviewerComment) {
        this.redactorsComment = reviewerComment;
    }

    /**
     * Gets uploaded file.
     *
     * @return the uploaded file
     */
    public UploadedFileDTO getUploadedFile() {
        return uploadedFile;
    }

    /**
     * Sets uploaded file.
     *
     * @param uploadedFile the uploaded file
     */
    public void setUploadedFile(UploadedFileDTO uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    /**
     * Getter for property 'redactor'.
     *
     * @return Value for property 'redactor'.
     */
    public BaseAccountInformationDTO getRedactor() {
        return redactor;
    }

    /**
     * Setter for property 'redactor'.
     *
     * @param redactor Value to set for property 'redactor'.
     */
    public void setRedactor(BaseAccountInformationDTO redactor) {
        this.redactor = redactor;
    }

    /**
     * Getter for property 'assignedReviewers'.
     *
     * @return Value for property 'assignedReviewers'.
     */
    public Set<BaseAccountInformationDTO> getAssignedReviewers() {
        return assignedReviewers;
    }

    /**
     * Setter for property 'assignedReviewers'.
     *
     * @param assignedReviewers Value to set for property 'assignedReviewers'.
     */
    public void setAssignedReviewers(Set<BaseAccountInformationDTO> assignedReviewers) {
        this.assignedReviewers = assignedReviewers;
    }

    /**
     * Getter for property 'articleAbstract'.
     *
     * @return Value for property 'articleAbstract'.
     */
    public String getArticleAbstract() {
        return articleAbstract;
    }

    /**
     * Setter for property 'articleAbstract'.
     *
     * @param articleAbstract Value to set for property 'articleAbstract'.
     */
    public void setArticleAbstract(String articleAbstract) {
        this.articleAbstract = articleAbstract;
    }


    /**
     * Getter for property 'decision'.
     *
     * @return Value for property 'decision'.
     */
    public Decision getDecision() {
        return decision;
    }

    /**
     * Setter for property 'decision'.
     *
     * @param decision Value to set for property 'decision'.
     */
    public void setDecision(Decision decision) {
        this.decision = decision;
    }
}
