package pl.lodz.p.acadart.dtos;

import pl.lodz.p.acadart.models.Title;

/**
 * The type Base account information dto.
 */
public class BaseAccountInformationDTO {

    private Long idAccount;

    private String email;

    private String name;

    private String surname;

    private String login;

    private Title title;

    /**
     * Gets login.
     *
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * Sets login.
     *
     * @param login the login
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * Gets email.
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets email.
     *
     * @param email the email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets surname.
     *
     * @return the surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Sets surname.
     *
     * @param surname the surname
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }


    /**
     * Gets id account.
     *
     * @return the id account
     */
    public Long getIdAccount() {
        return idAccount;
    }

    /**
     * Sets id account.
     *
     * @param idAccount the id account
     */
    public void setIdAccount(Long idAccount) {
        this.idAccount = idAccount;
    }

    /**
     * Gets title.
     *
     * @return the title
     */
    public Title getTitle() {
        return title;
    }

    /**
     * Sets title.
     *
     * @param title the title
     */
    public void setTitle(Title title) {
        this.title = title;
    }
}
