package pl.lodz.p.acadart.dtos;

import pl.lodz.p.acadart.models.ArticleStatus;

/**
 * Created by jacek on 11.04.2016.
 */
public class BaseArticleInformationDTO {

    private BaseAccountInformationDTO author;

    private String title;

    private String identifier;

    private ArticleStatus articleStatus = ArticleStatus.PENDING;

    /**
     * Instantiates a new Base article information dto.
     */
    public BaseArticleInformationDTO() {
    }

    /**
     * Instantiates a new Base article information dto.
     *
     * @param authorLogin the author login
     */
    public BaseArticleInformationDTO(String authorLogin) {
        this.author = new BaseAccountInformationDTO();
        this.author.setLogin(authorLogin);
    }

    /**
     * Gets author.
     *
     * @return the author
     */
    public BaseAccountInformationDTO getAuthor() {
        return author;
    }

    /**
     * Sets author.
     *
     * @param author the author
     */
    public void setAuthor(BaseAccountInformationDTO author) {
        this.author = author;
    }

    /**
     * Gets title.
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets title.
     *
     * @param title the title
     */
    public void setTitle(String title) {
        this.title = title;
    }


    /**
     * Gets identifier.
     *
     * @return the identifier
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * Sets identifier.
     *
     * @param identifier the identifier
     */
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    /**
     * Gets article status.
     *
     * @return the article status
     */
    public ArticleStatus getArticleStatus() {
        return articleStatus;
    }

    /**
     * Sets article status.
     *
     * @param articleStatus the article status
     */
    public void setArticleStatus(ArticleStatus articleStatus) {
        this.articleStatus = articleStatus;
    }
}
