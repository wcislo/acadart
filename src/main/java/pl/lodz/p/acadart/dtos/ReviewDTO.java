package pl.lodz.p.acadart.dtos;

import pl.lodz.p.acadart.models.Decision;

/**
 * The type Review dto.
 */
public class ReviewDTO {

    private Long idReview;

    private BaseArticleInformationDTO article;

    private Decision decision;

    private BaseAccountInformationDTO reviewer;

    private UploadedFileDTO uploadedFile;

    private String comment;

    /**
     * Gets article.
     *
     * @return the article
     */
    public BaseArticleInformationDTO getArticle() {
        return article;
    }


    /**
     * Sets article.
     *
     * @param article the article
     */
    public void setArticle(BaseArticleInformationDTO article) {
        this.article = article;
    }

    /**
     * Gets decision.
     *
     * @return the decision
     */
    public Decision getDecision() {
        return decision;
    }

    /**
     * Sets decision.
     *
     * @param decision the decision
     */
    public void setDecision(Decision decision) {
        this.decision = decision;
    }

    /**
     * Gets reviewer.
     *
     * @return the reviewer
     */
    public BaseAccountInformationDTO getReviewer() {
        return reviewer;
    }

    /**
     * Sets reviewer.
     *
     * @param reviewer the reviewer
     */
    public void setReviewer(BaseAccountInformationDTO reviewer) {
        this.reviewer = reviewer;
    }

    /**
     * Gets id review.
     *
     * @return the id review
     */
    public Long getIdReview() {
        return idReview;
    }

    /**
     * Sets id review.
     *
     * @param idReview the id review
     */
    public void setIdReview(Long idReview) {
        this.idReview = idReview;
    }

    /**
     * Gets uploaded file.
     *
     * @return the uploaded file
     */
    public UploadedFileDTO getUploadedFile() {
        return uploadedFile;
    }

    /**
     * Sets uploaded file.
     *
     * @param uploadedFile the uploaded file
     */
    public void setUploadedFile(UploadedFileDTO uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    /**
     * Gets comment.
     *
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * Sets comment.
     *
     * @param comment the comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }
}
