package pl.lodz.p.acadart.dtos;

import pl.lodz.p.acadart.models.Authority;

import java.util.HashSet;
import java.util.Set;

/**
 * The type Account dto.
 */
public class AccountDTO extends BaseAccountInformationDTO {

    private String password;

    private AddressDTO address;

    private String affiliation;

    private Set<Authority> authorities = new HashSet<>();

    private Set<ArticleDTO> articles = new HashSet<ArticleDTO>();

    private Set<ReviewDTO> reviews = new HashSet<ReviewDTO>();

    private Set<BaseAccountInformationDTO> reviewers = new HashSet<BaseAccountInformationDTO>();

    private Set<BaseAccountInformationDTO> redactors = new HashSet<BaseAccountInformationDTO>();

    private Boolean enabled;

    private Boolean isAccountNonLocked;

    /**
     * Gets password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets password.
     *
     * @param password the password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets address.
     *
     * @return the address
     */
    public AddressDTO getAddress() {
        return address;
    }

    /**
     * Sets address.
     *
     * @param address the address
     */
    public void setAddress(AddressDTO address) {
        this.address = address;
    }

    /**
     * Gets affiliation.
     *
     * @return the affiliation
     */
    public String getAffiliation() {
        return affiliation;
    }

    /**
     * Sets affiliation.
     *
     * @param affiliation the affiliation
     */
    public void setAffiliation(String affiliation) {
        this.affiliation = affiliation;
    }


    /**
     * Gets authorities.
     *
     * @return the authorities
     */
    public Set<Authority> getAuthorities() {
        return authorities;
    }

    /**
     * Sets authorities.
     *
     * @param authorities the authorities
     */
    public void setAuthorities(Set<Authority> authorities) {
        this.authorities = authorities;
    }

    /**
     * Gets articles.
     *
     * @return the articles
     */
    public Set<ArticleDTO> getArticles() {
        return articles;
    }

    /**
     * Sets articles.
     *
     * @param articles the articles
     */
    public void setArticles(Set<ArticleDTO> articles) {
        this.articles = articles;
    }

    /**
     * Gets reviews.
     *
     * @return the reviews
     */
    public Set<ReviewDTO> getReviews() {
        return reviews;
    }

    /**
     * Sets reviews.
     *
     * @param reviews the reviews
     */
    public void setReviews(Set<ReviewDTO> reviews) {
        this.reviews = reviews;
    }

    /**
     * Gets reviewers.
     *
     * @return the reviewers
     */
    public Set<BaseAccountInformationDTO> getReviewers() {
        return reviewers;
    }

    /**
     * Sets reviewers.
     *
     * @param reviewers the reviewers
     */
    public void setReviewers(Set<BaseAccountInformationDTO> reviewers) {
        this.reviewers = reviewers;
    }

    /**
     * Gets redactors.
     *
     * @return the redactors
     */
    public Set<BaseAccountInformationDTO> getRedactors() {
        return redactors;
    }

    /**
     * Sets redactors.
     *
     * @param redactors the redactors
     */
    public void setRedactors(Set<BaseAccountInformationDTO> redactors) {
        this.redactors = redactors;
    }

    /**
     * Gets enabled.
     *
     * @return the enabled
     */
    public Boolean getEnabled() {
        return enabled;
    }

    /**
     * Sets enabled.
     *
     * @param enabled the enabled
     */
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * Gets account non locked.
     *
     * @return the account non locked
     */
    public Boolean getAccountNonLocked() {
        return isAccountNonLocked;
    }

    /**
     * Sets account non locked.
     *
     * @param accountNonLocked the account non locked
     */
    public void setAccountNonLocked(Boolean accountNonLocked) {
        isAccountNonLocked = accountNonLocked;
    }

}
